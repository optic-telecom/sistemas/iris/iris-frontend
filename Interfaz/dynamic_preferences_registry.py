from dynamic_preferences.types import BooleanPreference, StringPreference
from dynamic_preferences.preferences import Section
from dynamic_preferences.registries import global_preferences_registry


general = Section('general')


@global_preferences_registry.register
class SiteTitle(StringPreference):
    section = general
    name = 'Enviroment'
    default = 'local'
    required = False