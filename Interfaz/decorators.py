from django.shortcuts import redirect
from django.urls import reverse
import requests
from django.conf import settings
import os

def check_sesion(func):

    def decorator(request, *args, **kwargs):

        #Se captura el token que tiene el navegador  
        token = request.COOKIES.get('token')
        DOMINE_API = os.environ['DOMAIN_API'] + '{0}'
        url_token_val = DOMINE_API.format("api/v1/user/token/verify/")

        #Se realiza la respectiva consulta en el servido, si el token se mantiene activo.
        request_token = requests.post(
            url=url_token_val,
            verify=False,
            data={
                'token': token
            }
        )

        #Si la respuesta es positiva se le permite seguir en la funcion. si no se le redirige al login.
        
        if request_token.status_code == 200:    
            result = func(request, *args, **kwargs)
        else:
            result = redirect(
                request.build_absolute_uri(
                    reverse(
                        viewname='Interfaz:login',
                        args=[],
                    ),
                )
            )
    
        return result

    return decorator
