CONTEXT_ADMIN_PAGE = {

    'Tipificar':
        {
            'icon': 'fas fa-ticket-alt',
            'item':
                [
                    {
                        'url': 'typify_create',
                        'name': 'Todas'
                    },
                    {
                        'url': 'typify_me',
                        'name': 'Mis tipificaciones'
                    },
                    {
                        'url': 'typify_opened',
                        'name': 'Abiertas'
                    },
                    {
                        'url': 'typify_last_hours',
                        'name': 'Últimas 24 horas'
                    },
                    {
                        'url': 'typify_refund',
                        'name': 'Reembolso'
                    },
                    {
                        'url': 'typify_defaulter',
                        'name': 'Morosidad por baja'
                    },
                    {
                        'url': 'typify_loss_of_service',
                        'name': 'Baja de servicio'
                    },
                    {
                        'url': 'typify_product',
                        'name': 'Producto'
                    },
                    {
                        'url': 'typify_retention',
                        'name': 'Retención'
                    },
                    {
                        'url': 'typify_new_service',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'typify_call_after',
                        'name': 'Llamar luego'
                    },
                    {
                        'url': 'typify_tracing',
                        'name': 'Seguimiento'
                    },
                    
                ]
        },
    'Actualizaciones':
        {
            'icon': 'fas fa-sync',
            'item':
                [
                    {
                        'url': 'feasibility_list',
                        'name': 'Factibilidad'
                    },
                    {
                        'url': 'new_service_list',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'change_headline_list',
                        'name': 'Cambio titular'
                    },
                    {
                        'url': 'change_plan_list',
                        'name': 'Cambio de plan'
                    },

                ]
        },
    'Configuración':
        {
            'icon': 'fa fa-cog',
            'item':
                [
                    {
                        'url': 'category_list',
                        'name': 'Tipificacion'
                    },
                    {
                        'url': 'operator_view',
                        'name': 'Operadores'
                    },
                    {
                        'url': 'user_list',
                        'name': 'Usuarios'
                    },
                ]
        },
}

CONTEXT_AGENT_ADMIN_PAGE = {

    'Tipificar':
        {
            'icon': 'fas fa-ticket-alt',
            'item':
                [
                    {
                        'url': 'typify_create',
                        'name': 'Todas'
                    },
                    {
                        'url': 'typify_me',
                        'name': 'Mis tipificaciones'
                    },
                    {
                        'url': 'typify_opened',
                        'name': 'Abiertas'
                    },
                    {
                        'url': 'typify_last_hours',
                        'name': 'Últimas 24 horas'
                    },
                    {
                        'url': 'typify_refund',
                        'name': 'Reembolso'
                    },
                    {
                        'url': 'typify_defaulter',
                        'name': 'Morosidad por baja'
                    },
                    {
                        'url': 'typify_loss_of_service',
                        'name': 'Baja de servicio'
                    },
                    {
                        'url': 'typify_product',
                        'name': 'Producto'
                    },
                    {
                        'url': 'typify_retention',
                        'name': 'Retención'
                    },
                    {
                        'url': 'typify_new_service',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'typify_call_after',
                        'name': 'Llamar luego'
                    },
                    {
                        'url': 'typify_tracing',
                        'name': 'Seguimiento'
                    },
                    
                ]
        },
    'Actualizaciones':
        {
            'icon': 'fas fa-sync',
            'item':
                [
                    {
                        'url': 'feasibility_list',
                        'name': 'Factibilidad'
                    },
                    {
                        'url': 'new_service_list',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'change_headline_list',
                        'name': 'Cambio titular'
                    },
                    {
                        'url': 'change_plan_list',
                        'name': 'Cambio de plan'
                    },

                ]
        },
    'Configuración':
        {
            'icon': 'fa fa-cog',
            'item':
                [
                    {
                        'url': 'category_list',
                        'name': 'Tipificacion'
                    },
                ]
        },
}

CONTEXT_AGENT_PAGE = {
    'Tipificar':
        {
            'icon': 'fas fa-ticket-alt',
            'item':
                [
                    {
                        'url': 'typify_create',
                        'name': 'Todas'
                    },
                    {
                        'url': 'typify_me',
                        'name': 'Mis tipificaciones'
                    },
                    {
                        'url': 'typify_opened',
                        'name': 'Abiertas'
                    },
                    {
                        'url': 'typify_last_hours',
                        'name': 'Últimas 24 horas'
                    },
                    {
                        'url': 'typify_refund',
                        'name': 'Reembolso'
                    },
                    {
                        'url': 'typify_defaulter',
                        'name': 'Morosidad por baja'
                    },
                    {
                        'url': 'typify_loss_of_service',
                        'name': 'Baja de servicio'
                    },
                    {
                        'url': 'typify_product',
                        'name': 'Producto'
                    },
                    {
                        'url': 'typify_retention',
                        'name': 'Retención'
                    },
                    {
                        'url': 'typify_new_service',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'typify_call_after',
                        'name': 'Llamar luego'
                    },
                    {
                        'url': 'typify_tracing',
                        'name': 'Seguimiento'
                    },
                    
                ]
        },
    'Actualizaciones':
        {
            'icon': 'fas fa-sync',
            'item':
                [
                    {
                        'url': 'feasibility_list',
                        'name': 'Factibilidad'
                    },
                    {
                        'url': 'new_service_list',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'change_headline_list',
                        'name': 'Cambio titular'
                    },
                    {
                        'url': 'change_plan_list',
                        'name': 'Cambio de plan'
                    },
                ]
        },
}

CONTEXT_MASSIVE_MESSAGES = {

    'Disparadores':
        {
           'icon': 'fa fa-calendar',
            'item':
                [
                    {
                        'url': 'view_template',
                        'name': 'Plantillas'
                    },
                    {
                        'url': 'view_triggers_time',
                        'name': 'Temporales'
                    },
                ]
        },
}

CONTEXT_EVENT_MESSAGES = {

    'Disparadores':
        {
            'icon': 'fa fa-calendar',
            'item':
                [
                    {
                        'url': 'view_template',
                        'name': 'Plantillas'
                    },
                    {
                        'url': 'view_trigger_event',
                        'name': 'Eventos'
                    }
                ],
        },
}

CONTEXT_MASSIVE_EVENT_MESSAGES = {
    'Disparadores':
        {
            'icon': 'fa fa-calendar',
            'item':
                [
                    {
                        'url': 'view_template',
                        'name': 'Plantillas'
                    },
                    {
                        'url': 'view_triggers_time',
                        'name': 'Temporales'
                    },
                    {
                        'url': 'view_trigger_event',
                        'name': 'Eventos'
                    }
                ]
        },
}

CONTEXT_ADMIN_PAGE_AUX = {
    'menu':
    {
        'Tipificar':
            {
                'icon': 'fa fa-file',
                'item':
                    [
                    {
                        'url': 'typify_create',
                        'name': 'Todas'
                    },
                    {
                        'url': 'typify_create_me',
                        'name': 'Mis tipificaciones'
                    },
                    {
                        'url': 'typify_opened',
                        'name': 'Abiertas'
                    },
                    {
                        'url': 'typify_last_hours',
                        'name': 'Últimas 24 horas'
                    },
                    {
                        'url': 'typify_refund',
                        'name': 'Reembolso'
                    },
                    {
                        'url': 'typify_defaulter',
                        'name': 'Morosidad por baja'
                    },
                    {
                        'url': 'typify_loss_of_service',
                        'name': 'Baja de servicio'
                    },
                    {
                        'url': 'typify_product',
                        'name': 'Producto'
                    },
                    {
                        'url': 'typify_retention',
                        'name': 'Retención'
                    },
                    {
                        'url': 'typify_new_service',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'typify_call_after',
                        'name': 'Llamar luego'
                    },
                    {
                        'url': 'typify_tracing',
                        'name': 'Seguimiento'
                    },
                    
                ]
            },
        'Configuraciòn':
            {
                'icon': 'fa fa-cog',
                'item':
                    [
                        {
                            'url': 'category_list',
                            'name': 'Tipificacion'
                        },
                        {
                            'url': 'operator_list',
                            'name': 'Operadores'
                        },
                        {
                            'url': 'template_list',
                            'name': 'Plantillas'
                        },
                        {
                            'url': 'user_list',
                            'name': 'Usuarios'
                        },
                    ]
            },
    }
}

CONTEXT_AGENT_PAGE_AUX = {
    'menu':
    {
        'Tipificar':
            {
                'icon': 'fa fa-file',
                'item':
                    [
                    {
                        'url': 'typify_create',
                        'name': 'Todas'
                    },
                    {
                        'url': 'typify_create_me',
                        'name': 'Mis tipificaciones'
                    },
                    {
                        'url': 'typify_opened',
                        'name': 'Abiertas'
                    },
                    {
                        'url': 'typify_last_hours',
                        'name': 'Últimas 24 horas'
                    },
                    {
                        'url': 'typify_refund',
                        'name': 'Reembolso'
                    },
                    {
                        'url': 'typify_defaulter',
                        'name': 'Morosidad por baja'
                    },
                    {
                        'url': 'typify_loss_of_service',
                        'name': 'Baja de servicio'
                    },
                    {
                        'url': 'typify_product',
                        'name': 'Producto'
                    },
                    {
                        'url': 'typify_retention',
                        'name': 'Retención'
                    },
                    {
                        'url': 'typify_new_service',
                        'name': 'Nuevo servicio'
                    },
                    {
                        'url': 'typify_call_after',
                        'name': 'Llamar luego'
                    },
                    {
                        'url': 'typify_tracing',
                        'name': 'Seguimiento'
                    },
                    
                ]
            },
    }
}