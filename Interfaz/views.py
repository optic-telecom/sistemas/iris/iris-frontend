from django.conf import settings
from django.shortcuts import render, redirect
from .decorators import check_sesion
from .context import CONTEXT_AGENT_PAGE, CONTEXT_ADMIN_PAGE, CONTEXT_AGENT_ADMIN_PAGE, CONTEXT_MASSIVE_MESSAGES, CONTEXT_EVENT_MESSAGES, CONTEXT_MASSIVE_EVENT_MESSAGES
from dynamic_preferences.registries import global_preferences_registry
from pdb import set_trace
import requests
import string
import random
import datetime
from django.views.decorators.cache import cache_page
from django.contrib.auth.decorators import login_required
from rest_framework_jwt.utils import jwt_decode_handler
import os

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
        return ''.join(random.choice(chars) for _ in range(size))

# Se define el Menú que se usara en el navegador.
def user_rules(response_json):

    rules = {}
    massive = False
    event = False

    for  values in response_json:

        if 'Administrador' in values:
            
            menu  = CONTEXT_ADMIN_PAGE.copy()

        if 'Agente' in values:

            menu = CONTEXT_AGENT_PAGE.copy()

        if 'Agente_administrador' in values:

            menu = CONTEXT_AGENT_ADMIN_PAGE.copy()

        if 'Massive_messages' in values:

            menu = CONTEXT_MASSIVE_MESSAGES.copy()
            massive = True

        if 'Event_messages' in values:

            menu= CONTEXT_EVENT_MESSAGES.copy()
            event = True

        if massive and event:

            menu= CONTEXT_MASSIVE_EVENT_MESSAGES.copy()

        rules.update(menu)

    return rules


def enviroment(CONTEXT, token=None, operator=1, REQUEST=None):

    CONTEXT['DOMAIN_API'] = os.environ['DOMAIN_API'] + '{0}'
    CONTEXT['DOMAIN_VIEW'] = os.environ['DOMAIN_VIEW'] + '{0}'
    CONTEXT['SENTRY_ET'] = settings.SENTRY_ET

    if REQUEST:

        #Se captura el token de la cookie del navegador.
        TOKEN = REQUEST.COOKIES['token']

        #Consulta si el token esta activo con el usuario.
        response = requests.get(
            url = CONTEXT['DOMAIN_API'].format('api/v1/user/info/'),
            verify=False,
            headers={
                "Authorization": 'JWT {0}'.format(TOKEN),
            },
        )

        if response.status_code == 200 or response.json()['groups']:

            response_json = response.json()['groups']

            CONTEXT['user_name'] = jwt_decode_handler(TOKEN.replace('JWT ', ''))['username']
            CONTEXT['user_ID'] = jwt_decode_handler(TOKEN.replace('JWT ', ''))['user_id']
            CONTEXT['menu'] = user_rules(response_json)

        CONTEXT['operator'] = operator

    CONTEXT['TOKEN'] = token if token else id_generator(20)


    return CONTEXT



def login(request):

    return render(
        request=request,
        template_name='Interfaz/Usuario/login.html',
        context=enviroment({}),
    
    )


@check_sesion
def principalView(request, token, operator):

    return render(
        request=request,
        template_name='index.html',
        context=enviroment({}, token, 2, request),
    )

@check_sesion
def view_category_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Category/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_category_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Category/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_category_list(request, token, operator=1):
    
    return render(
        request=request,
        template_name='Interfaz/Agente/Category/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_category_add(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Category/add.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_classification_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Clasification/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_classification_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Clasification/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_classification_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Clasification/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_ticket_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Ticket/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_ticket_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Ticket/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_operator_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Operator/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_operator_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Operator/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_operator_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Operator/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_operator_view(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Operator/view.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_customer_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Customer/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_customer_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Customer/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_template_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Template/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_template_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Template/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_template_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Template/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_template_view(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Template/view.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_typify_create(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_create_me(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'create_me'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de colas de tipificacion para el menú ninguna
@check_sesion
def view_typify_create_queue(request, token, operator=1, queue_id=0):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['queue_ID'] = queue_id
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_typify_change(request, token,  operator, typify_number):

    CONTEXT = {}
    CONTEXT['typify_id'] = typify_number

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/show.html',
        context=enviroment(CONTEXT, token, operator),
    )

@check_sesion
def view_typify_typify(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/typify.html',
        context=enviroment({}, token, operator, request),
    )

#Vista donde se cargan una tabla de las tipificaciones abiertas.
@check_sesion
def view_typify_open(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/showtypifyopen.html',
        context=enviroment({}, token, operator, request),
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_opened(request, token, operator=1, status='opened'):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'opened'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_refund(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'refund'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_retention(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'retention'
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_new_service(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'new_service'
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_call_after(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'call_after'
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_tracing(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'tracing'
    del CONTEXT['user_ID']

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_defaulter(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'defaulter'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_loss_of_service(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'loss_of_service'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones abiertas
@check_sesion
def view_typify_product(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'product'


    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Cargar lista de tipificaciones de las ultimas 24 horas
@check_sesion
def view_typify_last_hours(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'last_hours'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

@check_sesion
def view_typify_me(request, token, operator=1):

    CONTEXT = enviroment({}, token, operator, request)
    CONTEXT['custom'] = 'me'

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/create.html',
        context=CONTEXT,
    )

#Vista que mostrara los selectores donde se mostraran los cambios masivos..
@check_sesion
def view_masive_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Typify/masivechange.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_user_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/User/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_history(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/History/history.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_new_contact_change(request, token, operator, new_contact_id, typify_id):

    CONTEXT = {}
    CONTEXT['new_contact_id'] = new_contact_id
    CONTEXT['typify_id'] = typify_id

    return render(
        request=request,
        template_name='Interfaz/Agente/New_Contact/change.html',
        context=enviroment(CONTEXT, token, operator),
    )

@check_sesion
def view_feasibility_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Feasibility/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_feasibility_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Feasibility/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_feasibility_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Feasibility/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_new_service_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/New_service/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_new_service_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/New_service/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_new_service_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/New_service/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_change_plan_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Actions_typify/ChangePlan/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_change_plan_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Actions_typify/ChangePlan/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_change_headline_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Actions_typify/ChangeHeadline/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_change_headline_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Actions_typify/ChangeHeadline/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_records(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Records/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_triggers_time(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Triggers_Message/time_trigger_create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/view.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event_list(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/list.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event_email_create(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/Event_email/create.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_trigger_event_email_change(request, token, operator=1):

    return render(
        request=request,
        template_name='Interfaz/Agente/Trigger_event/Event_email/change.html',
        context=enviroment({}, token, operator, request),
    )

@check_sesion
def view_triggers_time_change(request, token, operator, trigger_time_number):

    CONTEXT = {}
    CONTEXT['TRIGGER_TIME_NUMBER'] = trigger_time_number

    return render(
        request=request,
        template_name='Interfaz/Agente/Triggers_Message/time_trigger_change.html',
        context=enviroment(CONTEXT, token, operator, request),
    )

@check_sesion
def view_wizard(request, token, operator=1):
    
    return render(
        request=request,
        template_name='Interfaz/Agente/Wizard/view.html',
        context=enviroment({}, token, operator, request),
    )

