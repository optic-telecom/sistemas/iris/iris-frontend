/**
 * @author {Cristian Cantero}
 * @file { feasibility_lis }
 */

/**
 * @function change_change_plan para cambiar los planes.
 * @param {number} ID
 *@JselectorElement {id_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar
 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
 * Llama a la @function {@param, @JselectorElement } load_data_feasibility del @file {index.js}
 */
function change_change_plan(ID) {
	$('#modal-html').empty();
	callView('change_plan_change', $('#modal-html'));
	load_data_change_plan(ID);
	$('#modals').show();
}

/**
 * @function delete_feasibility esta funcio elimina (desactiva) la factibilidad para vista
 * @param {number} ID
 */
function delete_change_plan(ID) {
	/**
	 * Llama a la @function {@param, @JselectorElement } removeItem del @file {index.js}
	 */
	removeItem(url_base.format('api/v1/customers/change_plan/{0}/'.format(ID)), $('#table-change-plan'));
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-change-plan').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Cambio de plan',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Cambio de plan de _START_ a _END_ de un total de _TOTAL_ Cambio de plan',
		sInfoEmpty: 'Mostrando Cambio de plan del 0 al 0 de un total de 0 Cambio de plan',
		sInfoFiltered: '(filtrado de un total de _MAX_ Cambio de plans)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/customers/change_plan/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
			d.inittime = $('#change_plan_datetimeinit').val();
			d.endtime = $('#change_plan_datetimeend').val();
		}
	},
	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 1, 2, 3, 4, 5]
		},
		{
			render: function(data, type, row) {
				/** Se coloco la diferencia en horas que ha ha sido creado el registro.  */
				return 'Hace ' + moment().diff(data, 'hours') + ' horas';
				//moment( data ).tz(moment.tz.guess()).subtract(10, 'days').calendar()
			},
			targets: 0
		}
	],
	columns: [{ name: 'created' }, { name: 'creator' }, { name: 'service' }, { name: 'old_plan' }, { name: 'new_plan' }, { name: 'number_typify' }]
});

/**
 * function current_values
 * @returns {object} con los valores definidos en el vistas
 */
function current_values() {
	return {
		draw: '1',

		'columns[0][data]': '0',
		'columns[0][name]': 'endtime',
		'columns[0][searchable]': 'true',
		'columns[0][orderable]': 'true',
		'columns[0][search][value]': '',
		'columns[0][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'inittime',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'order[0][column]': '0',
		'order[0][dir]': 'desc',
		start: '0',
		length: '-1',
		'search[value]': $('#download_data_change_plan > label > input').val(),
		'search[regex]': 'false',
		endtime: $('#change_plan_datetimeend').val(),
		inittime: $('#change_plan_datetimeinit').val(),
		_: '1568749802687'
	};
}

/**
 * @JselectorElement {id_element} @module {event} botton para descarga la informacion.
 */
$('#download_data_change_plan').click(function() {
	/**
	 * Se declara una @var {XHLHR}  req
	 */
	var req = new XMLHttpRequest();
	/** se declara una @var urlToSend con formato de la ruta a la cual se solicitara la información */
	var urlToSend = url_base.format('api/v1/customers/change_plan/download_data/') + '?{0}'.format(jQuery.param(current_values()));

	/** Se configura la solicitud del request.  */
	req.open('GET', urlToSend, true);
	req.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	req.responseType = 'blob';
	req.onload = function(event) {
		var blob = req.response;
		var link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		link.download = 'Cambio de plan.xlsx';
		link.click();
		window.URL.revokeObjectURL(link.href);
	};

	/** se tealiza el envio del request con toda la configuracion cargada.  */
	req.send();
});

/** Se agrega formato al @JselectorElemento {class_element} @plugin {datetimepicker} Formatea la fecha y el tiempo */
$('.query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/**
 * @JselectorElement {class_element} @on {event} dp.change se realizan los cambios en
 * @JselectorElement {id_element} @plugin {event} DataTable
 */
$('.query').on('dp.change', function(e) {
	$('#table-change-plan')
		.DataTable()
		.ajax.reload();
});
