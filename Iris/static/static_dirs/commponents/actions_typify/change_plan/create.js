/**
 * @author {Cristian Cantero}
 */

/**
 * @var {string} url_html se guarda la direccion URL  formateada
 * @var {string} url_item se guarda la direccion URL  formateada
 */
var SELECT = $('#new_service_plan');
var URL = url_base.format('api/v1/matrix/plans/?fields=id,name');

/**
 * @function callBack {void}
 */
function callBack() {
	/**
	 * @JselectorElement {id_element} @plugin {conf} select2  se le indica al operado que debe elegir una opcion
	 */
	$('#new_service_plan').select2({
		placeholder: 'Seleccione plan'
	});
	/**
	 * @JselectorElement {id_element} @module {attr} estilo para controlar el tamaño
	 */
	$('#new_service_plan')
		.nextAll()
		.attr('style', 'width: 100% !important');
}

/**
 * Llama a la @function {@param, @param, @param, @param, @param} listItems del @file {index.js}
 */
listItems(SELECT, URL, 'name', 'id', callBack);
