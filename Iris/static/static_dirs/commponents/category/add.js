/**
 * @author {Cristian Cantero}
 */

/**
 * @var {element} new_element
 */
let new_element = $("<ul class='primary line-mode'></ul>");

/**
 * @JselectorElement {event} Carga despues el nuevo elemento
 */
$('#add_filters').after(new_element);

/** Seccion donde se agrega un nuevo tag, se le indica al usuario que ingrese el mismo.*/
new_element.tagit({
	allowDuplicates: false,
	placeholderText: 'Ingrese nuevos tipos',
	tabIndex: -1
});

/**
 * @JselectorElement {event} Envio de la informacion que esta en formularo de categorias.
 */
$('#form_category_add').submit(function(e) {
	e.preventDefault();
	/** @var {array} form alamacenara todos los datos */
	form = $(this).serializeArray();
	id = form.shift();

	/**
	 * @API [ajax]: El cual se consultan segun los valores que esta defindo en @var  id
	 */
	$.ajax({
		type: 'GET',
		url: url_base.format('api/v1/tickets/category/{0}/'.format(id['value'])),
		success: function(data) {
			/** @namespace {object} response declarado vacio  */
			var response = {};

			/** Se configura la @namespace */
			response['operator'] = data['operator'];
			response['classification'] = form.shift().value;
			response['parent_id'] = data['ID'];

			form.forEach(function(item) {
				/** @namespace {object} response donde se almacenara los datos   */
				response['name'] = item['value'];
				/**
				 * @API [ajax]: Se envian los datos de @namespace  resposnse
				 */
				$.ajax({
					type: 'POST',
					url: url_base.format('api/v1/tickets/category/'),
					data: response,
					success: function(data) {
						/** @plugin {notfication} Se le notifiaca el resultado al operador */
						$.gritter.add({
							title: 'Resultado:',
							text: 'La tipificacion {0} agregada'.format(response['name']),
							sticky: false,
							time: ''
						});
						/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
						$('#table-category')
							.DataTable()
							.ajax.reload();
					},
					error: function(error) {
						/** @plugin {notfication} Se le notifiaca el resultado al operador */
						$.gritter.add({
							title: 'Resultado:',
							text: 'No se pudo crear {0}'.format(response['name']),
							sticky: false,
							time: ''
						});
					}
				});
			});

			/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
			$('#table-category')
				.DataTable()
				.ajax.reload();
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Ocurrio un error',
				sticky: false,
				time: ''
			});
		}
	});
});
