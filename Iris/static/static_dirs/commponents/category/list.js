/**
 * @author {Cristian Cantero}
 * @file { Configuracion - category_list }
 */

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-category').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Categoria',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Categoria de _START_ a _END_ de un total de _TOTAL_ Categoria',
		sInfoEmpty: 'Mostrando Categoria del 0 al 0 de un total de 0 Categoria',
		sInfoFiltered: '(filtrado de un total de _MAX_ Categoriaes)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},

		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/category/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
		}
	},
	columns: [
		{ name: 'ID' },
		{ name: 'name' },
		{ name: 'parent' },
		{ name: 'classification' },
		{ name: 'show' },
		{ name: 'edit' },
		{ name: 'delete' },
		{ name: 'subType' }
	],
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<a onclick='show_tree({0})' class='btn btn-sm btn-success' data-toggle='modal' style='color:white;'>Ver</a>";
				return html.format(data);
			},
			targets: 4
		},
		{
			render: function(data, type, row) {
				html =
					"<div class='with-btn' nowrap='' ><a onclick='change_values({0})' class='btn btn-sm btn-primary width-80 m-r-2' style='color:white;'>Cambiar</a><div>";
				return html.format(data);
			},
			targets: 5
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='removeItem(`{0}`, `{1}`)' class='btn btn-sm btn-success' data-toggle='modal' style='color:white;'>Borrar</a>";
				url = url_base.format('api/v1/tickets/category/{0}/'.format(data));
				dataTable = '#table-category';
				return html.format(url, dataTable);
			},
			targets: 6
		},
		{
			render: function(data, type, row) {
				html =
					"<div class='with-btn' nowrap='' ><a onclick='add({0})' class='btn btn-sm btn-primary width-80 m-r-2' style='color:white;'>Agregar</a><div>";
				return html.format(data);
			},
			targets: 7
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 1, 2, 3]
		},
		{
			orderable: false,
			searchable: false,
			targets: [4, 5, 6, 7]
		}
	]
});

/**
 * @function show_tree
 * @param {number} id
 * @JselectorElement {id_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar
 */
function show_tree(id) {
	$('#modal-html').empty();
	/**
	 * @API [ajax]: Se consultan segun los valores que esta defindo en el @param  id
	 */
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: url_base.format('api/v1/tickets/category/{0}/tree/'.format(id)),
		complete: function(data) {
			/** @JselectorElement {id_element} @module {html}*/
			$('#modal-html').append('<div id="tree-simple" class="chart"> </div>');

			/** @namespace {object} chart_config  */
			chart_config = {
				chart: {
					container: '#tree-simple',
					levelSeparation: 60,
					rootOrientation: 'NORTH',
					nodeAlign: 'BOTTOM',
					siblingSeparation: 50,
					subTeeSeparation: 40,
					hideRootNode: false,
					scrollbar: 'fancy',
					padding: 15,
					connectors: {
						type: 'step',
						style: {
							'stroke-width': 2.5
						},
						stackIndent: 15
					},
					node: {
						HTMLclass: 'big-commpany',
						collapsable: true
					}
				},
				nodeStructure: data.responseJSON
			};

			/** @JselectorElement {id_element} @module {html}*/
			$('#modal').modal('show');

			/** @instance Treant con el @namespace chart_config */
			tree = new Treant(chart_config);
		}
	});
}

/**
 * @function add agregar
 * @param {*} id
 */
function add(id) {
	/**
	 * @API [ajax]: Se consultan segun los valores que esta defindo en el @JselectorElement {val} y @JselectorElement {val}
	 */
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url_base_templates.format('Iris/category_add/{0}/{1}/'.format($('#token_user').val(), $('#menu_operators').val())),
		success: function(body_html) {
			/**
			 * @JselectorElement {id_element} @module se estrablece el elemento
			 */
			let content = $('#modal-html');
			content.empty();
			content.html(body_html);

			/**
			 * @JselectorElement {id_element} @module {val} se configura el valor para el elemento
			 */
			$('#add_category_parent').val(id);

			/**
			 * @API [ajax]: Se consultan segun los valores que esta defindo en el @param id
			 */
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: url_base.format('api/v1/tickets/category/{0}/'.format(id)),
				complete: function(data) {
					data = data.responseJSON;
					/** se agrega la clasifiacion de la categoria en la vista */
					$('#add_category_classification').val(data['classification'] + 1);
					/** se muestra en el modal */
					$('#modal').modal('show');
				}
			});
		},
		error: function(error) {
			//TODO: vacia
		}
	});
}

/**
 * @function change_values cambios de valores
 * @param {number} id
 */
function change_values(id) {
	/**
	 * @API [ajax]: Se consultan segun los valores que esta defindo en el @JselectorElement {val} y @JselectorElement {val}
	 */
	$.ajax({
		type: 'GET',
		dataType: 'html',
		url: url_base_templates.format('Iris/category_change/{0}/{1}/'.format($('#token_user').val(), $('#menu_operators').val())),
		success: function(body_html) {
			/**
			 * @JselectorElement {id_element} @module se estrablece el elemento
			 */
			let content = $('#modal-html');
			content.empty();
			content.html(body_html);
			/**
			 * @API [ajax]: Se consultan segun los valores que esta defindo en el @param id
			 */
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: url_base.format('api/v1/tickets/category/{0}/'.format(id)),
				complete: function(data) {
					data = data.responseJSON;
					debugger
					/** se establecen los valores que seran mostrados en la vista */
					$('#change_category_sla').val(data['sla']);
					$('#change_category_name').val(data['name']);
					$('#change_category_id').val(data['ID']);
					$("#change_category_classification option[value='{0}']".format(data['classification'])).prop('selected', true);
					$("#change_category_classification option[value='{0}']".format(data['classification'])).trigger('change')

					if (data['parent']) {
						/**
						 * @API [ajax]: Se consultan segun los valores que esta defindo en el @var data
						 */
						$.ajax({
							type: 'GET',
							dataType: 'json',
							url: data['parent'],
							complete: function(data) {
								data = data.responseJSON;
								/** se establece el valor que se mostrara en la vista. formateada y seleccionada */
								$("#change_category_parent option[value='{0}']".format(data['ID'])).prop('selected', true);
								$("#change_category_parent option[value='{0}']".format(data['ID'])).trigger('change')
							}
						});
					}

					/** se muestra el modal */
					$('#modal').modal('show');
				}
			});
		},
		error: function(error) {
			//TODO: vacia
		}
	});
}

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
	$('#table-category')
		.DataTable()
		.ajax.reload();
});
