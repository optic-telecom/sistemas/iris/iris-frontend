/**
 * @author {Cristian Cantero}
 *  @file { typify_create, typify_create_queue, typify_opened, typify_create_me, typify_last_hours }
 */

/**
 * Variables definidas para los filtros.
 * @var {array} types
 * @var {array} filters
 * @var {object} filters_transition
 * @var {object} filters_transition
 * @var {object} filter_type_transition
 */
var types = []; //TODO: No esta seiendo usuado.
var filters = []; //TODO: No esta seiendo usuado.
var filters_transition = {}; //TODO: No esta seiendo usuado.
var filters_types = {}; //TODO: No esta seiendo usuado.
var filter_type_transition = {}; //TODO: No esta seiendo usuado.

/**
 * Variables que son usadas para como banderas.
 * @var {bool} add_flag //TODO: No esta seiendo usuado.
 * @var {bool} sentForm //TODO: No esta seiendo usuado.
 * @var {bool} preview //TODO: No esta seiendo usuado.
 *
 * Variables que de tipo objetos usadas de forma global.
 *@var {object} DATA_CUSTOMER
 *@var {object} DATA_SERVICES
 *
 */
var add_flag = true;
var sentForm = false;
var preview = false;
var DATA_CUSTOMER = {};
var DATA_SERVICES = {};

/**
 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
 */

/**
 * @function { @param } actions
 * @param {number} number_typify
 */
function actions(number_typify) {
	/**
	 * @function {void} feasibility  limpia el @JselectorElement {selector } modal-html
	 *  Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
	 */
	function feasibility() {
		/** @JselectorElement {class_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
		$('#modal-html').empty();
		$('#modal-title').text('Factibilidad');
		callView('feasibility_create', $('#modal-html'));
		$('#modals').show();
	}

	/**
	 * @function {void} new_service Mostrara un modal para un nuevo servicio
	 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
	 */
	function new_service() {
		/** @JselectorElement {class_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
		$('#modal-html').empty();
		$('#modal-title').text('Nuevo servicio');
		callView('new_service_create', $('#modal-html'));
		$('#modals').show();
	}

	/**
	 * @function {void} change_plan Mostrara un modal para cambiar el plan
	 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
	 */
	function change_plan() {
		/** @JselectorElement {class_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
		$('#modal-html').empty();
		$('#modal-title').text('Cambio de plan');
		callView('change_plan_create', $('#modal-html'));
		$('#modals').show();
	}

	/**
	 * @function {void} change_headline Se mostrara un modal para cambiar el de titular.
	 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
	 */
	function change_headline() {
		/** @JselectorElement {class_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
		$('#modal-html').empty();
		$('#modal-title').text('Cambio de titular');
		callView('change_headline_create', $('#modal-html'));
		$('#modals').show();
	}

	/**
	 * @namespace {objet} DICT_ACTIONS Objeto que contiene el menu de las aciones.
	 */
	var DICT_ACTIONS = {
		'1.1.1': feasibility,
		'1.1.7': feasibility,
		'2.1.1': new_service,
		'2.1.3': change_plan,
		'2.1.10': change_headline
	};

	/** Se verifica que el @var {number} number typify este en el index del @namespace DICT_ACTIONS  */
	if (number_typify in DICT_ACTIONS) {
		DICT_ACTIONS[number_typify]();
	}
}

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#category_type').select2({
	placeholder: 'Seleccione un tipo'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#category_type')
	.nextAll()
	.attr('style', 'width: 100% !important');

/**
 *@JselectorElement {id_element} @module{ event} cambio de las tipicacion
 */
$('#category_type').change(function() {


	/** @JselectorElement {id_element} @module {event} limpia el componente del select */
	$('#category_category').empty();

	/** @JselectorElement {id_element} @module {html} agrega las opciones del componente select  */
	$('#category_category').append('<option></option>');

	/**
	 * @callback @function {@JselectorElement, @API, @param, @param}
	 * listItems donde se cargan se los opcion segun el id. @file { index.js}
	 */
	listItems(
		$('#category_category'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#category_category').select2({
	placeholder: 'Seleccione una categoria'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#category_category')
	.nextAll()
	.attr('style', 'width: 100% !important');

/**
 * @JselectorElement {id_element} @module {event}
 */
$('#category_category').change(function() {
	/**
	 * @JselectorElement {class_element} @module {prop} donde se requiere la categoria
	 * @JselectorElement {id_element} @module {prop} doene es requerido la categoria
	 * @JselectorElement {id_element} @module {event} se limpia el componente select
	 * @JselectorElement {id_element} @module {html} se le agrega el las opcion vacia al select
	 *
	 */
	$('#category_subcategory').empty();
	$('#category_subcategory').append('<option></option>');

	/**
	 * @callback @function {@JselectorElement, @API, @param, @param}
	 * listItems donde se cargan se los opcion segun el id. @file { index.js}
	 */
	listItems(
		$('#category_subcategory'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#category_subcategory').select2({
	placeholder: 'Seleccione una subcategoria'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#category_subcategory')
	.nextAll()
	.attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @module {event} el cual se accionara de existir un cambio en las subcategoria */
$('#category_subcategory').change(function() {
	/** @callback @function {@JselectorElement} que llama la accion de la categias */
	actions(
		$('#category_subcategory option:selected')
			.text()
			.split(' ')[0]
	);

	$('#category_second_subcategory').empty();
	$('#category_second_subcategory').append('<option></option>');

	/**
	 * @callback @function {@JselectorElement, @API, @param, @param}
	 * listItems donde se cargan se los opcion segun el id. @file { index.js}
	 */

	function validate_exist_second_sub(){

		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: url_base.format('api/v1/tickets/category/?fields=ID&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $('#category_subcategory').val())),
			complete: function(data) {
				debugger
				if (data.responseJSON.length){

					$('#category_subcategory').removeAttr('name');

					$('#category_second_subcategory').attr('name', 'category');
					$('#category_second_subcategory').prop('required', true);
					$('#category_second_subcategory').parent().show()

				}else{

					$('#category_second_subcategory').parent().hide()
					$('#category_second_subcategory').removeAttr('name');
					$('#category_second_subcategory').prop('required', false);
					$('#category_subcategory').attr('name', 'category');

				}

			}
		});

	}


	listItems(
		$('#category_second_subcategory'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID',
		validate_exist_second_sub,
	);

});

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#category_second_subcategory').select2({
	placeholder: 'Seleccione una segunda subcategoria'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#category_second_subcategory')
	.nextAll()
	.attr('style', 'width: 100% !important')
	.parent().hide();

	/** @JselectorElement {id_element} @module {event} el cual se accionara de existir un cambio en las segunda subcategoria */
$('#category_second_subcategory').change(function(){

	actions(
		$('#category_second_subcategory option:selected')
			.text()
			.split(' ')[0]
	);
	
})

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_discount_reason').select2({
	placeholder: 'Seleccione un motivo'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_discount_reason')
	.nextAll()
	.attr('style', 'width: 100% !important');


/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_discount_responsable_area').select2({
	placeholder: 'Seleccione un area responsable'
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_discount_responsable_area')
	.nextAll()
	.attr('style', 'width: 100% !important');

/** @JselectorElement {class_element} @module {event} @function*/
$('#typify_management').change(function() {
	/** @var {boolean} pass  */

	if ($(this).val() == 'NINGÚNA') {
		/**
		 * @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del tecnico
		 * @JselectorElement {id_element} @module {prop} donde no es requerido la tipificacion del tecnico.
		 */
		$('#typify_technician').prop('disabled', 'disabled');
		$('#typify_technician').prop('required', false);
	} else {
		/**
		 * @JselectorElement {id_element} @module {prop} donde se habilita las tipificaciones del tecnico
		 * @JselectorElement {id_element} @module {prop} donde es requerido la tipificacion del tecnico.
		 */
		$('#typify_technician').prop('disabled', false);
		$('#typify_technician').prop('required', true);
	}
	
});

/**
 * @callback @function {@JselectorElement, @API, @param, @param}
 * listItems donde se cargan se los opcion segun el id. @file { index.js}
 */

listItems($('#typify_technician'), url_base.format('api/v1/matrix/technicians/?fields=id,name'), 'name', 'id');

/** @JselectorElement {class_element} @module {event} @function*/
$('#typify_customer_type').change(function() {
	/** Se verifica el valor del @JselectorElement */
	if ($('#typify_customer_type').val() == 'Cliente') {
		/**
		 * @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del cliente
		 * @JselectorElement {id_element} @module {prop} donde no es requerido la tipificacion del cliente
		 * @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del servicio
		 * */
		$('#typify_customer').prop('disabled', false);
		$('#typify_customer').prop('required', true);
		$('#typify_services').prop('disabled', false);
	} else {
		/**
		 * @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del cliente
		 * @JselectorElement {id_element} @module {prop} donde no es requerido la tipificacion del cliente
		 * @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del servicio
		 * */
		$('#typify_customer').prop('disabled', 'disabled');
		$('#typify_customer').prop('required', false);
		$('#typify_services').prop('disabled', 'disabled');
	}
});

/**
 *Funcion donde se cargaran los datos en el modal.
 * @param {@namespace} data
 */
function showOpenTipify(data) {
	/** @JselectorElement {class_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
	$('#modal-html').empty();

	/** @JselectorElement {class_element} @module {html} Se le agrega un titulo de preparacio del modal */
	$('#modal-title').text('Tipificaciones abiertas');

	/**
	 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
	 * */
	callView('typify_open', $('#modal-html'));

	/**
	 *  @JselectorElement @plugin dt_table[@todo]  Se pasan las configuraciones del de la tabla que se muestra.
	 */
	$('#typifysOpenDataTable').DataTable({
		data: data,
		language: {
			sLengthMenu: 'Mostrar _MENU_ Tipificaciones',
			sZeroRecords: 'No se encontraron resultados',
			sEmptyTable: 'Ningún dato disponible en esta tabla',
			sInfo: 'Mostrando Tipificaciones Abiertas de _START_ a _END_ de un total de _TOTAL_ registro',
			sInfoEmpty: 'Mostrando Historico del 0 al 0 de un total de 0 Historico',
			sInfoFiltered: '(filtrado de un total de _MAX_ Registro)',
			sInfoPostFix: '',
			sSearch: 'Buscar:',
			sUrl: '',
			sInfoThousands: ',',
			sLoadingRecords: 'Cargando...',
			sProcessing: 'Cargando...',
			oPaginate: {
				sFirst: 'Primero',
				sLast: 'Último',
				sNext: 'Siguiente',
				sPrevious: 'Anterior'
			},
			oAria: {
				sSortAscending: ': Ordenar ascendente',
				sSortDescending: ': Ordenar descendente'
			}
		},
		columnDefs: [
			{
				/** Se muestra la horas que han transcurrido desde que se creo el registro */
				render: function(data, type, row) {
					html = "<a href='javascript:;' style='cursor: pointer;' onclick='show_typify(`{0}`)'>{0}</a>";
					return html.format(data);
				},
				targets: 0
			}
		],
		destroy: true
	});
	/** Se muestra el modal. */
	$('#modals').show();
}

/** @JselectorElement {class_element} @module {event} @function*/
$('#typify_customer').change(function() {
	/** @var {string} rut */
	var rut = $('#typify_customer').val();

	/** @API [ajax=get] Se consulta si tiene tipificaciones abiertas. */
	$.get(url_base.format(`api/v1/tickets/typify/new_contact`), { rut: rut }, (data) => {
		if (data.length > 0) {
			/** @callback @function {@namespace} */
			showOpenTipify(data);
		}
	});

	/** se verifica la variable */
	if (rut) {
		var url = url_base.format(
			'api/v1/matrix/services/?fields=number&customer__rut={0}&operator__id={1}'.format($('#typify_customer').val(), $('#menu_operators').val())
		);

		/** @JselectorElement {id_element} @module {html} Limpia el elemento de tipificacion de servicios */
		$('#typify_services').empty();

		/**
		 * @callback @function {@JselectorElement, @API, @param, @param}
		 * listItems donde se cargan se los opcion segun el id. @file { index.js}
		 */
		listItems($('#typify_services'), url, 'number', 'number');

		/**
		 * @callback @function { @param}
		 * listItems donde se cargan se los opcion segun el id. @file {@this}
		 */
		list_services(rut);
	}
});

/** @var {number} TYPE_SEARCH - global*/
var TYPE_SEARCH = 1;

/** @JselectorElement {class_element} @module {event} se captura el evento de los radio button */
$('.inlineCssRadio').change(function() {
	var input = $(this);

	/** Se verifica que el input este selecionado, dependiendo se realizan varias acciones  */
	if (input.is(':checked')) {
		switch ($(this).attr('position')) {
			case '1':
				/** radio RUT */
				TYPE_SEARCH = 1; 
				$('#inlineCssRadio2').prop('checked', false);
				$('#inlineCssRadio3').prop('checked', false);
				$('#inlineCssRadio4').prop('checked', false);

				break;

			case '2':
				/** Radio Nombre */
				TYPE_SEARCH = 2;
				$('#inlineCssRadio1').prop('checked', false);
				$('#inlineCssRadio3').prop('checked', false);
				$('#inlineCssRadio4').prop('checked', false);

				break;

			case '3':
				/** Radio Direccion */
				TYPE_SEARCH = 3;
				$('#inlineCssRadio1').prop('checked', false);
				$('#inlineCssRadio2').prop('checked', false);
				$('#inlineCssRadio4').prop('checked', false);
				break;

			case '4':
				//TODO: Porque esta el 3 y no el 4??
			 	/** Radio Direccion */
				TYPE_SEARCH = 3;
				$('#inlineCssRadio1').prop('checked', false);
				$('#inlineCssRadio2').prop('checked', false);
				$('#inlineCssRadio3').prop('checked', false);
				break;
		}

		/** Se activa el atributo de la posicion del radio. */
		input.attr('position');
	}
});

/** 
 * @JselectorElement {id_element} @plugin {conf} select2
 * Se modificara segun el resultado de lo que se ha ingresado por el input
 * */
$('#typify_customer').select2({
	placeholder: 'Seleccione un cliente',
	minimumInputLength: 1,
	ajax: {
		url: url_base.format('api/v1/matrix/estimate_customer/'),
		dataType: 'json',
		data: function(params) {

			/** @namespace {object} query donde se configuran la busqueda y el criterio*/
			var query = {
				search: params.term,
				criterion: $('#inlineCssRadio1').is(':checked')
					? 'rut': $('#inlineCssRadio2').is(':checked')
					? 'name': $('#inlineCssRadio3').is(':checked')
					? 'composite_address': 'service'
			};

			/** @return query ya configurado */
			return query;
		},
		processResults: function(data) {
			/** @return @namespace {object} la data. */
			return {
				results: data
			};
		}
	},
	language: {
		/** Se aplican cambios segun lo seleccionado lo radio Buttom */
		inputTooShort: function() {
			var type_search = $('#inlineCssRadio1').is(':checked') ? 'rut' : $('#inlineCssRadio2').is(':checked') ? 'nombre' : 'dirección';
			/** @return se imprime el valor del tipo de busqueda. */
			return 'Ingrese {0} , para realizar la busqueda'.format(type_search);
		}
	}
});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_customer').nextAll().attr('style', 'width: 100%');

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_customer').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_services').select2({ placeholder: 'Selecciones un servicio'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_services').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_status').select2({ placeholder: 'Seleccione un estado' });

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_status').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_urgency').select2({placeholder: 'Seleccione urgencia'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_urgency').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_channel').select2({ placeholder: 'Seleccione canal' });

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_channel').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_in_out').select2({ placeholder: 'Seleccione tipo' });

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_in_out').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_city').select2({placeholder: 'Seleccione una ciudad'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_city').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_management').select2({placeholder: 'Gestion'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_management').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_customer_type').select2({placeholder: 'Seleccione tipo de cliente'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_customer_type').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#typify_technician').select2({placeholder: 'Seleccione el técnico'});

/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
$('#typify_technician').nextAll().attr('style', 'width: 100% !important');

/** @JselectorElement {id_element} @module {prop} donde se deshabilita las tipificaciones del tecnico */
$('#typify_technician').prop('disabled', 'disabled');

/** @var {string} url donde se guarda la url donde se consulta la categoria.  */
var url = url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__isnull=True'.format($('#menu_operators').val()));

/**
 * @callback @function {@JselectorElement, @API, @param, @param}
 * listItems donde se cargan se los opcion segun el id. @file { index.js}
 */
listItems($('#category_type'), url, 'name', 'ID');

/**
 * @function postTypify 
 * @param {numbre} ID_typify 
 */
function postTypify(ID_typify) {

	/** @var {@JselectorElement} number_typify se captura la opcion selecionada de la subcategoria */
	var number_typify = $('#category_subcategory option:selected')
		.text()
		.split(' ')[0];

	/**
	 * @function feasibility para cargar la factibilidad.
	 * @param {@JselectorElement} number_typify declarada en la @function @this
	 */
	function feasibility(number_typify) {
		
		/** @var {object} data donde se almacenaran los datos serializado. */
		var data = $('#form-typify-action').serializeArray();

		/** Se agregan al objeto el name y el value del operador */
		data.push({name: 'operator', value: $('#menu_operators').val()});

		/** Se agrega el numero de tipificacion y el valor de Id del mismo  */
		data.push({ name: 'number_typify',value: ID_typify});

		/**
		 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
		 */
		createItem(url_base.format('api/v1/customers/feasibility/'), data, 'Factibilidad creada', 'Ocurrio un error al crear factibilidad');
	}

	/**
	 * @function new_service se carga el nuevos servicios.
	 * @param {@JselectorElement} number_typify declarada en la @function @this
	 */
	function new_service(number_typify) {

		/** @var {object} data donde se almacenaran los datos serializado. */
		var data = $('#form-typify-action').serializeArray();
	
		/** Se agregan al objeto el name y el value del operador */
		data.push({ name: 'operator', value: $('#menu_operators').val()});

		/** Se agrega el numero de tipificacion y el valor de Id del mismo  */
		data.push({name: 'number_typify', value: ID_typify});

		/**
		 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
		 */
		createItem(url_base.format('api/v1/customers/new_service/'), data, 'Nuevo servicio registrado', 'Ocurrio un error al crear nuevo servicio');
	}

	/**
	 * @function change_plan se carga el cambio de plan
	 * @param {@JselectorElement} number_typify declarada en la @function @this
	 */
	function change_plan(number_typify) {

		/** @var {object} data donde se almacenaran los datos serializado. */
		var data = $('#form-typify-action').serializeArray();

		/** Se agregan al objeto el name y el value del operador */
		data.push({ name: 'operator', value: $('#menu_operators').val()});

		/** Se agrega el numero de tipificacion y el valor de Id del mismo  */
		data.push({name: 'number_typify', value: ID_typify});

		$('#typify_services')
			.val()
			.forEach(function(item, index) {
				/** @API[ajax] donde se consutla la informacion en matrix por recorrido [servicios]  */
				$.ajax({
					type: 'GET',
					url: url_base.format('api/v1/matrix/services/{0}/'.format(item)),
					args: {
						data: data.slice()
					},
					success: function(data) {

						/** @var {@namespace} data_sent  configuracion de la data para crear el item*/
						data_sent = this.args['data'];
						/** Se agregan al objeto el name  del viejo plan  y el value del plan */
						data_sent.push({ name: 'old_plan', value: data['plan'].split('plans/')[1].replace('/', '')});
						/** Se agregan al objeto el name del servicio y el value id del servicio */
						data_sent.push({ name: 'service', value: data['id']});

						/** @var {string} url se almacena la ruta donde ira el cambio de plan */
						let url = url_base.format('api/v1/customers/change_plan/');

						/**
						 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
						 */
						createItem(url, data_sent, 'Cambio de plan realizado', 'Error al cambiar plan');
					},
					error: function(error) {
						/** @plugin {notfication} Se le notifiaca el resultado al operador */
						$.gritter.add({
							title: 'Resultado:',
							text: error,
							sticky: false,
							time: ''
						});
					}
				});
			});
	}

	/**
	 * @function change_headline se carga el cambio de dueño
	 * @param {@JselectorElement} number_typify declarada en la @function @this
	 */
	function change_headline(number_typify) {

		/** @var {object} data donde se almacenaran los datos serializado. */
		var data = $('#form-typify-action').serializeArray();

		/** Se agregan al objeto el name y el value del operador */
		data.push({name: 'operator', value: $('#menu_operators').val()});

		/** Se agrega el numero de tipificacion y el valor de Id del mismo  */
		data.push({ name: 'number_typify', value: ID_typify});

		/** Se agregan el antiguo cliente y el value del nuevo cliente */
		data.push({ name: 'old_customer', value: $('#typify_customer').val()});

		$('#typify_services')
			.val()
			.forEach(function(item, index) {

				/** @var {@namespace} data_sent  configuracion de la data para crear el item*/
				var data_sent = data.slice();

				/** Se agregan al objeto el name del servicio y el value del item */
				data_sent.push({ name: 'service', value: item});

				/** @var {string} url se almacena la ruta donde ira el cambio de cliente */
				url = url_base.format('api/v1/customers/change_headline/');

				/**
				 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
				 */
				createItem(url, data_sent, 'Cambio de plan realizado', 'Error al cambiar plan');
			});
	}

	/** @namespace {object} DICT_ACTIONS Diccionario de aciones. */
	var DICT_ACTIONS = {
		'1.1.1': feasibility,
		'1.1.7': feasibility,
		'2.1.1': new_service,
		'2.1.3': change_plan,
		'2.1.10': change_headline
	};

	/** se consulta el diccionario de accion si existe el numero de la tipificacion */
	if (number_typify in DICT_ACTIONS) {
		DICT_ACTIONS[number_typify](number_typify);
	}
}

/**
 * @JselectorElement {id_element} @module {event} despues de enviar la data del formulario. 
 * Esto se aplica cuando se en el @tag {nuevo} typify 
 */
$('#typify_form').submit(function(event) {

	/**
	 * @function clearForm donde se limpia y ponen los valores por defecto en el formulario
	 */
	function clearForm() {
		$('#typify_customer_type').val('Cliente').trigger('change');
		$('#typify_customer').val(null).trigger('change');
		$('#typify_services').val(null).trigger('change');

		$('#category_type').val(null).trigger('change');
		$('#category_category').val(null).trigger('change');
		$('#category_subcategory').val(null).trigger('change');

		$('#typify_liveagent').val(null).trigger('change');
		$('#typify_channel').val(null).trigger('change');
		$('#typify_in_out').val(null).trigger('change');

		$('#typify_status').val(null).trigger('change');
		$('#typify_urgency').val(null).trigger('change');
		$('#typify_city').val(null).trigger('change');

		$('#typify_management').val('NINGÚNA').trigger('change');
		$('#typify_technician').val(null).trigger('change');

		$('#typify_discount_reason').val('NINGUNO').trigger('change');
		$('#typify_discount_responsable_area').val('NINGUNO').trigger('change');
		$('#typify_discount_porcentage').val(0).trigger('change');

		$('#typify_commentary').val('').trigger('change');

	}

	event.preventDefault();

	/** @var {@namespace} result donde se almacenaran todos los datos del formulario  */
	var result = {};

	/** @var {array} data que esta registrada en el formulario */
	var data = $('#typify_form').serializeArray();

	/** @var {string} services declarado por defecto vacio. */
	var services = '';

	/** Se distribuyen la informacion en los siguientes  */
	data.forEach(function(item, index) {

		switch (item['name']) {
			case 'services[]': /** Se agregan los servicios */
				services += services == '' ? item['value'] : ',' + item['value'];
				break;

			case 'category': /** Se agrega la categoria */
				result['category_id'] = item['value'];
				break;

			case 'commentary': /** se agrega el comentario */
				result['commentary'] = item['value'] ? item['value'] : 'Sin comentario';
				break;

			case 'liveagent': /** se agrega el liveagent */
				result['liveagent'] = item['value'] ? item['value'] : 'Sin ticket';
				break;

			default: /** se agrega el nombre */
				result[item['name']] = item['value'];
		}
	});

	/** Se agrega los servisios al objeto service */
	result['services'] = services;

	/** Se agrega el cliente  */
	result['customer'] = $('#typify_customer').val();

	/** Se agrega el operador */
	result['operator'] = $('#menu_operators').val();

	/** Se agrega un @namespace {object} al @var result  de la primera data del cliente*/
	result['first_data_customer'] = {};
	result['first_data_customer']['customer'] = DATA_CUSTOMER['customer'];
	result['first_data_customer']['services'] = DATA_SERVICES;

	/** @API[ajax] donde se envia los datos por metio de la url.  */
	$.ajax({
		type: 'POST',
		dataType: 'json',
		contentType: 'application/json',
		url: url_base.format('api/v1/tickets/typify/'),
		data: JSON.stringify(result),
		success: function(data) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Tipificacion creada',
				sticky: false,
				time: ''
			});

			/** @JselectorElement {id_element} @module {html} donde se le muestra la tipificacion creada */
			$('#final-message').html('Tipificacion creada: {0}'.format(data['ID']));
			/** @JselectorElement {id_element} @function {} DataTable para recargar la informacion */
			$('#table-typify').DataTable().ajax.reload();

			postTypify(data['ID']);

			var data_comment = {
				message: $('#typify_commentary').val(),
				chat: data['commentaries']
			};

			/** @API[ajax] donde se envia los datos por comunicacion interna [chat] */
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: url_base.format('api/v1/communications/internal_chat/'),
				data: data_comment
			});

			/** se llama la @function clearForm */
			clearForm();
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Error al crear tipificacion',
				sticky: false,
				time: ''
			});
		}
	});
});

/**
 * @function list_services
 * @param {string} rut es la identificacion del cliente
 *  Se listan los servicios en la informacion del cliente
 */
function list_services(rut) {
	/** @JselectorElement {id_element} se remueve el menu de servicio de la inforrmacion del cliente. */
	$('#service-menu-header')
		.nextAll()
		.remove();

	/** @JselectorElement {id_element} se limpia el elemento.. */
	$('#service-menu-header').empty();

	/**@var {string}  html_button  */
	var html_button =
		'<li class="nav-items">' +
		'<a href="#nav-pills-tab-{0}" data-toggle="tab" class="nav-link">' +
		'<span class="d-sm-none">{1}</span>' +
		'<span class="d-sm-block d-none">{2}</span>' +
		'</a>' +
		'</li>';

	/**@var {string}  html_table  */
	var html_table =
		'<div class="tab-content tab-pane fade" id="nav-pills-tab-{0}">' +
		'<ul class="nav nav-pills">' +
		'<li class="nav-items">' +
		'<a href="#service-{0}" data-toggle="tab" class="nav-link">' +
		'<span class="d-sm-none">Servicio</span>' +
		'<span class="d-sm-block d-none">Servicio</span>' +
		'</a>' +
		'</li>' +
		'<li class="nav-items">' +
		'<a href="#node-{0}" data-toggle="tab" class="nav-link">' +
		'<span class="d-sm-none">Nodo</span>' +
		'<span class="d-sm-block d-none">Nodo</span>' +
		'</a>' +
		'</li>' +
		'<li class="nav-items">' +
		'<a href="#plan-{0}" data-toggle="tab" class="nav-link">' +
		'<span class="d-sm-none">Plan</span>' +
		'<span class="d-sm-block d-none">Plan</span>' +
		'</a>' +
		'</li>' +
		'</ul>' +
		'<div class="tab-content table-responsive tab-pane fade" id="service-{0}"  style="margin-right: 5px; margin-left: 5px;">' +
		'<table  class="table table-condensed table-bordered" id="table-service-{0}">' +
		'<thead>' +
		'<tr>' +
		'<th>Campo</th>' +
		'<th>Valor</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>' +
		'</div>' +
		'<div class="table-responsive tab-pane fade" id="node-{0}" style="margin-right: 5px; margin-left: 5px; ">' +
		'<table  class="table table-condensed table-bordered" id="table-node-{0}">' +
		'<thead>' +
		'<tr>' +
		'<th>Campo</th>' +
		'<th>Valor</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>' +
		'</div>' +
		'<div class="table-responsive tab-pane fade" id="plan-{0}" style="margin-right: 5px; margin-left: 5px;">' +
		'<table  class="table table-condensed table-bordered" id="table-plan-{0}">' +
		'<thead>' +
		'<tr>' +
		'<th>Campo</th>' +
		'<th>Valor</th>' +
		'</tr>' +
		'</thead>' +
		'<tbody>' +
		'</tbody>' +
		'</table>' +
		'</div>' +
		'</div>';

	/**@var {string}  tr_table  */
	var tr_table = '<tr>' + '<td class="bg-silver-lighter">{0}</td>' + '<td>' + '<a class="editable editable-click">{1}</a>' + '</td>' + '</tr>';

	/**
	 * Variables que de tipo objetos usadas de forma global.
	 *@var {object} DATA_CUSTOMER
	 *@var {object} DATA_SERVICES
	 */
	DATA_CUSTOMER = {};
	DATA_SERVICES = {};

	/** @API[ajax]  donde se consulta el proyecto Matrix la identificación del cliente*/
	$.ajax({
		url: url_base.format('api/v1/matrix/customers/?rut={0}'.format(rut)),
		args: {
			button: html_button,
			tr_table: tr_table
		},
		success: function(data) {
			/**@var {array}  tr_table  */
			var tr_table = this.args['tr_table'];

			/**@var {string}  customer_button  */
			var customer_button =
				'<li class="nav-items">' +
				'<a href="#nav-pills-tab-customer" data-toggle="tab" class="nav-link">' +
				'<span class="d-sm-none">Cliente</span>' +
				'<span class="d-sm-block d-none">Cliente</span>' +
				'</a>' +
				'</li>';

			/**@var {string}  html_table  */
			var html_table =
				'<div class="tab-content tab-pane fade" id="nav-pills-tab-customer">' +
				'<ul class="nav nav-pills">' +
				'<li class="nav-items">' +
				'<a href="#customer-personal" data-toggle="tab" class="nav-link">' +
				'<span class="d-sm-none">Personal</span>' +
				'<span class="d-sm-block d-none">Personal</span>' +
				'</a>' +
				'</li>' +
				'<li class="nav-items">' +
				'<a href="#customer-payorruts" data-toggle="tab" class="nav-link">' +
				'<span class="d-sm-none">Pagadores</span>' +
				'<span class="d-sm-block d-none">Pagadores</span>' +
				'</a>' +
				'</li>' +
				'<li class="nav-items">' +
				'<a href="#customer-payments" data-toggle="tab" class="nav-link">' +
				'<span class="d-sm-none">Pagos</span>' +
				'<span class="d-sm-block d-none">Pagos</span>' +
				'</a>' +
				'</li>' +
				'<li class="nav-items">' +
				'<a href="#customer-invoices" data-toggle="tab" class="nav-link">' +
				'<span class="d-sm-none">Facturas</span>' +
				'<span class="d-sm-block d-none">Facturas</span>' +
				'</a>' +
				'</li>' +
				'</ul>' +
				'<div class="table-responsive tab-pane fade" id="customer-personal"  style="margin-right: 5px; margin-left: 5px;">' +
				'<table  class="table table-condensed table-bordered" id="table-customer-personal">' +
				'<thead>' +
				'<tr>' +
				'<th>Campo</th>' +
				'<th>Valor</th>' +
				'</tr>' +
				'</thead>' +
				'<tbody>' +
				'</tbody>' +
				'</table>' +
				'</div>' +
				'<div class="table-responsive tab-pane fade" id="customer-payorruts"  style="margin-right: 5px; margin-left: 5px;">' +
				'<table  class="table table-condensed table-bordered" id="table-customer-payorruts">' +
				'<thead>' +
				'<tr>' +
				'<th>Rut</th>' +
				'<th>Nombre</th>' +
				'</tr>' +
				'</thead>' +
				'<tbody>' +
				'</tbody>' +
				'</table>' +
				'</div>' +
				'<div class="table-responsive tab-pane fade" id="customer-payments"  style="margin-right: 5px; margin-left: 5px;">' +
				'<table  class="table table-condensed table-bordered" id="table-customer-payments">' +
				'<thead>' +
				'<tr>' +
				'<th>Fecha de pago</th>' +
				'<th>Fecha de deposito</th>' +
				'<th>Cantidad</th>' +
				'<th>¿Manual?</th>' +
				'<th>Tipo</th>' +
				'<th>¿Despejado?</th>' +
				'<th>Comentario</th>' +
				'</tr>' +
				'</thead>' +
				'<tbody>' +
				'</tbody>' +
				'</table>' +
				'</div>' +
				'<div class="table-responsive tab-pane fade" id="customer-invoices"  style="margin-right: 5px; margin-left: 5px;">' +
				'<table  class="table table-condensed table-bordered" id="table-customer-invoices">' +
				'<thead>' +
				'<tr>' +
				'<th>Tipo</th>' +
				'<th>Folio</th>' +
				'<th>Fecha de pago</th>' +
				'<th>Total</th>' +
				'<th>¿Pagado?</th>' +
				'<th>Estado</th>' +
				'<th>Comentario</th>' +
				'</tr>' +
				'</thead>' +
				'<tbody>' +
				'</tbody>' +
				'</table>' +
				'</div>' +
				'</div>';

			/**
			 * @JselectorElement {id_element} @module {html} se muestra los tags
			 * @JselectorElement {id_element} @module {html} se muestra la informacion.
			 */
			$('#service-menu-header').prepend(customer_button);
			$('#service-menu-header').after(html_table);

			/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
			DATA_CUSTOMER['customer'] = data[0];

			/** Se construye la tabla donde se muestra la informacion del persona. */
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Rut', data[0]['rut']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Nombre', data[0]['name']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Tipo', data[0]['kind']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Correo', data[0]['email']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Telefono', data[0]['phone']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Dia de pago', data[0]['default_due_day']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Direccion', data[0]['composite_address']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Comuna', data[0]['commune']));
			$('#table-customer-personal > tbody:last-child').append(tr_table.format('Nota', data[0]['notes']));

			/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
			DATA_CUSTOMER['customer']['commercial_activity'] = null;
			DATA_CUSTOMER['customer']['balance'] = null;
			DATA_CUSTOMER['customer']['payorruts'] = null;
			DATA_CUSTOMER['customer']['payments'] = null;
			DATA_CUSTOMER['customer']['invoices'] = null;

			/** se verifica que la actividad comecial del cliente  */
			if (data[0]['commercial_activity']) {
				/** @API[ajax] donde se consutla la informacion en matrix */
				$.ajax({
					url: url_base.format('api/v1/matrix/{0}'.format(data[0]['commercial_activity'].split('api/v1/').pop())),
					success: function(data) {
						/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
						DATA_CUSTOMER['customer']['commercial_activity'] = data['name'];
						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-customer-personal > tbody:last-child').append(tr_table.format('Actividad comercial', data['name']));
					}
				});
			}

			/** @API[ajax] donde se consutla la informacion en matrix [balance] */
			$.ajax({
				url: url_base.format('api/v1/matrix/balances/?customer__id={0}'.format(data[0]['id'])),
				success: function(data) {
					/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
					DATA_CUSTOMER['customer']['balance'] = data[0]['balance'];
					/** Se construye la tabla donde se muestra la informacion del persona. */
					$('#table-customer-personal > tbody:last-child').append(tr_table.format('Balance', data[0]['balance']));
				}
			});

			/** @API[ajax] donde se consutla la informacion en matrix [Pagos or ruts] */
			$.ajax({
				url: url_base.format('api/v1/matrix/payorruts/?customer__id={0}'.format(data[0]['id'])),
				success: function(data) {
					/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
					DATA_CUSTOMER['customer']['payorruts'] = data;

					data.forEach(function(item) {
						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-customer-payorruts > tbody:last-child').append(tr_table.format(item['rut'], item['name']));
					});

					/**@JselectorElement {Id_element} @plugin {conf} DataTable Se construye la tabla donde se muestra la informacion del persona. */
					$('#table-customer-payorruts').DataTable({
						autoWidth: true,
						keys: true
					});
				}
			});

			/** @API[ajax] donde se consutla la informacion en matrix [Pagos] */
			$.ajax({
				url: url_base.format('api/v1/matrix/payments/?customer__id={0}'.format(data[0]['id'])),
				success: function(data) {
					/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
					DATA_CUSTOMER['customer']['payments'] = data;

					data.forEach(function(item) {
						/**@var {string}  tr_table  */
						var tr_table =
							'<tr>' +
							'<td>' +
							'<a class="editable editable-click">{0}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{1}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{2}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{3}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{4}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{5}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{6}</a>' +
							'</td>' +
							'</tr>';

						/**@var {string} tr  con formato */
						var tr = tr_table.format(
							moment(item['paid_on'])
								.tz(moment.tz.guess())
								.subtract(10, 'days')
								.calendar(),
							moment(item['deposited_on'])
								.tz(moment.tz.guess())
								.subtract(10, 'days')
								.calendar(),
							item['amount'],
							item['manual'] ? 'Si' : 'No',
							item['kind'],
							item['cleared'] ? 'Si' : 'No',
							item['comment']
						);

						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-customer-payments > tbody:last-child').append(tr);
					});

					/**@JselectorElement {Id_element} @plugin {conf} DataTable Se construye la tabla donde se muestra la informacion del persona. */
					$('#table-customer-payments').DataTable({
						autoWidth: true,
						keys: true
					});
				}
			});

			/** @API[ajax] donde se consutla la informacion en matrix [Pagos] */
			$.ajax({
				url: url_base.format('api/v1/matrix/invoices/?customer__id={0}'.format(data[0]['id'])),
				success: function(data) {
					/** @namespace {conf}  DATA_CUSTOMER se carga la informacion del cliente. */
					DATA_CUSTOMER['customer']['invoices'] = data;

					data.forEach(function(item) {
						/**@var {string}  tr_table  */
						var tr_table =
							'<tr>' +
							'<td>' +
							'<a class="editable editable-click">{0}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{1}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{2}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{3}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{4}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{5}</a>' +
							'</td>' +
							'<td>' +
							'<a class="editable editable-click">{6}</a>' +
							'</td>' +
							'</tr>';

						/**@var {string} tr  con formato */
						var tr = tr_table.format(
							item['kind'],
							item['folio'],
							moment(item['due_date'])
								.tz(moment.tz.guess())
								.subtract(10, 'days')
								.calendar(),
							item['total'],
							item['paid'] ? 'Si' : 'No',
							item['status'],
							item['comment']
						);

						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-customer-invoices > tbody:last-child').append(tr);
					});

					/**@JselectorElement {Id_element} @plugin {conf} DataTable Se construye la tabla donde se muestra la informacion del persona. */
					$('#table-customer-invoices').DataTable({
						autoWidth: true,
						keys: true
					});
				}
			});
		}
	});

	/** @API[ajax]  donde se consulta el proyecto Matrix la identificación del cliente*/
	$.ajax({
		url: url_base.format('api/v1/matrix/services/?customer__rut={0}&operator__id={1}'.format(rut, $('#menu_operators').val())),
		args: {
			button: html_button,
			table: html_table,
			'item-table': tr_table
		},
		success: function(data) {
			/**
			 * @var {@JselectorElement } div_plan
			 * @var {@JselectorElement } url_plan_menu
			 * @var {@namespace } args
			 */
			var div_plan = $('#services-menu');
			var ul_plan_menu = $('#service-menu-header');
			var args = this.args;

			data.forEach(function(data, index) {
				/** Se agrega al @JselectorElement @module {html} */
				ul_plan_menu.append(args['button'].format(index, data['number'], data['number']));
				/** Se agrega al @JselectorElement @module {html} */
				div_plan.append(args['table'].format(index));

				/** @namespace {conf}  DATA_SERVICES se carga la informacion del cliente. */
				DATA_SERVICES[data['number']] = data;

				/**@var {array} tr_table  connfiguracion de la tabla */
				var tr_table = args['item-table'];

				/** Se construye la tabla donde se muestra la informacion del persona. */
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Tipo de documento', data['document_type']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Dia de pago', data['due_day']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Estado', data['get_status_display']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('SSID', data['ssid']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Tecnologia', data['get_technology_kind_display']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Direccion', data['composite_address']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Comuna', data['commune']));
				/**@var {array} allow_auto_cut   verificando la informacion */
				var allow_auto_cut = data['allow_auto_cut'] ? 'Si' : 'No';
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('¿Auto corte?', allow_auto_cut));

				/**@var {array} seen_connected  verificando la informacion */
				var seen_connected = data['seen_connected'] ? 'Si' : 'No';
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('¿Visto conectado?', seen_connected));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('MAC de ONU', data['mac_onu']));

				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Estado de red', data['network_status'][1]));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Fecha de activacion', time_to_human(data['activated_on'])));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Fecha de instalacion', time_to_human(data['installed_on'])));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(
					tr_table.format('Fecha de desintalacion', time_to_human(data['uninstalled_on']))
				);
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Fecha de expiracion', time_to_human(data['expired_on'])));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Nota facturacion', data['billing_notes']));
				$('#table-service-{0} > tbody:last-child'.format(index)).append(tr_table.format('Nota interna', data['internal_notes']));

				/** @API[ajax] donde se consutla la informacion en matrix [Red de equipamietos] */
				$.ajax({
					url: url_base.format('api/v1/matrix/network-equipments/?contract__id={0}&primary=True'.format(data['id'])),
					args: {
						index: index,
						number: data['number']
					},
					success: function(data) {
						/**@var {string}  tr_table  */
						var tr_table =
							'<tr>' + '<td class="bg-silver-lighter">{0}</td>' + '<td>' + '<a class="editable editable-click">{1}</a>' + '</td>' + '</tr>';
						/**@var {string}  tr por defecto vacia en esta instacia.  */
						var tr = '';

						/** Se verifica que exista data donde se indica la IP, caso contrario se le informa que al operado que no No Indica */
						if (data.length) {
							tr = tr_table.format('IP', data[0]['ip']);

							/** @namespace {conf}  DATA_SERVICES se carga la informacion del cliente. */
							DATA_SERVICES[this.args['number']]['ip'] = data[0]['ip'];
						} else {
							tr = tr_table.format('IP', 'No indica');

							/** @namespace {conf}  DATA_SERVICES se carga la informacion del cliente. */
							DATA_SERVICES[this.args['number']]['ip'] = 'No indica';
						}

						$('#table-service-{0} > tbody:last-child'.format(this.args['index'])).before(tr);
					}
				});

				/** @API[ajax] donde se consutla la informacion en matrix [Nodo] */
				$.ajax({
					url: url_base.format('api/v1/matrix/{0}'.format(data['node'].split('api/v1/').pop())),
					args: {
						index: index,
						'item-table': tr_table,
						number: data['number']
					},
					success: function(data) {
						/**
						 * @var {string}  tr_table
						 * @var {string}  tr_table
						 * @namespace {conf}  DATA_SERVICES se carga la informacion del cliente.
						 * */
						var index = this.args['index'];
						var tr_table = this.args['item-table'];
						DATA_SERVICES[this.args['number']]['node'] = data;

						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('Codigo', data['code']));
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('Estado', data['status']));
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('Direccion', data['address']));
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('Comuna', data['commune']));
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('Ubicacion', data['location']));

						/**@var {string}  is_building Verificando la informacion.   */
						var is_building = data['location'] ? 'Si' : 'No';
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('¿Es edificio?', is_building));

						/**@var {string}  is_optical_fiber Verificando la informacion.   */
						var is_optical_fiber = data['is_optical_fiber'] ? 'Si' : 'No';
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('¿Es fibra?', is_optical_fiber));

						/**@var {string}  gpon Verificando la informacion.   */
						var gpon = data['gpon'] ? 'Si' : 'No';
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('GPON', gpon));

						/**@var {string}  gepon Verificando la informacion.   */
						var gepon = data['gepon'] ? 'Si' : 'No';
						$('#table-node-{0} > tbody:last-child'.format(index)).append(tr_table.format('GEPON', gepon));
					}
				});

				/** @API[ajax] donde se consutla la informacion en matrix [Plan] */
				$.ajax({
					url: url_base.format('api/v1/matrix/{0}'.format(data['plan'].split('api/v1/').pop())),
					args: {
						index: index,
						'item-table': tr_table,
						number: data['number']
					},
					success: function(data) {
						/**
						 * @var {string}  tr_table
						 * @var {string}  tr_table
						 * @namespace {conf}  DATA_SERVICES se carga la informacion del cliente.
						 * */
						var index = this.args['index'];
						var tr_table = this.args['item-table'];
						DATA_SERVICES[this.args['number']]['plan'] = data;

						/** Se construye la tabla donde se muestra la informacion del persona. */
						$('#table-plan-{0} > tbody:last-child'.format(index)).append(tr_table.format('Nombre', data['name']));
						$('#table-plan-{0} > tbody:last-child'.format(index)).append(tr_table.format('Precio', data['price']));
						$('#table-plan-{0} > tbody:last-child'.format(index)).append(tr_table.format('Categoria', data['get_category_display']));

						/**@var {string}  uf Verificando la informacion.   */
						var uf = data['uf'] ? 'Si' : 'No';
						$('#table-plan-{0} > tbody:last-child'.format(index)).append(tr_table.format('UF', uf));

						/**@var {string}  active Verificando la informacion.   */
						var active = data['active'] ? 'Si' : 'No';
						$('#table-plan-{0} > tbody:last-child'.format(index)).append(tr_table.format('¿Activo?', active));
					}
				});
			});
		}
	});
}

/** @JselectorElement {class_element} @module {html} se oculta el input del paso */
$('.input-step').hide();

/** @JselectorElement {id_element} @module {html} se muestra el paso 1 */
$('#step-1').show();

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#new_contact_typify').select2({
	minimumInputLength: 1,
	/** @API[ajax] donde se consutla la informacion, que es recibida como json.  */
	ajax: {
		url: url_base.format('api/v1/tickets/typify/get_ID/'),
		dataType: 'json',
		data: function(params) {

			/** @namespace {object} query donde se configuran la busqueda y el criterio*/
			var query = {
				search: params.term,
				operator: $('#menu_operators').val()
			};

			/** @return query ya configurado */
			return query;
		},
		/** @return @namespace {object} la data. */
		processResults: function(data) {
			return {
				results: data
			};
		}
	},
	language: {
		inputTooShort: function() {
			return 'Ingrese números de tipificaciones';
		}
	}
});

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#new_contact_channel').select2({
	placeholder: 'Seleccione un canal'
});

/** @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operado que debe seleccionar una opcion */
$('#new_contact_form').submit(function(event) {
	event.preventDefault();

	/**@var {array} data se serializa en un arreglo toda la informacion del nuevo contacto. .   */
	var data = $(this).serializeArray();

	/**@var {string}  url se codifica la url.   */
	var url = url_base.format('api/v1/tickets/new_contact/');

	/** Se agregan al objeto el name y el value del operador */
	data.push({ name: 'operator', value: $('#menu_operators').val() });

	/**
	 * Llama a la @function {@param, @param, @param, @param, @param} createItem del @file {index.js}
	 */
	createItem(url, data, 'Recontacto creado', 'Error al crear recontacto');
});
