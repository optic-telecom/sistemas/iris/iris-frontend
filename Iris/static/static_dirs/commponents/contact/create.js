/**
 * @author {Cristian Cantero}
 *  @file { }
 */

/**
 * @var {function} Fn
 */
var Fn = {
	/** Valida el rut con su cadena completa "XXXXXXXX-X" */
	validaRut: function(rutCompleto) {
		rutCompleto = rutCompleto.split('.').join('');
		rutCompleto = rutCompleto.replace('‐', '-');
		if (!/^[0-9]+[-|‐]{1}[0-9kK]{1}$/.test(rutCompleto)) return false;
		var tmp = rutCompleto.split('-');
		var digv = tmp[1];
		var rut = tmp[0];
		if (digv == 'K') digv = 'k';

		return Fn.dv(rut) == digv;
	},
	dv: function(T) {
		var M = 0,
			S = 1;
		for (; T; T = Math.floor(T / 10)) S = (S + (T % 10) * (9 - (M++ % 6))) % 11;
		return S ? S - 1 : 'k';
	}
};

/**
 * @function validate_rut para validad el rut de los clientes
 * @param {string} rut
 */
function validate_rut(rut) {
	result = Fn.validaRut(rut);

	if (!result) {
		/** @plugin {notfication} Se le notifiaca el resultado al operador */
		$.gritter.add({
			title: 'Resultado:',
			text: 'Rut invalido',
			sticky: false,
			time: ''
		});
	}

	return result;
}

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#contact_create_form').submit(function(event) {
	event.preventDefault();
	$('#contact_operator').val($('#menu_operators').val());
	var data = $(this).serializeArray();
	var rut = data[1].value;
	var url_item = url_base.format('api/v1/tickets/contact/?rut={0}'.format(rut));

	if (inputValue($('#contact_rut'), 'Ingrese un rut') && validate_rut(rut) && !existItem(url_item, 'El Contacto ya existe', 'Ocurrio un error')) {
		var url_new = url_base.format('api/v1/tickets/contact/');
		/**
		 * Llama a la @function {@param, @namespace, @param, @param, @function } createItem del @file {index.js}
		 */
		createItem(url_new, data, 'Contacto creado', 'Error al crear contacto');
	}
});
