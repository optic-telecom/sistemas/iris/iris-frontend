/**
 * @author {Cristian Cantero}
 *  @file {view_template}
 */

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form_change_template').submit(function(e) {
	e.preventDefault();
	/** @var {array} form */
	form = $(this).serializeArray();
	id = form.shift();
	/** @var {string} url  ruta a la cual seran enviado los datos. */
	url = url_base.format('api/v1/communications/template/{0}/'.format(id['value']));
	/**
	 * Llama a la @function {@param, @namespace, @param, @function} changeItem del @file {index.js}
	 */
	changeItem(url, form, '#table-operator');
});
/**
 * Llama a la @function {@param, @param, @param, @param, @function} listItems del @file {index.js}
 */
listItems($('#change_template_operator'), url_base.format('api/v1/communications/operator/?fields=ID,name'), 'name', 'ID');
