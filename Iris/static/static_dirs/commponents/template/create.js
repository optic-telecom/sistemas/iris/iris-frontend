/**
 * @author {Cristian Cantero}
 *  @file {view_template}
 */

/**
 * @function handleBootstrapWizardsValidation
 */
var handleBootstrapWizardsValidation = function() {
		'use strict';
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'slide',
			transitionSpeed: 600,
			useURLhash: !1,
			showStepURLhash: !1,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			/**
			 * @JselectorElement {id_element} @module {leaveStep} smartWizard cambios de los pasos
			 */
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				if (r == 'backward') {
					return i == 3 ? false : true;
				}

				var result = false;
				/** Se verifica el paso que se deja y se realizan dos caso.  */
				switch (i) {
					case 0:
						if (inputValue($('#template_name'), 'Ingrese nombre')) {
							var url = url_base.format(
								'api/v1/communications/template/?name={0}&operator={1}'.format($('#template_name').val(), $('#menu_operators').val())
							);
							/**
							 * Llama a la @function {@param, @param, @param } existItem del @file {index.js}
							 */
							var existOperator = existItem(url, 'La plantilla ya existe', 'Ocurrio un error');

							if (!existOperator) {
								result = true;
								var name = $('#template_name').val();
								$('#item-value-2').html(name);
							}
						}

						break;

					case 1:
						$('#template_operator').val($('#menu_operators').val());
						if (inputValue($('#template_template'), 'Ingrese plantilla') && inputValue($('#template_subject'), 'Ingrese asunto')) {
							result = true;
							$('#item-value-3').html($('#template_subject').val());
							var data = $('#form-template').serializeArray();
							var url = url_base.format('api/v1/communications/template/');
							/**
							 * Llama a la @function {@param, @param, @param } existItem del @file {index.js}
							 */
							createItem(url, data, 'Plantilla creada', 'Error al crear plantilla');
						}
						break;
					default:
					//TODO vacio!
				}

				return result;
			}),
			/**
			 * @JselectorElement {id_element} @module {keypress} smartWizard cuando se da enter o se presiona el botton next
			 */
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			});

		/**
		 * @JselectorElement {id_element} @module {showStep} smartWizard muestra los pasos
		 */
		$('#wizard').on('showStep', function(t, a, i, r) {
			var step_dict = {};

			step_dict[1] = 'Ingresar nombre';
			step_dict[2] = 'Ingrese plantilla';

			if (r == 'backward') {
				/**
				 * Llama a la @function {@param, @namespace} removeDone del @file {index.js}
				 */
				removeDone(i, step_dict);
			}
		});
	},
	/**
	 * @function FormWizardValidation
	 *  Se usa para deplegar la funcion interna.
	 */
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form-template').submit(function(event) {
	event.preventDefault();
	var url = url_base.format('api/v1/communications/template/?name={0}&operator__ID={1}'.format($('#template_name').val(), $('#menu_operators').val()));
	/**
	 * @var {boolean} existOperator
	 * Llama a la @function {@param, @param, @param} createItem del @file {index.js}
	 */
	var existOperator = existItem(url, 'La plantilla ya existe', 'Ocurrio un error');

	/** se verifica que el operado no sea diferente */
	if (!existOperator) {
		$('#template_operator').val($('#menu_operators').val());
		/** @var {array} data */
		var data = $('#form-template').serializeArray();
		/** @var {string} url  ruta a la cual seran enviado los datos. */
		var url = url_base.format('api/v1/communications/template/');
		/**
		 * Llama a la @function {@param, @namespace, @param, @param, @function} createItem del @file {index.js}
		 */
		createItem(url, data, 'Plantilla creada', 'Error al crear plantilla');
	}
});
