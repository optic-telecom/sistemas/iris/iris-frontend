/**
 * @author {Cristian Cantero}
 * @file {  }
 */

/**
 * @function new_contact_change se cambia a nuevo contacto.
 * @param {number} new_contact_id
 */
function new_contact_change(new_contact_id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/inicio/sesion/iniciada/new_contact_change');
	var url_item = url_base.format('api/v1/tickets/new_contact/{0}/'.format(new_contact_id));

	/**
	 * @function post_ajax funcion interna
	 * @namespace {object} data
	 */
	function post_ajax(data) {
		/**
		 * @var {number} id_contact almacenara el ID
		 * @var {string} url_item se guarda la direccion URL  formateada
		 */
		var id_contact = data['ID'];
		var id_typify = data['typify'].split('typify')[1].match(/\d+/)[0];

		/**
		 * Configuracion de los parametros de a mostrar en la vista
		 * @JselectorElement {id_element} @plugin {conf} select2 formateado con el id_contact
		 * @JselectorElement {id_element}  paramatrizado con el valor obtenido en id_typify
		 * @JselectorElement {id_element}  paramatrizado con el valor obtenido de data['channel']
		 * @JselectorElement {id_element}  paramatrizado con el valor obtenido de data['commentary']
		 */
		$('#new_contact_channel_list_change_{0}'.format(id_contact)).select2({});
		$('#id_typify_new_contact_{0}'.format(id_contact)).val(id_typify);
		$('#new_contact_channel_list_change_{0}'.format(id_contact))
			.val(data['channel'])
			.triger('change');
		$('#new_contact_commentary_change_{0}'.format(id_contact)).val(data['commentary']);

		/**
		 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
		 */
		$('#form_change_new_contact_{0}'.format(id_contact)).submit(function(event) {
			/**
			 * @var {array} data almacenara de forma serializada
			 * @var {string} url formateada con el id_contacto
			 */
			var data = $(this).serializeArray();
			var url = url_base.format('api/v1/tickets/new_contact/{0}/'.format(id_contact));

			/**
			 * Llama a la @function {@param, @namespace @plugin, @function} changeItem del @file {index.js}
			 */
			changeItem(url, data, $('#table-new-contact-{0}'.format(id_contact)), $('#table-new-contact-{0}'.format(id_typify)));
		});
	}
	/**
	 * Llama a la @function {@param, @param, @param, @param} changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}
