/**
 * @author {Cristian Cantero}
 *  @file {template: index.js}
 * /
 * 
/**
 * @API [ajax] Se me pasan los datos del operado.
 */
$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/tickets/operator/?fields=ID,name,color_body,color_lateral,color_higher'),
	complete: function(data) {
		data.responseJSON.forEach(function(item, index) {
			if (index == 0) {
				COLOR_BODY = item['color_body'];
				COLOR_SIDEBAR = item['color_lateral'];
				COLOR_HEADER = item['color_higher'];
			}
		});

		/**
		 * @JselectElement {id_element} @module {style} color del body
		 * @JselectElement {id_element} @module {style} color del sidebar
		 * @JselectElement {id_element} @module {style} color de la cabecera
		 **/
		$('#body').css('background', COLOR_BODY);
		$('#sidebar').css('background', COLOR_SIDEBAR);
		$('#header').css('background', COLOR_HEADER);
	}
});

/**
 * @JselectElement dondo se configura el select2 despues de hacer una consulta a la API
 **/
$('#menu_operators').change(function() {
	/**
	 * @var {window} view se captura la url que esta despues de Iris/
	 */
	let view = window.location.href.split('Iris/')[1];

	/** se verifica que @var view sea diferente del inicio */
	if (!view.includes('inicio/')) {
		view = view.split('/')[0];

		values = window.location.href.split('{0}/'.format(view))[1].split('/');
		url_view = url_base_templates.format('{0}/{1}'.format('Iris', view));
		/** Se le da formato a la url del operador */
		url_operator = '{0}/{1}/{2}/'.format(url_view, $('#token_user').val(), $('#menu_operators').val());

		for_init = 2;
		for_limit = values.length - 2;

		for (for_init; for_init <= for_limit; for_init++) {
			/** Se le da formato a la url del operador */
			url_operator += '{0}/'.format(values[for_limit]);
		}
	} else {
		/** Se le da formato a la url del operador */
		url_operator = url_base_templates.format('Iris/inicio/{0}/{1}/'.format($('#token_user').val(), $('#menu_operators').val()));
	}

	/** Se carga en el location la url del operador.   */
	window.location.href = url_operator;
});

/**
 *  @JselectorElement {id_selector} @plugin { conf } select2 se le muestra al operado que debe seleccionar una opcion.
 */
$('#menu_operators').select2({
	placeholder: 'Seleccione operador'
});
