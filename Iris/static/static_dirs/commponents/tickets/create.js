var service_number = -1;

var handleBootstrapWizardsValidation = function() {
		'use strict';
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'slide',
			transitionSpeed: 600,
			useURLhash: !1,
			showStepURLhash: !1,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				if (r == 'backward') {
					return i == 3 ? false : true;
				}

				var result = false;

				switch (i) {
					case 0:
						/** @var {number} number  numero del servicio */
						var number = $('#service_number').val();
						/**
						 * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @var number
						 */
						$.ajax({
							type: 'GET',
							async: false,
							url: url_base.format('api/v1/matrix/contract/?number={0}'.format(number)),
							success: function(data) {
								if (data.length == 1) {
									service_number = number;
									result = true;

									data = data[0];
									debugger;

									$('#modal_service_number').html(data['number']);
									$('#modal_service_activated').html(data['activated_on']);
									$('#modal_service_installed').html(data['installed_on']);
									$('#modal_service_due_day').html(data['due_day']);
									$('#modal_service_status').html(data['get_status_display']);
									$('#modal_service_tecnology').html(data['get_technology_kind_display']);
									$('#modal_service_direction').html(data['composite_address']);
									$('#modal_service_doc_type').html(data['document_type']);
									$('#modal_service_localization').html(data['location']);

									$('#modal_customer_name').html(data['customer']['name']);
									$('#modal_customer_rut').html(data['customer']['rut']);
									$('#modal_customer_balance').html(data['customer']['balance']);
									$('#modal_customer_email').html(data['customer']['email']);
									$('#modal_customer_telephone').html(data['customer']['phone']);
									$('#modal_customer_due_day').html(data['customer']['default_due_day']);
									$('#modal_customer_direction').html(data['customer']['composite_address']);
									$('#modal_customer_location').html(data['customer']['location']);

									$('#modal_node_code').html(data['node']['code']);
									$('#modal_node_group').html(data['node']['group']);
									$('#modal_node_status').html(data['node']['status']);
									$('#modal_node_address').html(data['node']['address']);
									$('#modal_node_street').html(data['node']['street']);
									$('#modal_node_house_number').html(data['node']['house_number']);
									$('#modal_node_commune').html(data['node']['commune']);
									$('#modal_node_location').html(data['node']['location']);
									$('#modal_node_is_building').html(data['node']['is_building'] ? 'Si' : 'No');
									$('#modal_node_is_optical_fiber').html(data['node']['is_optical_fiber'] ? 'Si' : 'No');
									$('#modal_node_gpon').html(data['node']['gpon'] ? 'Si' : 'No');
									$('#modal_node_gepon').html(data['node']['gepon'] ? 'Si' : 'No');

									$('#modal_plan_name').html(data['service']['name']);
									$('#modal_plan_price').html(data['service']['price']);
									$('#modal_plan_get_category_display').html(data['service']['get_category_display']);
									$('#modal_plan_uf').html(data['service']['uf'] ? 'Si' : 'No');
									$('#modal_plan_active').html(data['service']['active'] ? 'Si' : 'No');
								} else {
									/** @plugin {notfication} Se le notifiaca el resultado al operador */
									$.gritter.add({
										title: 'Resultado:',
										text: 'El numero de servicio no existe',
										sticky: false,
										time: ''
									});
								}
							},
							error: function() {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Resultado:',
									text: 'Ocurrio un error',
									sticky: false,
									time: ''
								});
							}
						});

						break;

					case 1:
						var parent = $('#parent').val();
						var brother = $('#brother').val();
						var count = 2;
						if (!parent) {
							parent = -1;
							count--;
						}

						if (!brother) {
							brother = -1;
							count--;
						}
						/**
						 * @API [ajax]: El cual se consulta segun los valores que esta defindo en el 
						 */
						$.ajax({
							type: 'GET',
							async: false,
							url: url_base.format('api/v1/tickets/ticket/?ID__in={0},{1}'.format(parent, brother)),
							success: function(data) {
								if (data.length == count) {
									result = true;
								} else {
									/** @plugin {notfication} Se le notifiaca el resultado al operador */
									$.gritter.add({
										title: 'Resultado:',
										text: 'Alguno de los tickets no existe no existe',
										sticky: false,
										time: ''
									});
								}
							},
							error: function() {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Resultado:',
									text: 'Ocurrio un error',
									sticky: false,
									time: ''
								});
							}
						});

						break;

					case 2:
						$('#typing_operator').val(OPERATOR_GLOBAL);
						var data = $(this)
							.parent()
							.serializeArray();
						data.push({ name: 'chat_id', value: 0 });
						var result = false;
						$.ajax({
							type: 'POST',
							url: url_base.format('api/v1/tickets/ticket/'),
							async: false,
							data: data,
							success: function() {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Resultado:',
									text: 'Elemento, creado exitosamente',
									sticky: false,
									time: ''
								});

								result = true;
							},
							error: function(error) {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Resultado:',
									text: 'Ocurrio un error',
									sticky: false,
									time: ''
								});
							}
						});
						break;

					case 3:
						break;

					default:
				}

				return result;
			}),
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			});
	},
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();

FormWizardValidation.init();

$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/matrix/contract/?fields=number'),
	beforeSend: function(request) {
		request.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	},
	complete: function(data) {
		var source = [];

		source = $.map(data.responseJSON, function(item) {
			return {
				label: item.number,
				value: item.number
			};
		});

		$('#service_number').autocomplete({
			autoFocus: true,
			delay: 2,
			minLength: 2,
			source: source
		});
	}
});

$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/tickets/ticket/?fields=ID'),
	beforeSend: function(request) {
		request.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	},
	complete: function(data) {
		var source = [];

		source = $.map(data.responseJSON, function(item) {
			return {
				label: item.ID,
				value: item.ID
			};
		});

		$('#parent').autocomplete({
			autoFocus: true,
			delay: 2,
			minLength: 2,
			source: source
		});
		$('#brother').autocomplete({
			autoFocus: true,
			delay: 2,
			minLength: 2,
			source: source
		});
	}
});

$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/tickets/ticket/choices/'),
	complete: function(data) {
		html = '<option value={0}>{1}</option>';

		var list = ['origin', 'type_tk', 'stage_tk', 'city', 'urgency'];

		list.forEach(function(key) {
			input = $('#{0}'.format(key));

			data.responseJSON[key].forEach(function(value) {
				html_render = html.format(value, value);
				input.append(html_render);
			});
		});
	}
});

$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/tickets/typing/?fields=ID,name&operator={0}'.format($('#menu_operators').val())),
	beforeSend: function(request) {
		request.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	},
	complete: function(data) {
		html = '<option value={0}>{1}</option>';

		data.responseJSON.forEach(function(value) {
			html_render = html.format(value['ID'], value['name']);
			$('#typing').append(html_render);
		});
	}
});

function showService() {
	$('#modal_service').modal('show');
}
function showCustomer() {
	$('#modal_customer').modal('show');
}
function showNode() {
	$('#modal_node').modal('show');
}
function showPlan() {
	$('#modal_plan').modal('show');
}
