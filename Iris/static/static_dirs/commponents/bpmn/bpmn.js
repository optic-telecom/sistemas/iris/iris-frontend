var diagramUrl = url_base_templates.format('static/bpmn/diagram.bpmn');

// modeler instance
var bpmnModeler = new BpmnJS({
  container: '#canvas',
  keyboard: {
    bindTo: window
  }
});

/* bpmnModeler.get('elementRegistry')
  var eventBus = bpmnModeler.get('eventBus');
  eventBus.on('element.click', function(e) {

        hide_delete_participant(e)
        
    
      });

  bpmnModeler.saveXML({ format: true }, function(err, xml) {

    debugger
  });
*/


/**
 * Open diagram in our modeler instance.
 *
 * @param {String} bpmnXML diagram to display
 */
function openDiagram(bpmnXML) {

    // import diagram
    bpmnModeler.importXML(bpmnXML, function(err) {
  
      if (err) {
        return console.error('could not import BPMN 2.0 diagram', err);
      }
  
      // access modeler components
      var canvas = bpmnModeler.get('canvas');
  
      // zoom to fit full viewport
      canvas.zoom('fit-viewport');

    });
  
  }
  
  
  // load external diagram file via AJAX and open it
  $.get(diagramUrl, openDiagram, 'text');