/**
 * @author {Cristian Cantero}
 * @file { feasibility_lis }
 */

/**
 * @var {string} url se guarda la direccion URL  formateada
 */
var url = url_base.format('api/v1/matrix/plans/?operator={0}'.format($('#menu_operators').val()));

/**
 * Llama a la @function {@param, @param, @param, @param, @param} listItems del @file {index.js}
 */
listItems($('#feasibility_list_plans'), url, 'name', 'id');

/**
 * @JselectorElement {id_element} @plugin {config} datetimepicker se formatea el fecha y tiempo [fin]
 */
$('#feasibility_list_plans').select2({
	placeholder: 'Seleccione plan'
});

/** @JselectorElement {id_element} @module {attr} estilo el cual mantiene al tamaño de la pantalla  segun su resolucion */
$('#feasibility_list_plans')
	.nextAll()
	.attr('style', 'width: 100% !important');
