/**
 * @author {Cristian Cantero}
 *  @file { typify_create, typify_queue, typify_opened, typify_create_me, typify_last_hours }
 * /

/**
 * @function {loadHistory } Esta funcion cargara las historia en la vistas. 
 * @param {selector al cual seran cargado los datos } id_load
 * @param {URL a consultar en la API} url
 * @param {*} human_field
 * @param {*} readable_field
 */
function loadHistory(id_load, url, human_field, readable_field) {
	/**
	 * @instance
	 * @API [ajax]: Se consultan segun los valores que esta defindo en el @param url
	 */
	$.ajax({
		url: url,
		success: function(data) {
			/**
			 * @var {variable que estara disponible despues del cambio} changeHTML
			 */
			changeHTML = `<article class="timeline-entry {0}">
                                <div class="timeline-entry-inner">
                                    <time class="timeline-time"><span>{1}</span> <span>{2}</span> </time>

                                    <div class="timeline-icon bg-success">
                                        <i class="entypo-feather"></i>
                                    </div>

                                    <div class="timeline-label">
                                        <h2><span>{4}</span></h2>
                                        {3}
                                    </div>
                                </div>
                            </article>`;

			/**
			 * @var { Variable inicial } initialHTML
			 */
			initialHTML = `<article class="timeline-entry left-aligned">
                                <div class="timeline-entry-inner ">
                                    <time class="timeline-time"><span>{0}</span> <span>{1}</span></time>

                                    <div class="timeline-icon bg-success">
                                        <i class="entypo-feather"></i>
                                    </div>

                                    <div class="timeline-label">
                                        <h2><span>Creación </span></h2>
                                        {2}
                                    </div>

                                </div>
                            </article>`;
			/**
			 * @var {*} itemInitial
			 * @var {type:array} dataInitial
			 * @var {type:string} initial
			 *
			 */
			itemInitial = "<span style='display:block;'><b> {0} </b>: {1}</span>";
			dataInitial = data['first'];
			initial = '';

			/** Ciclo for que se ejecutara mientas exista datos en
			 * @var {} field
			 * dentro  da*/
			for (field in dataInitial) {
				if (!['created', 'updated'].includes(field) && human_field[field]) {
					initial += itemInitial.format(human_field[field], readable_field(field, dataInitial[field]));
				}
			}

			/**
			 * @function {@param } Se usa para cambiar tiempo a cadena de caracteres de la fecha.
			 * @param {type:date} date
			 */
			function time_to_string_date(date) {
				return '{0}/{1}/{2}'.format(date.getDay(), date.getMonth(), date.getFullYear());
			}

			/**
			 * @function {@param } Se usa para cambiar tiempo a cadena de caracteres del tiempo.
			 * @param {type:date} date
			 */
			function time_to_string_time(date) {
				return ' {0}:{1}:{2}'.format(date.getHours(), date.getMinutes(), date.getSeconds());
			}

			/**
			 * @var {type:@JselectorElemente}
			 */
			load_node = $('#{0}'.format(id_load));

			/**
			 * @var {type:@JselectorElemente}
			 *  Se le agregan los componetes que estan conformado con las funciones que transforman en fecha y tiempo
			 */
			load_node.prepend(
				initialHTML.format(time_to_string_date(new Date(dataInitial['created'])), time_to_string_time(new Date(dataInitial['created'])), initial)
			);

			/**
			 * Se hace un ciclo para la carga de nuevos datos actualizados.
			 */
			data['changes'].reverse().forEach(function(item, index) {
				initial = '';

				function classOri(iter) {
					if (iter % 2) {
						return '';
					} else {
						return 'left-aligned';
					}
				}

				for (field in item) {
					if (!['updated', 'updater'].includes(field)) {
						initial += itemInitial.format(human_field[field], readable_field(field, item[field]['new_value']));
					}
				}

				if (initial) {
					load_node.prepend(
						changeHTML.format(
							classOri(index),
							time_to_string_date(new Date(item['updated']['new_value'])),
							time_to_string_time(new Date(item['updated']['new_value'])),
							initial,
							readable_field('updater', item['updater']['new_value'])
						)
					);
				}
			});
		},

		/**
		 * @function {error} actualemnte vacia.
		 * @param {*} data
		 */
		error: function(data) {}
	});
}
