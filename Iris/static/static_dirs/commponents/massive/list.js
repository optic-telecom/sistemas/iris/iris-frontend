/**
 * @author {Cristian Cantero}
 * @file { feasibility_lis }
 */

/**
 * @function operator_change
 * @param {number} id
 */
function operator_change(id) {
	content = $('#modal-html');
	content.empty();
	/**
	 * @API [ajax]: Se consulan segun los valores que esta defindo en la ruta
	 */
	$.ajax({
		url: url_base_templates.format('Iris/inicio/sesion/iniciada/operator_change'),
		success: function(body_html) {
			content.html(body_html);
			/**
			 * @API [ajax]: Se consulan segun los valores que esta defindo en el @param id
			 */
			$.ajax({
				url: url_base.format('api/v1/tickets/operator/{0}/'.format(id)),
				success: function(data) {
					$('#change_operator_id').val(data['ID']);
					$('#change_operator_name').val(data['name']);
					$('#change_operator_template').val(data['template']);

					$('#modals').modal('show');
				},
				error: function() {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Error',
						text: 'No se puedo cargar el recurso',
						sticky: false,
						time: ''
					});
				}
			});
		},

		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Error',
				text: 'No se puedo cargar el recurso',
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-massive').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Envios',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Envios de _START_ a _END_ de un total de _TOTAL_ Envios',
		sInfoEmpty: 'Mostrando Envios del 0 al 0 de un total de 0 Envios',
		sInfoFiltered: '(filtrado de un total de _MAX_ Envios)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/massive/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},

	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 1, 2, 3, 4, 5, 6, 7, 8]
		},
		{
			render: function(data, type, row) {
				return moment(data)
					.tz(moment.tz.guess())
					.subtract(10, 'days')
					.calendar();
			},
			targets: 6
		}
	],
	columns: [
		{ name: 'ID' },
		{ name: 'email' },
		{ name: 'sms' },
		{ name: 'fb' },
		{ name: 'ws' },
		{ name: 'category' },
		{ name: 'created' },
		{ name: 'quantity_services' },
		{ name: 'creator' }
	]
});

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
	$('#table-massive')
		.DataTable()
		.ajax.reload();
});
