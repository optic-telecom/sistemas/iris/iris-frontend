var types = [];
var filters = [];
var filters_transition = {};
var filters_types = {};
var filter_type_transition = {};

var add_flag = true;
var cont = 0;
var sentForm = false;
var preview = false;

$('#category_type').select2({
	placeholder: 'Seleccione un tipo'
});

$('#category_type')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_type').change(function() {
	$('.category').removeAttr('name');
	$('.category').prop('required', false);
	$(this).attr('name', 'category');
	$(this).prop('required', true);

	$('#category_category').empty();
	$('#category_category').append('<option></option>');
	listItems(
		$('#category_category'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

$('#category_category').select2({
	placeholder: 'Seleccione una categoria'
});
$('#category_category')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_category').change(function() {
	$('.category').removeAttr('name');
	$('.category').prop('required', false);
	$(this).attr('name', 'category');
	$(this).prop('required', true);

	$('#category_subcategory').empty();
	$('#category_subcategory').append('<option></option>');
	listItems(
		$('#category_subcategory'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

$('#category_subcategory').select2({
	placeholder: 'Seleccione una subcategoria'
});
$('#category_subcategory')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_subcategory').change(function() {
	$('.category').removeAttr('name');
	$('.category').prop('required', false);
	$(this).attr('name', 'category');
	$(this).prop('required', true);

	$('#category_second_subcategory').empty();
	$('#category_second_subcategory').append('<option></option>');
	listItems(
		$('#category_second_subcategory'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

$('#category_second_subcategory').select2({
	placeholder: 'Seleccione una segunda subcategoria'
});
$('#category_second_subcategory')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_second_subcategory').change(function() {
	$('.category').removeAttr('name');
	$('.category').prop('required', false);
	$(this).attr('name', 'category');
	$(this).prop('required', true);
});

var url = url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__isnull=True'.format($('#menu_operators').val()));
listItems($('#category_type'), url, 'name', 'ID');

/**
 * @function handleBootstrapWizardsValidation
 */
var handleBootstrapWizardsValidation = function() {
		'use strict';
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'slide',
			transitionSpeed: 600,
			useURLhash: !1,
			showStepURLhash: !1,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				var result = false;

				if (r == 'backward') {
					return true;
				}

				switch (i) {
					case 0:
						if (!inputValue($('.category'), 'Seleccione categoria')) {
							return false;
						}
						$('#massive_select_template').empty();

						var category__ID = $('[name="category"]')
							.find('option:selected')
							.val();
						var category__text = $('[name="category"]')
							.find('option:selected')
							.text();
						$('#item-value-2')[0].innerText = category__text;
						/**
						 * @API [ajax]: Se consulan segun los valores que esta defindo en la ruta @JselectorElement {val}
						 */
						$.ajax({
							type: 'GET',
							dataType: 'json',
							async: false,
							url: url_base.format(
								'api/v1/communications/template/?fields=ID,name,template,subject&operator__ID={0}'.format($('#menu_operators').val())
							),
							complete: function(data) {
								data = data.responseJSON;

								if (!data.length) {
									$('#template_select').css('visibility', 'hidden');
									/** @plugin {notfication} Se le notifiaca el resultado al operador */
									$.gritter.add({
										title: 'Error',
										text: 'No hay plantillas para esta empresa',
										sticky: false,
										time: ''
									});
								} else {
									$('#massive_select_template').append(new Option());
									data.forEach(function(value, index) {
										$('#massive_select_template').append(new Option(value['name'], value['ID']));
									});

									$('#massive_select_template').select2({
										placeholder: 'Seleccione una plantilla'
									});
									$('#massive_select_template')
										.nextAll()
										.attr('style', 'width: 100% !important');

									$('#massive_select_template').change(function() {
										template__ID = $(this).val();
										/**
										 * @API [ajax]: Se consulan segun los valores que esta defindo en el @this {val}
										 */
										$.ajax({
											type: 'GET',
											dataType: 'json',
											url: url_base.format('api/v1/communications/template/{0}/'.format(template__ID)),
											complete: function(data) {
												data = data.responseJSON;

												$('#massive_subject_message').val(data['subject']);
												$('#massive_template_html').val(data['template_html']);
												$('#massive_template_text').val(data['template_text']);
											}
										});
									});
								}

								result = true;
							}
						});

						break;

					case 1:
						$('#massive_template').val();

						if ($('#massive_select_template').children().length > 0 && $('#massive_select_template').val()) {
							var template__ID = $('#massive_select_template').val();
							var template__text = $("#massive_select_template option[value='{0}']".format(template__ID))[0].innerText;
							$('#item-value-3')[0].innerText = template__text;
						}

						if (!$('#massive_template_html').val() && !$('#massive_template_text').val()) {
							/** @plugin {notfication} Se le notifiaca el resultado al operador */
							$.gritter.add({
								title: 'Error',
								text: 'Ingrese un mensaje',
								sticky: false,
								time: ''
							});
							return false;
						} else {
							if (!$('#massive_subject_message').val()) {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Error',
									text: 'Ingrese un asunto',
									sticky: false,
									time: ''
								});
							} else {
								result = true;
							}
						}

						break;

					case 2:
						var chanels = $('input[name="mincheck[]"]');
						var chanels_result = '';

						if (chanels[0].checked) {
							chanels_result += ' Correo';
						}

						if (chanels[1].checked) {
							chanels_result += ' Facebook';
						}

						if (chanels[2].checked) {
							chanels_result += ' Whatsapp';
						}

						if (chanels[3].checked) {
							chanels_result += ' SMS';
						}

						$('#item-value-4')[0].innerText = chanels_result;

						if (!$('input[name="mincheck[]"]:checked').length) {
							/** @plugin {notfication} Se le notifiaca el resultado al operador */
							$.gritter.add({
								title: 'Error',
								text: 'Ingrese al menos, un medio de comunicacion',
								sticky: false,
								time: ''
							});
						} else {
							result = true;
						}

						break;

					case 3:
						if (!$('#list-filters').children().length) {
							/** @plugin {notfication} Se le notifiaca el resultado al operador */
							$.gritter.add({
								title: 'Error',
								text: 'Ingrese al menos, un criterio de busqueda',
								sticky: false,
								time: ''
							});
						} else {
							result = true;
							$('#massive_operator').val($('#menu_operators').val());
							$('#massive_filters').trigger('submit');
						}

						break;

					case 4:
						result = true;
						$('#preview').hide();
						$('#loading').show();
						break;
				}

				return result;
			}),
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			});
		$('#wizard').on('showStep', function(t, a, i, r) {
			var step_dict = {};

			step_dict[1] = 'Seleccione categoria';
			step_dict[2] = 'Seleccione plantilla';
			step_dict[3] = 'Seleccione canales de comunicacion';
			step_dict[4] = 'Ingrese filtros';
			step_dict[5] = 'Vista previa';
			step_dict[6] = '';

			if (r == 'backward') {
				removeDone(i, step_dict);
			}

			if (i <= 3 && r == 'backward') {
				$('#preview').show();
				$('#loading').hide();
			}

			if (i == 5) {
				sentForm = true;
				preview = false;
				$('#massive_filters').trigger('submit');
			}

			if (i == 4) {
				sentForm = true;
				preview = true;
				$('#massive_filters').trigger('submit');
			}
		});
	},
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();

FormWizardValidation.init();

$('#massive_select_template').change(function() {
	template_ID = $(this).val();
	/**
	 * @API [ajax]: Se consulan segun los valores que esta defindo en el @this {val}
	 */
	$.ajax({
		type: 'GET',
		dataType: 'json',
		async: false,
		url: url_base.format('api/v1/communications/template/?fields=template,subject&ID={0}'.format(template_ID)),
		complete: function(data) {
			template = data.responseJSON[0]['template'];
			subject = data.responseJSON[0]['subject'];
			$('#massive_template').val(template);
			$('#massive_subject_message').val(subject);
		}
	});
});

function createTagit(node, defaulsTags, array) {
	new_element = $("<ul class='primary line-mode form-group row m-b-10'></ul>");
	node.after(new_element);
	fieldName = 'tags{0}[]'.format(cont++);

	new_element.tagit({
		fieldName: fieldName,
		availableTags: array,
		autocomplete: { delay: 0, minLength: 1 },
		showAutocompleteOnFocus: true,
		allowDuplicates: false,
		tabIndex: -1,

		beforeTagAdded: function(event, ui) {
			tags = $(this).tagit('assignedTags');
			switch (tags.length) {
				case 1:
					if (!filters.includes(ui.tagLabel)) {
						event.preventDefault();
						/** @plugin {notfication} Se le notifiaca el resultado al operador */
						$.gritter.add({
							title: 'Error',
							text: 'El filtro no es valido',
							sticky: false,
							time: ''
						});
					} else {
						if (add_flag) {
							event.preventDefault();
							add_flag = false;
							createTagit($(this), ['X', ui.tagLabel], filters_types[types[ui.tagLabel]]);
							$(this).remove();
							add_flag = true;
						}
					}
					break;

				case 2:
					if (!add_flag) {
						break;
					}

					if (!filters_types[types[tags[1]]].includes(ui.tagLabel)) {
						event.preventDefault();
						/** @plugin {notfication} Se le notifiaca el resultado al operador */
						$.gritter.add({
							title: 'Error',
							text: 'El criterio no es valido',
							sticky: false,
							time: ''
						});
					} else {
						event.preventDefault();

						var tags = $(this).tagit('assignedTags');
						var filter = filters_transition[tags[1]];
						tags.push(ui.tagLabel);

						var values = [];
						var search = '';

						if (filter.startsWith('node')) {
							field = filter.split('__')[1];
							search = 'node';
						} else {
							if (filter.startsWith('service')) {
								field = filter.split('__')[1];
								search = 'service';
							} else {
								if (filter.startsWith('customer')) {
									field = filter.split('__')[1];
									search = 'customer';
								} else {
									field = filter;
									search = 'contract';
								}
							}
						}

						/**
						 * @API [ajax]: Se consulan segun los valores que esta defindo en el @var search y el @var field
						 */
						$.ajax({
							url: url_base.format('api/v1/matrix/{0}/?fields={1}'.format(search, field)),
							type: 'GET',
							async: false,
							success: function(data) {
								data.forEach(function(item, index) {
									try {
										values.push(item[field].toString(10));
									} catch (error) {}
								});
							},
							error: function(error) {}
						});

						var valuesUnique = [];
						$.each(values, function(i, el) {
							if ($.inArray(el, valuesUnique) === -1) valuesUnique.push(el);
						});

						add_flag = false;
						createTagit($(this), tags, valuesUnique);
						$(this).remove();
						add_flag = true;
					}

					break;

				default:
			}
		},
		beforeTagRemoved: function(event, ui) {
			currentTags = $(this).tagit('assignedTags');
			positionTag = currentTags.indexOf(ui.tagLabel);

			switch (positionTag) {
				case 0:
					event.preventDefault();
					$(this).remove();
					break;

				case 1:
					event.preventDefault();
					createTagit($(this), ['X'], filters);

					$(this).remove();
					break;

				case 2:
					event.preventDefault();
					createTagit($(this), ['X', currentTags[1]], filters_types[types[currentTags[1]]]);

					$(this).remove();
					break;

				default:
			}
		}
	});

	defaulsTags.forEach(function(item, index) {
		new_element.tagit('createTag', item);
	});
}

function remove_filter(step) {
	$('#div_filter{0}'.format(step)).remove();
	debugger;
}

$('#add-filter').on('click', function(event) {
	event.preventDefault();

	var html =
		'<div style=" display: inline-block; margin-top: 10px;" id="div_filter{0}">' +
		'<a onclick="remove_filter({0})" class="close" aria-label="Close"><span aria-hidden="true">×</span></a>' +
		'<div style=" display: inline-block; margin-top: 10px;">' +
		'<select type="text"  id="filter_{0}" step="{0}" name="filter_{0}[]" ><option></option></select>' +
		'<select type="text" disabled id="filter_comparison{0}" step="{0}"></select>' +
		'</div>' +
		'<div id="input_{0}" style=" display: inline-block;">' +
		'</div>' +
		'</div>';

	var new_element = $($.parseHTML(html.format(cont)));

	$('#list-filters').append(new_element);

	$('#filter_{0}'.format(cont)).select2({
		placeholder: 'Seleccione un filtro'
	});
	$('#filter_{0}'.format(cont))
		.nextAll()
		.attr('style', 'width: 250px !important');
	$('#filter_{0}'.format(cont)).change(function() {
		var step = $(this).attr('step');
		$('#filter_comparison{0}'.format(step)).removeAttr('disabled');
		$('#filter_comparison{0}'.format(step)).empty();
		$('#filter_comparison{0}'.format(step)).append(new Option());

		$('#input_{0}'.format(step)).empty();

		filters_types[types[$(this).val()]].forEach(function(item) {
			$('#filter_comparison{0}'.format(step)).append(new Option(item, item));
		});

		var html_input = '';
		var type_input = types[$('#filter_{0}'.format(step)).val()];
		switch (type_input) {
			case 'str':
			case 'int':
				html_input = '<input disabled style = "width: 250px !important" type="{1}" id="filter_values_{0}" class="form-control" placeholder="valor">';
				html_input = html_input.format(step, type_input == 'str' ? 'text' : 'number');
				$('#input_{0}'.format(step)).append(html_input);

				var filter = filters_transition[$('#filter_{0}'.format(step)).val()].split('__');
				var endpoint = '',
					field = '';
				debugger;
				if ($('#filter_values_{0}'.format(step)).data('DateTimePicker')) {
					$('#filter_values_{0}'.format(step))
						.data('DateTimePicker')
						.destroy();
				}

				if (filter.length == 1) {
					endpoint = 'services';
					field = filter[0];
				} else {
					field = filter[1];

					switch (filter[0]) {
						case 'service':
							endpoint = 'plans';
							break;
						case 'customer':
							endpoint = 'customers';
							break;
						case 'node':
							endpoint = 'nodes';
							break;
					}
				}

				/**
				 * @API [ajax]: Se consulan segun los valores que esta defindo en el @var endpoint y el @var field
				 */
				$.ajax({
					url: url_base.format('api/v1/matrix/{0}/?fields={1}'.format(endpoint, field)),
					type: 'GET',
					args: { field: field, step: step },
					success: function(data) {
						var values = [];
						var field = this.args['field'],
							step = this.args['step'];

						data.forEach(function(item) {
							values.push(item[field].toString(10));
						});
						var valuesUnique = [];
						$.each(values, function(i, el) {
							if ($.inArray(el, valuesUnique) === -1) valuesUnique.push(el);
						});

						source = $.map(valuesUnique, function(item) {
							return {
								label: item,
								value: item
							};
						});

						$('#filter_values_{0}'.format(step)).autocomplete({
							autoFocus: true,
							delay: 2,
							minLength: 2,
							source: source
						});
					}
				});

				break;

			case 'bool':
				html_input =
					'<select disabled id="filter_values_{0}" >' + '<option value="True">Cierto</option>' + '<option value="False">Falso</option>' + '</select>';

				html_input = html_input.format(step);
				$('#input_{0}'.format(step)).append(html_input);

				if ($('#filter_values_{0}'.format(step)).data('DateTimePicker')) {
					$('#filter_values_{0}'.format(step))
						.data('DateTimePicker')
						.destroy();
				}

				$('#filter_values_{0}'.format(step)).select2({
					placeholder: 'Seleccione valor'
				});

				$('#filter_values_{0}'.format(step))
					.nextAll()
					.attr('style', 'width: 250px !important');

				break;

			case 'date':
				$('#filter_comparison{0}'.format(step)).change(function(event) {
					var html_input = '<input style ="width: 250px !important" type="{0}" id="filter_values_{1}" class="form-control" placeholder="valor">';
					var type = 'text';

					$('#input_{0}'.format(step)).empty();

					if (['año', 'mes', 'dia', 'dia_semana', 'semana', 'trimestre'].includes($(this).val())) {
						type = 'number';
						$('#input_{0}'.format(step)).append(html_input.format(type, step));
					} else {
						$('#input_{0}'.format(step)).append(html_input.format(type, step));
						$('#filter_values_{0}'.format(step)).datetimepicker({
							format: 'DD/MM/YYYY'
						});
					}
				});

				break;
		}
	});

	$('#filter_comparison{0}'.format(cont)).select2({
		placeholder: 'Seleccione un criterio'
	});
	$('#filter_comparison{0}'.format(cont))
		.nextAll()
		.attr('style', 'width: 250px !important');
	$('#filter_comparison{0}'.format(cont)).change(function() {
		var step = $(this).attr('step');
		$('#filter_values_{0}'.format(step)).removeAttr('disabled');
	});

	filters.forEach(function(item) {
		$('#filter_{0}'.format(cont)).append(new Option(item, item));
	});
	$('#filter_values{0}'.format(cont)).hide();
	cont++;

	//createTagit($( "#filters" ), ['X'], filters )
});

/**
 * @API [ajax]: Se consulan segun los valores que esta defindo en la ruta
 */
$.ajax({
	url: url_base.format('api/v1/tickets/massive/massive/'),
	type: 'GET',
	success: function(data) {
		types = data['filters'];
		filters = Object.keys(data['filters']);
		filters_types = data['type_filters'];
		filters_transition = data['filters_translation'];
		filter_type_transition = data['type_filters_tranlation'];
	},
	error: function(a) {
		/** @plugin {notfication} Se le notifiaca el resultado al operador */
		$.gritter.add({
			title: 'Error',
			text: 'No se puedo cargar el recurso',
			sticky: false,
			time: ''
		});
	}
});

$('#massive_template_email').select2({
	ajax: {
		url: url_base.format('api/v1/tickets/operator_email/'),
		data: { operator: $('#menu_operators').val() },
		processResults: function(data) {
			data.forEach(function(item, index, array) {
				array[index] = {
					id: item['ID'],
					text: item['email']
				};
			});

			return {
				results: data
			};
		}
	}
});

$('#massive_template_email')
	.nextAll()
	.attr('style', 'width: 400px !important');

$('#massive_filters').on('submit', function(event) {
	event.preventDefault();

	if (!sentForm) {
		return false;
	}

	sentForm = false;
	event.preventDefault();

	data = $(this).serializeArray();

	var result = {};
	var ckeck = [];
	var filters = [];

	data.forEach(function(item, index) {
		switch (item['name']) {
			case 'category':
				result['category'] = item['value'];

				break;

			case 'subject_message':
				result['subject_message'] = item['value'];

				break;

			case 'operator':
				result['operator'] = item['value'];

				break;

			case 'template_message_html':
				result['template_html'] = item['value'];

				break;

			case 'template_message_text':
				result['template_text'] = item['value'];

				break;

			case 'mincheck[]':
				ckeck.push(item['value']);

				break;

			case 'email':
				result['email'] = item['value'];

				break;

			default:
				var filter = item['name'];
				var step = filter.match(/\d+/)[0];
				var comparison = $('#filter_comparison{0}'.format(step)).val();
				var value = $('#filter_values_{0}'.format(step)).val();

				if (types[item['value']] == 'date' && ['igual', 'mayor_igual', 'menor', 'menor_igual', 'distinto'].includes(comparison)) {
					var date = value.split('/');
					value = '{0}-{1}-{2}'.format(date[2], date[1], date[0]);
				}

				if (comparison && value && item['value']) {
					filters.push(JSON.stringify([item['value'], comparison, value]));
				}
		}
	});

	result['mincheck[]'] = ckeck;
	result['filters'] = filters;
	result['email'] = $('#massive_template_email').val();

	if (!preview) {
		/**
		 * @API [ajax]: Se envian los datos de @namespace  result
		 */
		$.ajax({
			type: 'POST',
			url: url_base.format('api/v1/tickets/massive/massive/'),
			data: result,
			success: function() {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: 'Envio exitos de comunicaciones',
					sticky: false,
					time: ''
				});
			},
			error: function() {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: 'Ha ocurrido un error, intente luego',
					sticky: false,
					time: ''
				});
			}
		});
	} else {
		/**
		 * @API [ajax]: Se consulan segun los valores que esta defindo en la ruta url
		 */
		$.ajax({
			type: 'GET',
			url: url_base.format('api/v1/tickets/massive/preview/'),
			data: {
				filters: filters,
				operator: $('#menu_operators').val()
			},
			dataType: 'json',
			traditional: true,
			success: function(data) {
				var channels = '';

				$('#preview-category').val($('[name="category"] option:selected').text());

				result['mincheck[]'].forEach(function(item, index) {
					switch (item) {
						case 'email':
							channels += ' Correo';
							break;

						case 'fb':
							channels += ' Facebook';
							break;

						case 'ws':
							channels += ' Whatsapp';
							break;

						case 'sms':
							channels += ' SMS';
							break;

						default:
					}
				});

				$('#preview-channels').val(channels);

				$('#preview-services').val(data.length);
				$('#preview-template').val(result['template_html']);

				$('#loading').hide();
				$('#preview').show();
			},
			error: function(error) {}
		});
	}
});

$('.switcher').on('click', function(e) {
	var input_id = $(this).children()[0].id;
	input = $('#{0}'.format(input_id));
	input[0].checked = !input[0].checked;
});

$('#strict').on('click', function(e) {
	$('#switcher_checkbox_5').checked = !$('#switcher_checkbox_5').checked;
});

$('#massive_operator').change(function() {
	$('#wizard').smartWizard('next');
});

$('#massive_category').change(function() {
	$('#wizard').smartWizard('next');
});

listItems($('#massive_category'), url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}'.format($('#menu_operators').val())), 'name', 'ID');
