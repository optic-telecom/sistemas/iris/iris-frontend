/**
 * @author {Cristian Cantero}
 * @file { inicio: index }
 */

/**
 * @var {variables definidas vacia} CUSTOMERS
 * @var {variables definidas vacia} COLOR_BODY
 * @var {variables definidas vacia} COLOR_SIDEBAR
 * @var {variables definidas vacia} COLOR_HEADER
 */
var CUSTOMERS = [];
var COLOR_BODY = '';
var COLOR_SIDEBAR = '';
var COLOR_HEADER = '';

/**
 * @generator
 */
String.prototype.replaceAll = function(str1, str2, ignore) {
	return this.replace(
		new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g, '\\$&'), ignore ? 'gi' : 'g'),
		typeof str2 == 'string' ? str2.replace(/\$/g, '$$$$') : str2
	);
};

if (!String.prototype.format) {
	String.prototype.format = function() {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function(match, number) {
			return typeof args[number] != 'undefined' ? args[number] : match;
		});
	};
}

jQuery.expr[':'].regex = function(elem, index, match) {
	var matchParams = match[3].split(','),
		validLabels = /^(data|css):/,
		attr = {
			method: matchParams[0].match(validLabels) ? matchParams[0].split(':')[0] : 'attr',
			property: matchParams.shift().replace(validLabels, '')
		},
		regexFlags = 'ig',
		regex = new RegExp(matchParams.join('').replace(/^\s+|\s+$/g, ''), regexFlags);
	return regex.test(jQuery(elem)[attr.method](attr.property));
};

$.ajaxSetup({
	//guardar el data store.
	beforeSend: function(request) {
		request.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	}
});

/**
 *  @function checkSesion Esta funcion se encarga de verificar la sesion inicia
 *  por medio del Token.
 *  @API [ajax] Se cosulta que el token este activo en el servidor.
 *  @module {remove} Cookies para remover la cookie que esta el localstorage del navegador.
 *  @callback {@link: window.location } para redirecionar a la pagina del login
 * 	en caso de no poseer seccion actidad
 */
function checkSesion() {
	$.post(url_base.format('api/v1/user/token/verify/'), { token: Cookies.get('token') }, function(data, status, xhr) {
		/** Si no retorna que esta activo la cookie sera removida del navegador y se enviara a a la pagina de inicio. */
		if (status !== 'success' && xhr.status !== 200) {
			Cookies.remove('token');
			window.location.replace(url_base_templates.format('Iris/inicio/'));
		}
	});
}

/**
 * @function removeDone remover la clase done de los pasos.
 * @param {number} step
 * @namespace {object} step_dict
 */
function removeDone(step, step_dict) {
	var maxStep = $('#num-steps').children().length;

	for (i = step + 1; i <= maxStep; i++) {
		$('#checked-step-{0}'.format(i)).removeClass('done');

		if (i != maxStep) {
			try {
				$('#item-value-{0}'.format(i))[0].innerText = step_dict[i];
			} catch (error) {}
		}
	}
}

/**
 * @function listItems Para cargar los valores en los selects.
 * @param {element} select
 * @param {string} url
 * @param {string} tag_text
 * @param {string} teg_value
 * @param {@callback} callBack
 */
function listItems(select, url, tag_text, teg_value, callBack = null) {
	/** @function checkSesion para verificar si la sesion esta activa  @this @file*/
	checkSesion();

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'GET',
		dataType: 'json',
		async: false,
		url: url,
		complete: function(data) {
			
			data.responseJSON.forEach(function(item) {
				select.append(new Option(item[tag_text], item[teg_value]));
			});
			/** Se verifiaca si el callBack esta true */
			if (callBack) {
				callBack();
			}
		}
	});
}

function inputValue(input, message) {
	var getValue = input.val() ? true : false;

	if (!getValue) {
		/** @plugin {notfication} Se le notifiaca el resultado al operador */
		$.gritter.add({
			title: 'Resultado:',
			text: message,
			sticky: false,
			time: ''
		});
	}

	return getValue;
}

/**
 * @function existItem consulta si existe el item .
 * @param {string} url
 * @param {string} existMessage
 * @param {string} errorMessage
 */
function existItem(url, existMessage, errorMessage) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();
	var result = true;

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'GET',
		async: false,
		url: url,
		success: function(data) {
			if (data.length > 0) {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: existMessage,
					sticky: false,
					time: ''
				});
			} else {
				result = false;
			}
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: errorMessage,
				sticky: false,
				time: ''
			});
		}
	});

	return result;
}

/**
 *  Funcion callView, la cual puede ser llamada por cualquier docuemento.
 * @param {variable que espera una URL} url_view
 * @param {Variable que tienen definido un @JselectElements} content = default.
 * @param {La data que viajara por medio del URL} data = default
 * @returns { html } el cual sera devuelta en el documento que lo este instanciando.
 */
function callView(url_view, content = $('#content'), data = []) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	/**
	 * @var {Variable en la cual se configurar para la URL}
	 */
	url_view += '/{0}/{1}'.format($('#token_user').val(), $('#menu_operators').val());

	for (i = 0; i < data.length; i++) {
		url_view += '/{0}'.format(data[i]);
	}

	url_view + '/';

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		url: url_base_templates.format('Iris/{0}/'.format(url_view)),
		async: false,
		success: function(body_html) {
			content.empty();
			content.html(body_html);
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Error',
				text: 'No se puedo cargar el recurso',
				sticky: false,
				time: ''
			});
		}
	});
}

function sendData(form, event, success, error) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	event.preventDefault();
	var url = form.attr('action');

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'POST',
		url: url,
		data: form.serialize(),
		success: function(data) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: success,
				sticky: false,
				time: ''
			});
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: error,
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * Funcion createItem Para crear nuevo items.
 * @param {string} url
 * @namespace {object} data
 * @param {string} success
 * @param {string} error
 * @function {funcion} callBack
 */
function createItem(url, data, success, error, callBack = null) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'POST',
		url: url,
		data: data,
		success: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: success,
				sticky: false,
				time: ''
			});
			/** Se verifiaca si el callBack esta true */
			if (callBack) {
				callBack();
			}
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: error,
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @function changeModal cambia el contenido del modal
 * @param {string} url_html
 * @param {sring} url_item
 * @function {function} post_ajax
 * @namespace {object} data
 */
function changeModal(url_html, url_item, post_ajax, data = {}) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	content = $('#modal-html');
	content.empty();

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		url: '{0}/{1}/{2}/'.format(url_html, $('#token_user').val(), $('#menu_operators').val()),
		data: data,
		success: function(body_html) {
			content.html(body_html);

			/** @API[ajax] donde se envia la informacion  */
			$.ajax({
				url: url_item,
				success: function(data) {
					/**
					 * @function post_ajax funcion interna
					 * @namespace {object} data
					 */
					post_ajax(data);

					$('#modals').modal('show');
				},
				error: function() {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Error',
						text: 'No se puedo cargar el recurso',
						sticky: false,
						time: ''
					});
				}
			});
		},

		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Error',
				text: 'No se puedo cargar el recurso',
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @function changeItem para cambias los items seleccionados.
 * @param {string} url
 * @namespace {object} data
 * @plugin {element} dataTable
 * @function {function} callBack
 */
function changeItem(url, data, dataTable = null, callBack = null) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'PUT',
		url: url,
		data: data,
		success: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Cambio exitoso',
				sticky: false,
				time: ''
			});
			/**
			 * Se verifica el dataTable
			 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
			 */
			if (dataTable) {
				$(dataTable)
					.DataTable()
					.ajax.reload();
			}
			/** Se verifiaca si el callBack esta true */
			if (callBack) {
				callBack();
			}
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Ocurrio un error',
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @function removeItem solo para eliminar (desactivar) informacion para la vistas.
 * @param {string} url
 * @plugin {element} dataTable
 * @function {funcion} callBack
 */
function removeItem(url, dataTable = null, callBack = null) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();

	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'DELETE',
		url: url,
		success: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Elemento removido',
				sticky: false,
				time: ''
			});

			/**
			 * Se verifica el dataTable
			 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
			 */
			if (dataTable) {
				$(dataTable)
					.DataTable()
					.ajax.reload();
			}
			/** Se verifiaca si el callBack esta true */
			if (callBack) {
				callBack();
			}
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Error al eliminar elemento',
				sticky: false,
				time: ''
			});
		}
	});
}

function time_to_human(datetime) {
	var result = '';

	if (datetime) {
		var time = datetime.split('T')[0];
		var dates = time.split('-');

		result = '{0}-{1}-{2}'.format(dates[0], dates[1], dates[2]);
	}

	return result;
}

/**
 * @function changeField se realizaran los cambios
 * @param {string} url
 * @namespace {object} data
 * @param {string} success
 * @param {string} error
 * @function { function } callBack
 */
function changeField(url, data, success, error, callBack = null) {
	/** @function checkSesion para verificar si la sesion esta activa  */
	checkSesion();
	/** @API[ajax] donde se envia la informacion  */
	$.ajax({
		type: 'PATCH',
		url: url,
		data: data,
		success: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: success,
				sticky: false,
				time: ''
			});
			/** Se verifiaca si el callBack esta true */
			if (callBack) {
				callBack();
			}
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: error,
				sticky: false,
				time: ''
			});
		}
	});
}
