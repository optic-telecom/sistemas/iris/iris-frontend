/**
 * @author {Cristian Cantero}
 *  @file {view_template}
 */

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-classification').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Clasificacion',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Clasificacion de _START_ a _END_ de un total de _TOTAL_ Clasificaciones',
		sInfoEmpty: 'Mostrando Clasificacion del 0 al 0 de un total de 0 Clasificacion',
		sInfoFiltered: '(filtrado de un total de _MAX_ Tipificaciones)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},

		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/classification/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<a onclick='classification_change({0})' class='btn btn-sm btn-success' data-toggle='modal'>Cambiar</a>";
				return html.format(data);
			},
			targets: 3
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='removeItem(`{0}`, `{1}`)' class='btn btn-sm btn-success' data-toggle='modal'>Borrar</a>";
				url = url_base.format('api/v1/tickets/classification/{0}/'.format(data));
				dataTable = '#table-classification';
				return html.format(url, dataTable);
			},
			targets: 4
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 2]
		},
		{
			orderable: false,
			searchable: false,
			targets: [3, 4]
		}
	],
	columns: [{ name: 'ID' }, { name: 'name' }, { name: 'company__name' }, { name: 'Cambiar' }, { name: 'Borrar' }]
});

/**
 *  @function classification_change cambiar la clasificacion
 * @param {@} id
 */
function classification_change(id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/inicio/sesion/iniciada/classification_change');
	var url_item = url_base.format('api/v1/tickets/classification/{0}/'.format(id));
	/**
	 * @function post_ajax funcion interna
	 * @namespace {object} data
	 */
	function post_ajax(data) {
		/**
		 * Configuracion de los parametros de a mostrar en la vista
		 * @JselectorElement {id_element}  paramatrizado con el valor obtenido en data
		 */
		$('#classification_name').val(data['name']);
		$('#classification_change_id').val(data['ID']);
		$("#classification_operator option[value='{0}']".format(data['operator'])).prop('selected', true);
	}
	/**
	 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
	$('#table-classification')
		.DataTable()
		.ajax.reload();
});
