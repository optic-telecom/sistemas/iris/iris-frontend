/**
 * @author {Cristian Cantero}
 * @file {  }
 */

/**
 * @function handleBootstrapWizardsValidation
 */
var handleBootstrapWizardsValidation = function() {
		'use strict';
		/**
		 * @JselectorElement {id_element} @plugin {conf} smartWizard
		 */
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'slide',
			transitionSpeed: 600,
			useURLhash: !1,
			showStepURLhash: !1,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			/**
			 * @JselectorElement {id_element} @module {leaveStep} smartWizard cambios de los pasos
			 */
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				if (r == 'backward') {
					return i == 2 ? false : true;
				}

				var result = false;
				/** Se verifica el paso que se deja y se realizan dos caso.  */
				switch (i) {
					case 0:
						if (inputValue($('#classification_name'), 'Ingrese nombre')) {
							var url = url_base.format('api/v1/tickets/classification/?name={0}'.format($('#classification_name').val()));
							/**
							 * Llama a la @function {@param, @param, @param } existItem del @file {index.js}
							 */
							var existClassification = existItem(url, 'La clasificacion ya existe', 'Ocurrio un error');

							if (!existClassification) {
								result = true;
								var name = $('#classification_name').val();
								$('#item-value-2').html(name);
								$('#classification_operator').val($('#menu_operators').val());
								var data = $('#form-classification').serializeArray();
								var url = url_base.format('api/v1/tickets/classification/');
								/**
								 * Llama a la @function {@param, @param, @param } existItem del @file {index.js}
								 */
								createItem(url, data, 'Clasificacion creada', 'Error al crear clasificacion');
							}
						}

						break;

					default:
						result = true;
				}

				return result;
			}),
			/**
			 * @JselectorElement {id_element} @module {keypress} smartWizard cuando se da enter o se presiona el botton next
			 */
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			}),
			/**
			 * @JselectorElement {id_element} @module {showStep} smartWizard muestra los pasos
			 */
			$('#wizard').on('showStep', function(t, a, i, r) {
				var step_dict = {};

				step_dict[1] = 'Ingresar nombre';

				if (r == 'backward') {
					/**
					 * Llama a la @function {@param, @namespace} removeDone del @file {index.js}
					 */
					removeDone(i, step_dict);
				}
			});
	},
	/**
	 * @function FormWizardValidation
	 *  Se usa para deplegar la funcion interna.
	 */
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();

/**  se despliega la funcion que contien el wizard  */
FormWizardValidation.init();

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 * @JselectorElement {id_element} @plugin {next} smartWizard
 */
$('#classification_operator').change(function() {
	$('#wizard').smartWizard('next');
});
