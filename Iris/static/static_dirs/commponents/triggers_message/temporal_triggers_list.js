/**
 * @author {Cristian Cantero}
 * @file { view_triggers_time }
 */

/**
 * @var {element} new_element
 */
function actions() {
	//Boton Activar/Desactivar disparador temporal..
	$('.change_status_trigger_temporal').click(function() {
		id_massive = $(this).attr('id_massive');

		//se definen los atributos que requieren para la data.
		if ($(this).attr('status') == 'True') {
			data = { endpoint: 'inactive', new_value: 'False', before_class: 'btn-secondary', new_class: 'btn-primary', text: 'Activar' };
		} else {
			data = { endpoint: 'active', new_value: 'True', before_class: 'btn-primary', new_class: 'btn-secondary', text: 'Desactivar' };
		}
		/**
		 * @API [ajax]: Se envian los datos por medios de dos @argument
		 */
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url_base.format('api/v1/communications/massive/{1}/{0}/'.format(data['endpoint'], id_massive)),
			args: { button: $(this), data: data },
			complete: function(data) {
				if (data.status == 200) {
					//Configuracion del botton y la data..
					button = this.args['button'];
					data = this.args['data'];
					//Se agregan los atributo del botton.
					button
						.attr('status', data['new_value'])
						.removeClass(data['before_class'])
						.addClass(data['new_class'])
						.text(data['text']);
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Muy Bien!',
						text: this.args.data['endpoint'] == 'active' ? 'Disparador Activado' : 'Disparador Desactivado',
						sticky: false,
						time: ''
					});
				} else {
					let title = '';
					let text = '';
					//Se consulta el un error interno, es decir, si es de comunicacion o es porque paso el tiempo del disparador.
					if (data.status === 500) {
						if (data.responseJSON['non_field_errors'] == 'Start time has passed') {
							title = 'No se puede activar';
							text = 'Modifique el disparador, coloque una fecha de inicio posterior a la actual';
						} else {
							title = 'No se puede activar';
							text = 'Ha ocurrido un error interno de servidor';
						}
					}

					//** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: title,
						text: text,
						sticky: false,
						time: ''
					});
				}
			}
		});
	});

	/** @JselectorElement {class_element} @module {event} click Boton Probar disparador Temporal */
	$('.test_trigger_temporal').click(function() {
		id_massive = $(this).attr('id_massive');
		/**
		 * @API [ajax]: se actina la prueba de envio de comunicacones masvas
		 */
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: url_base.format('api/v1/communications/massive/{0}/test/'.format(id_massive)),
			complete: function(data) {
				if (data.status == 200) {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Exitoso',
						text: 'La prueba se inicio de forma exitosa',
						sticky: false,
						time: ''
					});
				} else {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Oooops',
						text: 'Ocurrio un error',
						sticky: false,
						time: ''
					});
				}
			}
		});
	});

	/** @JselectorElement {class_element} @module {event} click  Boton para eliminar el disparador teporal. */
	$('.delete_trigger_temporal').click(function() {
		let id_massive = $(this).attr('id_massive');
		//Se le pregunta al usuario/operador si esta seguro si que desea eliminar el disparador de la lista
		swal(
			{
				title: '¿Estas seguro?',
				text: 'Se eliminara el disparador temporal!',
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn-danger',
				confirmButtonText: 'Si, eliminarlo!',
				cancelButtonText: 'No, Cancelar',
				closeOnConfirm: true,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					/**
					 * @API [ajax]: se desactiva de la vista el disparador temporal
					 */
					$.ajax({
						type: 'DELETE',
						dataType: 'json',
						url: url_base.format('api/v1/communications/massive/{0}/'.format(id_massive)),
						complete: function(data) {
							if ([200, 204].includes(data.status)) {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Exitoso',
									text: 'El item se elimino de forma exitosa',
									sticky: false,
									time: ''
								});

								/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
								$('#table-temporal-trigger')
									.DataTable()
									.ajax.reload(actions);
							} else {
								/** @plugin {notfication} Se le notifiaca el resultado al operador */
								$.gritter.add({
									title: 'Oooops',
									text: 'Ocurrio un error',
									sticky: false,
									time: ''
								});
							}
						}
					});
				} else {
					swal('Cancelado', 'El disparador conttinua en la lista', 'success');
				}
			}
		);
	});
	/** @JselectorElement {class_element} @module {event} click  Realizar cambios del disparador temporal. */
	$('.change_trigger_temporal').click(function() {
		/**
		 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
		 */
		changeModal(url_base_templates.format('Iris/view_triggers_time_change'), '', function() {});
	});
	//datatable que se mostrara en el datatable.
}

/**
 * @function showHistoryDataTable Muestra el historial en una datatable.
 * @JselectorElement @plugin {conf} dataTable
 */
function showHistoryDataTable(ID) {
	$('#historyModalDataTable').DataTable({
		aaSorting: [[0, 'desc']],
		language: {
			sLengthMenu: 'Mostrar _MENU_ Historico',
			sZeroRecords: 'No se encontraron resultados',
			sEmptyTable: 'Ningún dato disponible en esta tabla',
			sInfo: 'Mostrando Historico de _START_ a _END_ de un total de _TOTAL_ Historico',
			sInfoEmpty: 'Mostrando Historico del 0 al 0 de un total de 0 Historico',
			sInfoFiltered: '(filtrado de un total de _MAX_ Historicos)',
			sInfoPostFix: '',
			sSearch: 'Buscar:',
			sUrl: '',
			sInfoThousands: ',',
			sLoadingRecords: 'Cargando...',
			sProcessing: 'Cargando...',
			oPaginate: {
				sFirst: 'Primero',
				sLast: 'Último',
				sNext: 'Siguiente',
				sPrevious: 'Anterior'
			},
			oAria: {
				sSortAscending: ': Ordenar ascendente',
				sSortDescending: ': Ordenar descendente'
			}
		},
		ajax: {
			type: 'GET',
			dataType: 'json',

			url: url_base.format('api/v1/communications/massive/history_execution/?id=' + ID)
		},
		columnDefs: [
			{
				/** Se muestra la horas que han transcurrido desde que se creo el registro */
				render: function(data, type, row) {
					return moment(data)
						.tz(moment.tz.guess())
						.fromNow();
				},
				targets: 1
			},
			{
				/** Se traduce lo que s guardo en  la base de datos. */
				render: function(data, type, row) {
					const loop_shot = {
						Minutes: 'Minutos',
						Once: 'Una vez',
						Hourly: 'Cada hora',
						Daily: 'Diariamente',
						Weekly: 'Semanalmente',
						Monthly: 'Mensualmente',
						Quarterly: 'Trimestralmente',
						Yearly: 'Anualmente'
					};
					return loop_shot[data];
				},
				targets: 2
			},
			{
				/** Se traduce lo que s guardo en  la base de datos. */
				render: function(data, type, row) {
					const type_filter = { Disjuntive: 'Disjuntivo', Conjuntive: 'Conjuntivo' };
					return type_filter[data];
				},
				targets: 6
			},
			{
				/** Se traduce lo que s guardo en  la base de datos. */
				render: function(data, type, row) {
					let filters = '';
					data = JSON.parse(data.replaceAll('&#39;', `"`));
					const type_conditions = { equal: 'igual', gt: 'mayor', gte: 'mayor o igual', lt: 'menor', lte: 'menor_igual' };
					for (let i = 0; i < data.length; i++) {
						filters += type_conditions[data[i][1]] + ' ' + data[i][2];
						if (data.length > i + 1) {
							filters += ', ';
						}
					}
					return filters;
				},
				targets: 7
			},
			{
				/** Se mustra a cantidad de correos que estan siendo excluidos */
				render: function(data, type, row) {
					data = JSON.parse(data.replaceAll('&#39;', `"`));
					return data.length;
				},
				className: 'text-center',
				targets: 8
			},
			{
				/** Se dice a cantidad de sercivios asociados. */
				render: function(data, type, row) {
					data = JSON.parse(data.replaceAll('"', ` `));
					return data.length;
				},
				className: 'text-center',
				targets: 9
			}
		],
		columns: [
			{ name: 'ID' },
			{ name: 'created' },
			{ name: 'schedule_type' },
			{ name: 'email_test' },
			{ name: 'email_response' },
			{ name: 'email_copy' },
			{ name: 'type_filters' },
			{ name: 'filters' },
			{ name: 'excludes' },
			{ name: 'services' }
		],
		/** se destruye la tabla cuando comienza otra llamada de datos en la base de datos. */
		destroy: true
	});
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-temporal-trigger').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Disparadores',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Disparadores de _START_ a _END_ de un total de _TOTAL_ Disparadores',
		sInfoEmpty: 'Mostrando Disparadores del 0 al 0 de un total de 0 Disparadores',
		sInfoFiltered: '(filtrado de un total de _MAX_ Disparadores)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/communications/massive/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
		}
	},
	initComplete: function(settings, json) {
		actions();
	},
	columnDefs: [
		{
			orderable: true,
			targets: [0, 1, 2, 3, 4, 5, 6]
		},
		{
			render: function(data, type, row) {
				human_values = {
					Minutes: 'Minutos',
					Once: 'Una vez',
					Hourly: 'Cada hora',
					Daily: 'Diariamente',
					Weekly: 'Semanalmente',
					Monthly: 'Mensualmente',
					Quarterly: 'Trimestralmente',
					Yearly: 'Anualmente',
					Minutes: 'Minutos'
				};
				return human_values[data];
			},
			targets: 2
		},
		{
			render: function(data, type, row) {
				return "<a href='javascript:;' style='cursor: pointer;' onclick='temporal_trigger_change(`{0}`)'>{0}</a>".format(data);
			},
			targets: 0
		},
		{
			render: function(data, type, row) {
				return data == -1 ? 'Siempre' : data == 0 ? 'Nunca' : '{0} veces'.format(data);
			},
			targets: 3
		},
		{
			render: function(data, type, row) {
				type_button = {
					False: ['btn-primary', 'Activar'],
			/**
 * @author {Cristian Cantero}
 * @file { view_triggers_time }
 */

vTrue: ['btn-secondary', 'Desactivar']
				};

				value_button = type_button[data];

				return '<button class="btn {0} change_status_trigger_temporal" id_massive="{2}" status="{3}" type="button">{1}</button>'.format(
					value_button[0],
					value_button[1],
					row[0],
					data
				);
			},
			targets: 7
		},
		{
			render: function(data, type, row) {
				return '<button class="btn btn-primary test_trigger_temporal" id_massive="{0}" type="button">Probar</button>'.format(row[0]);
			},
			targets: 8
		},
		{
			render: function(data, type, row) {
				return '<button class="btn btn-primary delete_trigger_temporal" id_massive="{0}" type="button">Eliminar</button>'.format(row[0]);
			},
			targets: 9
		},
		{
			render: function(data, type, row) {
				return '<button class="btn btn-primary" id="showHistoryDataTable" data-toggle="modal" data-target="#modalHistory" onclick="showHistoryDataTable({0})" type="button">Historico</button>'.format(
					row[0]
				);
			},
			targets: 10
		}
	],
	columns: [
		{ name: 'ID' },
		{ name: 'schedule_name' },
		{ name: 'schedule_type' },
		{ name: 'schedule_repeats' },
		{ name: 'email_test' },
		{ name: 'email_response' },
		{ name: 'email_copy' },
		{ name: 'status' },
		{ name: 'ID' },
		{ name: 'ID' }
	]
});
