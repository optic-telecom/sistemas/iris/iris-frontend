/**
 * @author {Cristian Cantero}
 * @file { view_triggers_time }
 */

/**
 * Llama a la @function { @param } FormWizardView del @file {temporal_triggers_wizard.js}
 */
FormWizardView('massive_filters');

/**
 * @JselectorElement {id_element} @module {event} se enviaran los archivos que estan en la vista.
 */
$('#massive_filters').on('submit', function(event) {
	event.preventDefault();

	/** @var {array} data alamacenara los datos que estan el formulario */
	let data = $(this).serializeArray();

	/**
	 * @var {array} ckeck
	 * @var {array} filters
	 * @instance {string} result
	 */
	var ckeck = [];
	var filters = [];
	var result = new FormData();

	/** se configura @var result con los datos almacenados en @var data */
	data.forEach(function(item, index) {
		/** segun el caso se configura @var result */
		switch (item['name']) {
			case 'schedule_name':
				result.set('schedule_name', item['value']);
				break;

			case 'email_test':
				result.set('email_test', item['value']);
				break;

			case 'schedule_type':
				result.set('schedule_type', item['value']);
				break;

			case 'next_run':
				var date = item['value'].split(' ')[0].split('/');
				var time = item['value'].split(' ')[1].split(':');
				/** @var {datetime} timezone almacena el tiempo local  */
				var timezone = moment()
					.tz(moment.tz.guess())
					.format()
					.split('T')[1];

				/** @plugin {datatime} moment se almacena en @var timezone */
				timezone = timezone.includes('+') ? '+{0}'.format(timezone.split('+')[1]) : '-{0}'.format(timezone.split('-')[1]);
				time = '{0}-{1}-{2}T{3}:{4}:{5}{6}'.format(date[2], date[1], date[0], time[0], time[1], time[2], timezone);

				result.set('next_run', time);

				break;

			case 'email_response':
				result.set('email_response', item['value']);

				break;

			case 'template_id':
				result.set('template_id', item['value']);

				break;

			case 'type_filters':
				result.set('type_filters', item['value']);

				break;

			case 'email_copy':
				result.set('email_copy', item['value']);

				break;

			case 'schedule_repeats':
				result.set('schedule_repeats', item['value']);

				break;

			default:
				var filter = item['name'];
				var step = filter.match(/\d+/)[0];
				var comparison = $('#filter_comparation_{0}'.format(step)).val();
				var value = $('#filter_value_{0}'.format(step)).val();
				var type_filter = $('#filter_type_{0} option:selected'.format(step)).text();

				if (TYPES[type_filter] == 'date' && ['igual', 'mayor_igual', 'menor', 'menor_igual', 'distinto'].includes(comparison)) {
					var date = value.split('/');
					value = '{0}-{1}-{2}'.format(date[2], date[1], date[0]);
				}

				if (comparison && value && item['value']) {
					filters.push(JSON.stringify([item['value'], comparison, value]));
				}
		}
	});

	result.set('filters', JSON.stringify(filters));

	/**
	 * se verifica el @JselectorElement {file} no este indefinido  y no este vacio
	 */

	if ($('#excludes_file')[0].files[0]) {
		result.set('excludes_file', $('#excludes_file')[0].files[0], 'file.xlsx');
	}

	if ($('#excludes_file')[0].files[0] != undefined) {
		result.set('excludes_file', $('#excludes_file')[0].files[0], 'file.xlsx');
	}
	if ($('#includes_file')[0].files[0]) {
		result.set('includes_file', $('#includes_file')[0].files[0], 'file.xlsx');
	}
	if ($('#includes_file')[0].files[0] != undefined) {
		result.set('includes_file', $('#includes_file')[0].files[0], 'file.xlsx');
	}

	result.set('filters', JSON.stringify(filters));
	result.set('operator', $('#menu_operators').val());
	/**
	 * @API [ajax]: Se envian los datos de @constant  result
	 */
	$.ajax({
		type: 'POST',
		url: url_base.format('api/v1/communications/massive/'),
		data: result,
		enctype: 'multipart/form-data',
		contentType: false,
		processData: false,
		success: function() {
			/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
			$('#table-temporal-trigger')
				.DataTable()
				.ajax.reload(actions);

			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Creado exitosamente el envio masivo',
				sticky: false,
				time: ''
			});
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Ha ocurrido un error, intente luego',
				sticky: false,
				time: ''
			});
		}
	});
});
