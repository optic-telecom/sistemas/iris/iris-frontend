/**
 * @author {Cristian Cantero}
 * @file { view_triggers_time }
 */

/**
 * @function temporal_trigger_change se realizaran los disparadoes de cambios temporales
 * @param {string } ID
 */
function temporal_trigger_change(ID) {
	/** se verifica @JselectorElement {length} el tamaño de este elemento   */
	if (!$('#trigger_massive_{0}'.format(ID)).length) {
		/** @var {string} data  */
		var data = [ID];
		/** @var {string} html_typify  alamacena el elemento de disparador masivo   */
		var html_typify = `<div id="trigger_massive_{0}" class="tab-pane fade view_detail">
							<div>`;
		/** @var {string} html_button_typify  se agrega el boton asociado al disparador. */
		var html_button_typify = `<li style="cursor: pointer;" class="nav-items change_view" id="button_{0}">
                                        <a data-toggle="tab" class="nav-link " href="#trigger_massive_{0}">
                                            <span>{0}</span>
                                        </a>
									</li>`;
		/** se tranforma por medio @type {number} base 10 el id del diparador*/
		ID = parseInt(ID, 10);

		/** Se agrega despues en el @JselectorElement {id_element} los @var [html_typify, html_button_typify] */
		$('#create_view').after(html_typify.format(ID));
		$('#list_show').append(html_button_typify.format(ID));

		/** @JselectorElement {class_element} @module {click} */
		$('.change_view').click(function() {
			/** @var {string} ID almacena el valor de @this*/
			var ID = $(this).attr('id');
			var id_detail = '';

			/**
			 * @JselectorElement {class_element} @module {css} donde se le remueve clases [ active, show]
			 */
			$('.view_detail').removeClass('active');
			$('.view_detail').removeClass('show');

			/**  Segune el @var ID se realizaran los distintos casos  */
			switch (ID) {
				case 'show_create':
					id_detail = 'create_view';
					break;

				case 'show_list':
					id_detail = 'list_view';
					break;

				default:
					id_detail = 'trigger_massive_{0}'.format(ID.replace('button_', ''));
			}

			/**
			 * @JselectorElement {id_element} @module {css} se agrega la clases [active, show] segun los que se decida en el case.
			 */
			$('#{0}'.format(id_detail)).addClass('active');
			$('#{0}'.format(id_detail)).addClass('show');
		});

		/**
		 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
		 */
		callView('view_triggers_time_change', $('#trigger_massive_{0}'.format(ID)), data);

		/** Verificamos que el archivo adjuntado sea el correcto. */
		$('input[name="excludes"]').on('change', function() {
			/** Se defiene las extensiones permitidas */
			allowedExtensions = /(.xls|.xlsx)$/i;
			/** @var {string} filePath alamacena la direcion del archivo */
			let filePath = $(this).val();
			/** @var {string} ext  se extrae la extesion del archico para la verificacion*/
			let ext = filePath.substring(filePath.lastIndexOf('.'));
			/** Se verifiva la extension */
			if (!allowedExtensions.exec(ext)) {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: 'Documento ' + ext + ' no valido',
					sticky: false,
					time: ''
				});
				/** De no ser la xtension correcta se limpia el input */
				$(this).val('');
			}
		});

		/**
		 * @JselectorElement {id_element} @module {event} se enviaran los archivos que estan en la vista.
		 */
		$('#massive_filters_change_{0}'.format(ID)).on('submit', function(event) {
			event.preventDefault();

			/** @constant  {array} data almacenara los datos  */
			const data = $(this).serializeArray();

			/** @constant  {array} result almacenara los datos @instance @function */
			const result = new FormData();

			/**
			 * @var {array} ckeck
			 * @var {array} filters
			 * @var {string} ID
			 */
			var ckeck = []; //TODO no se esta usando
			var filters = [];
			var ID = $(this)
				.attr('id')
				.replace('massive_filters_change_', '');

			data.forEach(function(item, index) {
				/** Segun sea el caso se configura la @constant result  */
				switch (item['name']) {
					case 'schedule_name':
						result.set('schedule_name', item['value']);
						break;

					case 'email_test':
						result.set('email_test', item['value']);

						break;

					case 'schedule_type':
						result.set('schedule_type', item['value']);

						break;

					case 'next_run':
						var date = item['value'].split(' ')[0].split('/');
						var time = item['value'].split(' ')[1].split(':');
						/** @var {datetime} timezone almacena el tiempo local  */
						var timezone = moment()
							.tz(moment.tz.guess())
							.format()
							.split('T')[1];

						/** @var {datetime} timezone almacena el tiempo local  */
						timezone = timezone.includes('+') ? '+{0}'.format(timezone.split('+')[1]) : '-{0}'.format(timezone.split('-')[1]);
						time = '{0}-{1}-{2}T{3}:{4}:{5}{6}'.format(date[2], date[1], date[0], time[0], time[1], time[2], timezone);
						result.set('next_run', time);
						break;

					case 'email_response':
						result.set('email_response', item['value']);
						break;

					case 'template_id':
						result.set('template_id', item['value']);
						break;

					case 'type_filters':
						result.set('type_filters', item['value']);
						break;

					case 'email_copy':
						result.set('email_copy', item['value']);
						break;

					case 'schedule_repeats':
						result.set('schedule_repeats', item['value']);
						break;

					default:
						var filter = item['name'];
						var step = filter.match(/\d+/)[0];
						var comparison = $('#filter_comparation_{0}'.format(step)).val();
						var value = $('#filter_value_{0}'.format(step)).val();
						var type_filter = $('#filter_type_{0} option:selected'.format(step)).text();

						if (TYPES[type_filter] == 'date' && ['igual', 'mayor_igual', 'menor', 'menor_igual', 'distinto'].includes(comparison)) {
							var date = value.split('/');
							value = '{0}-{1}-{2}'.format(date[2], date[1], date[0]);
						}

						if (comparison && value && item['value']) {
							filters.push(JSON.stringify([item['value'], comparison, value]));
						}
				}
			});

			/** se verifica el @JselectorElement {file} no este indefinido */
			if ($('#excludes_file_ch')[0].files[0] != undefined) {
				result.set('excludes_file', $('#excludes_file_ch')[0].files[0], 'file.xlsx');
			}

			/** se verifica el @JselectorElement {file} no este indefinido */
			if ($('#includes_file_ch')[0].files[0] != undefined) {
				result.set('excludes_file', $('#includes_file_ch')[0].files[0], 'file.xlsx');
			}

			/** Se configura @constant result */
			result.set('filters', JSON.stringify(filters));
			result.set('operator', $('#menu_operators').val());

			/**
			 * @API [ajax]: Se envian los datos de @constant  result
			 */
			$.ajax({
				type: 'PUT',
				url: url_base.format('api/v1/communications/massive/{0}/'.format(ID)),
				data: result,
				enctype: 'multipart/form-data',
				contentType: false,
				processData: false,
				success: function() {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Resultado:',
						text: 'Creado exitosamente el envio masivo',
						sticky: false,
						time: ''
					});
				},
				error: function() {
					/** @plugin {notfication} Se le notifiaca el resultado al operador */
					$.gritter.add({
						title: 'Resultado:',
						text: 'Ha ocurrido un error, intente luego',
						sticky: false,
						time: ''
					});
				}
			});
		});

		/**
		 * Llama a la @function { @param } FormWizardView del @file {temporal_triggers_wizard.js}
		 */
		FormWizardView('massive_filters_change_{0}'.format(ID));

		/**
		 * @API [ajax]: El cual se consultan segun los valores que esta defindo en @param  ID
		 */
		$.ajax({
			url: url_base.format('api/v1/communications/massive/{0}/'.format(ID)),
			args: {
				ID: ID
			},
			success: function(data) {
				/** @var {string} ID  argumento enviado por el ajax*/
				var ID = this.args['ID'];

				/** Se configura el elemento donde se cargaran todos los datos */
				var form = $('#massive_filters_change_{0}'.format(ID));
				/** Se configura el elemento donde se cargaran todos los datos */
				var type_repeat = form.find('.type_repeat_trigger_time');

				var data_filters = data['filters'];

				form.find('.first_time_trigger_time').val(moment(data['next_run']).format('DD/MM/YYYY HH:mm:ss'));
				id_template = data['template'].split('template/')[1].replace('/', '');
				form.find('.template_list_trigger_time')
					.val(id_template)
					.trigger('change');

				form.find('#name_trigger_time').val(data['schedule_name']);
				form.find('#email_test_trigger_time').val(data['email_test']);
				form.find('input[name="email_response"]').val(data['email_response']);
				form.find('input[name="email_copy"]').val(data['email_copy']);
				form.find('.type_trigger_time')
					.val(data['schedule_type'])
					.trigger('change');

				/** dependiendo del caso se configura en @var type_repeat */
				switch (data['schedule_repeats']) {
					case 0:
						type_repeat.val('0').trigger('change');
						break;

					case -1:
						type_repeat.val('-1').trigger('change');
						break;

					default:
						type_repeat.val('n').trigger('change');
						break;
				}

				form.find('.number_repeat_trigger_time').val(data['schedule_repeats']);
				form.find('.type_logic_trigger_time')
					.val(data['type_filters'])
					.trigger('change');

				var filter_count = null;
				var filter = [];

				for (var i = 0; i < data_filters.length; i++) {
					filter_count = COUNT_FILTER;
					filter = data_filters[i];
					/**@module
					 * @JselectorElement {id_element} @module {event} se le agregan el evento para los diferetes filtros .
					 * @JselectorElement {id_element} @module {event} se le agregan el evento para los diferetes filtros .
					 * @JselectorElement {id_element} @module {event} se le agregan el evento para los diferetes filtros .
					 * @JselectorElement {id_element} @module {event} se le agregan el evento para los diferetes filtros .
					 */
					$('#massive_filters_change_{0}'.format(ID))
						.find('.add_filter')
						.trigger('click');
					$('#filter_type_{0}'.format(filter_count))
						.val(filter[0])
						.trigger('change');
					$('#filter_comparation_{0}'.format(filter_count))
						.val(filter[1])
						.trigger('change');
					$('#filter_value_{0}'.format(filter_count))
						.val(filter[2])
						.trigger('change');
				}
			},
			error: function() {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Error',
					text: 'No se puedo cargar el recurso',
					sticky: false,
					time: ''
				});
			}
		});

		/**@module
		 * @JselectorElement {id_element} @module {css} se agrega la clases [active, show] segun los que se decida en el case.
		 */
		$('.btn.btn-xs.btn-icon.btn-circle.btn-danger')
			.unbind('click')
			.click(function() {
				id_massive = $(this)
					.attr('id')
					.replace('remove_', '');
				/** se le cambian los parametros de las clases a mostrar por oclutar */
				$('.tooltip')
					.find('div')
					.removeClass('show')
					.addClass('hide');
				$('#button_{0}'.format(id_massive)).remove();
				$('#trigger_massive_{0}'.format(id_massive)).remove();
				$('#show_list').trigger('click');
				$('#show_list')
					.find('a')
					.addClass('active');
				$('#show_list')
					.find('a')
					.addClass('show');
			});
	}

	
}
