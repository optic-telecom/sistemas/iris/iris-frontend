/**
 * @author {Cristian Cantero}
 * @file { new_service_list,  }
 */

/**
 * @function change_new_service
 * @param {number} ID
 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
 * Llama a la @function {@param, @JselectorElement } load_data_feasibility del @file {index.js}
 */
function change_new_service(ID) {
	/** @JselectorElement {id_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
	$('#modal-html').empty();
	callView('new_service_change', $('#modal-html'));
	load_data_new_service(ID);
	$('#modals').show();
}

/**
 * @function delete_feasibility esta funcio elimina (desactiva) la factibilidad para vista
 * @param {number} ID
 */
function delete_new_service(ID) {
	/**
	 * Llama a la @function {@param, @JselectorElement } removeItem del @file {index.js}
	 */
	removeItem(url_base.format('api/v1/customers/new_service/{0}/'.format(ID)), $('#table-new_service'));
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-new_service').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Factibilidad',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Factibilidad de _START_ a _END_ de un total de _TOTAL_ Factibilidad',
		sInfoEmpty: 'Mostrando Factibilidad del 0 al 0 de un total de 0 Factibilidad',
		sInfoFiltered: '(filtrado de un total de _MAX_ Factibilidads)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/customers/new_service/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();

			d.inittime_creation = $('#new_service_datetimeinit_creation').val();
			d.endtime_creation = $('#new_service_datetimeend_creation').val();

			d.inittime_installation = $('#new_service_datetimeinit_installation').val();
			d.endtime_installation = $('#new_service_datetimeend_installation').val();
		}
	},
	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 1, 2, 3, 4, 5]
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='change_new_service({0})' class='btn btn-sm btn-success' data-toggle='modal' style='color:white;'>Cambiar</a>";
				return html.format(row[row.length - 1]);
			},
			targets: 6
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='delete_new_service({0})' class='btn btn-sm btn-primary'' data-toggle='modal' style='color:white;'>Borrar</a>";
				return html.format(row[row.length - 1]);
			},
			targets: 7
		},
		{
			render: function(data, type, row) {
				return moment(data)
					.tz(moment.tz.guess())
					.subtract(10, 'days')
					.calendar();
			},
			targets: 0
		},
		{
			render: function(data, type, row) {
				/** Se coloco la diferencia en horas que ha ha sido creado el registro.  */
				return 'Hace ' + moment().diff(data, 'hours') + ' horas';
				//moment( data ).tz(moment.tz.guess()).subtract(10, 'days').calendar()
			},
			targets: 2
		}
	],
	columns: [
		{ name: 'created' },
		{ name: 'creator' },
		{ name: 'datetime_installation' },
		{ name: 'technicians' },
		{ name: 'time_qualification' },
		{ name: 'number_typify' }
	]
});

/**
 * function current_values
 * @returns {object} con los valores definidos en el vistas
 */
function current_values() {
	return {
		draw: '1',

		'columns[0][data]': '0',
		'columns[0][name]': 'endtime_creation',
		'columns[0][searchable]': 'true',
		'columns[0][orderable]': 'true',
		'columns[0][search][value]': '',
		'columns[0][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'inittime_creation',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'endtime_installation',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'inittime_installation',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'order[0][column]': '0',
		'order[0][dir]': 'desc',
		start: '0',
		length: '-1',
		'search[value]': $('#download_data_new_service > label > input').val(),
		'search[regex]': 'false',
		endtime_creation: $('#new_service_datetimeend_creation').val(),
		inittime_creation: $('#new_service_datetimeinit_creation').val(),
		endtime_installation: $('#new_service_datetimeend_installation').val(),
		inittime_installation: $('#new_service_datetimeinit_installation').val(),
		_: '1568749802687'
	};
}

/**
 * @JselectorElement {id_element} @module {event} botton para descarga la informacion.
 */
$('#download_data_new_service').click(function() {
	/**
	 * Se declara una @var {XHLHR}  req
	 */
	var req = new XMLHttpRequest();
	/** se declara una @var urlToSend con formato de la ruta a la cual se solicitara la información */
	var urlToSend = url_base.format('api/v1/customers/new_service/download_data/') + '?{0}'.format(jQuery.param(current_values()));
	/** Se pone a hacer la solicitud del request.  */
	req.open('GET', urlToSend, true);
	req.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	req.responseType = 'blob';
	req.onload = function(event) {
		var blob = req.response;
		var link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		link.download = 'Nuevo servicio.xlsx';
		link.click();
		window.URL.revokeObjectURL(link.href);
	};

	/** se tealiza el envio del requesr con toda la configuracion cargada.  */
	req.send();
});

/**
 * @JselectorElement {class_element} @plugin {config} datetimepicker se formatea el fecha y tiempo [inicio]
 */
$('.query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/**
 * @JselectorElement {class_element} @on {event} dp.change se realizan los cambios en
 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */
$('.query').on('dp.change', function(e) {
	$('#table-new_service')
		.DataTable()
		.ajax.reload();
});
