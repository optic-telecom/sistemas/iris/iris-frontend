$('#new_service_datetime').datetimepicker({
	format: 'YYYY-MM-DDThh:mm'
});

var SELECT = $('#new_service_technicians');
var URL = url_base.format('api/v1/matrix/technicians/');

/**
 * @function callBack Funcion interna para
 * @JselectorElement {id_element} @plugin {conf} select2 Se le indica al operador que debe elegir una opcion
 * @JselectorElement {id_element} @module {attr} estilo el cual mantiene al tamaño de la pantalla  segun su resolucion
 */
function callBack() {
	$('#new_service_technicians').select2({
		placeholder: 'Seleccione técnico'
	});
	$('#new_service_technicians')
		.nextAll()
		.attr('style', 'width: 100% !important');
}

/**
 * Llama a la @function {@param, @param, @param, @param, @param} listItems del @file {index.js}
 */
listItems(SELECT, URL, 'name', 'id', callBack);
