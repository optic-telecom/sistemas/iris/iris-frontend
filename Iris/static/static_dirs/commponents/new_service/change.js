/**
 * @author {Cristian Cantero}
 * @file { new_service_list: list.js  }
 */

/**
 * @function load_data_new_service que cargara la data de la factibilidades.
 * @param {number} ID
 */
function load_data_new_service(ID) {
	/**
	 * @function load_data funcion interna que carga la data.
	 * @param {*} ID
	 */
	function load_data(ID) {
		/**
		 * @var {string} url se guarda la direccion URL  formateada
		 */
		var url = url_base.format('api/v1/customers/new_service/{0}/'.format(ID));

		/**
		 * @JselectorElement {id_element} @plugin {conf} select2  se le indica al operado que debe elegir una opcion
		 */
		$('#new_service_change_technicians').select2({
			placeholder: 'Seleccione un técnico'
		});

		/** 
		 * @JselectorElement {id_element} @module {attr} estilo para controlar el tamaño 
		*/
		$('#new_service_change_technicians')
			.nextAll()
			.attr('style', 'width: 100% !important');

		/**
		 * @API [ajax]: donde se obtienes los datos de factibilidad que esta asociada al registro
		 */
		$.ajax({
			type: 'GET',
			url: url,
			success: function(data) {
				/**
				 * @JselectorElement {id_element} : @module {push[val]} data
				 */
				$('#new_service_change_ID').val(data['ID']);
				$('#new_service_change_datetime_installation').val(data['datetime_installation'].replace('-05:00', ''));
				$('#new_service_change_time_qualification').val(data['time_qualification']);

				/** Se le adiciona el disparador de evento de cambio. */
				$('#new_service_change_technicians')
					.val(data['technicians'])
					.trigger('change');
			},
			error: function() {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: 'Error al cargar nuevo servicio',
					sticky: false,
					time: ''
				});
			}
		});
	}

	/** @var {@JselectorElement} SELECT */
	SELECT = $('#new_service_change_technicians');
	/** @var {@JselectorElement} URL */
	URL = url_base.format('api/v1/matrix/technicians/');

	/**
	 * Llama a la @function {@param, @param, @param, @param, @param} listItems del @file {index.js}
	 */
	listItems(SELECT, URL, 'name', 'id', () => load_data(ID));

	/**
	 * @JselectorElement {id_element} @plugin {config} datetimepicker se formatea el fecha y tiempo
	 */
	$('#new_service_change_datetime_installation').datetimepicker({
		format: 'YYYY-MM-DDThh:mm:ss'
	});
}

/**
 * @JselectorElement {id_element} @module {event} botton para aplicar los cambios
 */
$('#new_service_change_button').click(function() {
	/** @var {array} data donde se almacenaran los datos serializado. */
	var data = $('#form-new_service-change').serializeArray();
	/** @var {number} ID */
	var ID = $('#new_service_change_ID').val();

	/**
	 * @var {string} url se guarda la direccion URL  formateada
	 */
	var url = url_base.format('api/v1/customers/new_service/{0}/'.format(ID));

	/**
	 * @function callBack Funcion interna para
	 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
	 * @JselectorElement {id_element} @module {event} ocultara el modal
	 */
	function callBack() {
		$('#table-new_service')
			.DataTable()
			.ajax.reload();
		$('#modals').hide();
	}

	/** Se agregan al objeto el name y el value del operador */
	data.push({ name: 'operator', value: $('#menu_operators').val() });

	/**
	 * Llama a la @function {@param, @param, @param, @param} changeItem del @file {index.js}
	 */
	changeItem(url, data, null, callBack);
});
