var handleBootstrapWizardsValidation = function() {
		'use strict';
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'fade',
			transitionSpeed: 0,
			useURLhash: true,
			showStepURLhash: false,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				if (
					$('form[name="form-wizard"]')
						.parsley()
						.validate('step-' + (i + 1))
				) {
					if (i == 0) {
						var value = true;

						$.ajax({
							type: 'GET',
							url: '/',
							async: false,
							contentType: 'application/json',
							dataType: 'json',
							success: function(data) {
								value = false;
							},
							error: function(data) {
								value = false;
							}
						});

						return value;
					} else {
						return true;
					}
				} else {
					return false;
				}

				return $('form[name="form-wizard"]')
					.parsley()
					.validate('step-' + (i + 1));
			}),
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			});
	},
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();
