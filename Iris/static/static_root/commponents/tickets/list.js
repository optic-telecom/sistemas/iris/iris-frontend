/**
 * @author {Cristian Cantero}
 *  @file {}
 */

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 * Lista todo los tickets en la vista.
 */
var dt_table = $('#table-tickets').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Ticket',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Ticket de _START_ a _END_ de un total de _TOTAL_ Ticket',
		sInfoEmpty: 'Mostrando Ticket del 0 al 0 de un total de 0 Ticket',
		sInfoFiltered: '(filtrado de un total de _MAX_ Tickets)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/datatables/tickets/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},
	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 3, 4, 5, 6, 7, 8]
		},
		{
			orderable: false,
			searchable: false,
			targets: [1, 2]
		}
	],
	columns: [
		{ name: 'ID' },
		{ name: 'parent' },
		{ name: 'brother' },
		{ name: 'origin' },
		{ name: 'type_tk' },
		{ name: 'stage_tk' },
		{ name: 'city' },
		{ name: 'urgency' },
		{ name: 'service_number' },
		{ name: 'Ver' }
	],
	/** @plugin {dataTable} */
	createdRow: function(row, data, dataIndex) {
		/** Segun el caso se decora con la clase [info, warning, danger] en @JselectorElement */
		switch (data[7]) {
			case 'NORMAL':
				$(row).addClass('info');
				break;

			case 'CRITICO':
				$(row).addClass('warning');
				break;

			case 'RECLAMO':
				$(row).addClass('danger');
				break;

			default:
			//TODO Caso vacio o sin ninguna accion
		}
		/** si el cado es NORMAL el mismo se le coloca la clase [important] */
		if (data[7] == 'NORMAL') {
			$(row).addClass('important');
		}
	}
});
