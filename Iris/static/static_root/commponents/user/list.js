/**
 * @author {Cristian Cantero}
 * @file { user_list}
 */

/**
 * @function template_change
 * @param {number} id
 */
function template_change(id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/inicio/sesion/iniciada/template_change');
	var url_item = url_base.format('api/v1/tickets/template/{0}/'.format(id));

	/**
	 * @function post_ajax de uso interno.
	 * @param {object} data
	 */
	function post_ajax(data) {
		/**
		 * Configuracion de los parametros de a mostrar en la vista
		 * Se cargan los datos en los @JselectorElement {id_element}  paramatrizado con el valor obtenido de data
		 */
		$('#change_template_id').val(data['ID']);
		$('#change_template_name').val(data['name']);
		$('#change_template_html').val(data['template_html']);
		$('#change_template_text').val(data['template_text']);
		$('#change_template_subject').val(data['subject']);
		$('#change_template_operator').val($('#menu_operators').val());
	}

	/**
	 * Llama a la @function {@param, @param, @param, @param} changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}

/**
 * @function status cambiar el estatus de activo de operador del sistema
 * @param {string} pk
 */
function status(pk) {
	/**
	 * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @param pk
	 * @API [ajax]: Se envian los datos de @namespace  result
	 */
	$.ajax({
		type: 'PATCH',
		data: { is_active: $('#status_{0}'.format(pk)).is(':checked') },
		url: url_base.format('api/v1/user/data/{0}/'.format(pk)),
		success: function(data) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Estado cambiado exitosamente',
				sticky: false,
				time: ''
			});
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Error al cambiar estado',
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @function admin cambiar el estatus de administrador del operador del sistema
 * @param {*} pk
 */
function admin(pk) {
	/**
	 * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @var
	 */
	$.ajax({
		type: 'PATCH',
		data: { is_superuser: $('#admin_{0}'.format(pk)).is(':checked') },
		url: url_base.format('api/v1/user/data/{0}/'.format(pk)),
		success: function(data) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Estado cambiado exitosamente',
				sticky: false,
				time: ''
			});
		},
		error: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Error al cambiar estado',
				sticky: false,
				time: ''
			});
		}
	});
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-user').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Usuario',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Usuario de _START_ a _END_ de un total de _TOTAL_ Usuario',
		sInfoEmpty: 'Mostrando Usuario del 0 al 0 de un total de 0 Usuario',
		sInfoFiltered: '(filtrado de un total de _MAX_ Usuari*os)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/user/data/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html =
					'<div class="switcher">' +
					'<input onchange=status("{0}") type="checkbox" id="status_{0}" {1} >' +
					'<label for="status_{0}"></label>' +
					'</div>';
				return html.format(data.pk, data.status ? 'checked' : '');
			},
			targets: 1
		},
		{
			render: function(data, type, row) {
				html =
					'<div class="switcher">' +
					'<input onchange=admin("{0}") type="checkbox" id="admin_{0}" {1} >' +
					'<label for="admin_{0}"></label>' +
					'</div>';
				return html.format(data.pk, data.status ? 'checked' : '');
			},
			targets: 2
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 2]
		}
	],
	columns: [{ name: 'username' }, { name: 'is_active' }, { name: 'is_superuser' }]
});

/**
 * @JselectorElement {id_element} @module {event} botton para descarga la informacion.
 */
$('#form_user').submit(function(eevent) {
	event.preventDefault();
	/**
	 * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @var
	 */
	$.ajax({
		type: 'POST',
		data: { username: $('#create_user').val(), is_active: true },
		url: url_base.format('api/v1/user/data/'),
		success: function(data) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Usuario creado exitosamente',
				sticky: false,
				time: ''
			});
			/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
			$('#table-user')
				.DataTable()
				.ajax.reload();
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Error al crear usuario',
				sticky: false,
				time: ''
			});
		}
	});
});
