/*

Template Name: Color Admin - Responsive Admin Dashboard Template build with Twitter Bootstrap 4
Version: 4.2.0
Author: Sean Ngu
Website: http://www.seantheme.com/color-admin-v4.2/admin/



*/

new_element = $("<ul class='primary line-mode'></ul>");
$('#add_filters').after(new_element);

new_element.tagit({
	allowDuplicates: false,
	placeholderText: 'Ingrese nuevos tipos',
	tabIndex: -1,
	allowSpaces: true
});

$('#show-input').hide();

$('#category_type').select2({
	placeholder: 'Seleccione un tipo'
});
$('#category_type')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_type')
	.parent()
	.hide();

$('#category_category').select2({
	placeholder: 'Seleccione una categoria'
});
$('#category_category')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_category')
	.parent()
	.hide();

$('#category_subcategory').select2({
	placeholder: 'Seleccione una subcategoria'
});
$('#category_subcategory')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_subcategory')
	.parent()
	.hide();

$('#category_classification').select2({
	placeholder: 'Seleccione una clasificacion'
});
$('#category_classification')
	.nextAll()
	.attr('style', 'width: 400px !important');
$('#category_classification').prop('required', true);

$('#category_operator').val($('#menu_operators').val());

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form_category_add').submit(function(e) {
	e.preventDefault();
	form = $(this).serializeArray();
	var result = {};
	var tags = [];

	form.forEach(function(item, index) {
		switch (item['name']) {
			case 'operator':
				result['operator'] = item['value'];

				break;

			case 'classification':
				result['classification'] = item['value'];

				break;

			case 'parent':
				result['parent_id'] = item['value'];

				break;

			case 'tags':
				tags.push(item['value']);

				break;

			case 'sla':
				result['sla'] = item['value'];

				break;
		}
	});

	var url = url_base.format('api/v1/tickets/category/');

	tags.forEach(function(item, index) {
		result['name'] = item;
		var success = 'Tipo: {0} , creado'.format(item);
		var error = 'Tipo: {0} , ocurrio un error'.format(item);

		createItem(url, result, success, error);
	});
});

$('#category_classification').change(function() {
	$('.parent').removeAttr('name');
	$('.parent').prop('required', false);
	$('#show-input').show();

	switch ($(this).val()) {
		case '1':
			$('#category_type')
				.parent()
				.hide();
			$('#category_category')
				.parent()
				.hide();
			$('#category_subcategory')
				.parent()
				.hide();

			break;

		case '2':
			$('#category_type')
				.parent()
				.show();
			$('#category_type').attr('name', 'parent');
			$('#category_type').prop('required', true);

			$('#category_category')
				.parent()
				.hide();
			$('#category_subcategory')
				.parent()
				.hide();

			$('#category_type').empty();
			$('#category_type').append('<option></option>');
			listItems(
				$('#category_type'),
				url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&classification=1'.format($('#menu_operators').val())),
				'name',
				'ID'
			);

			break;

		case '3':
			$('#category_type')
				.parent()
				.show();

			$('#category_category')
				.parent()
				.show();
			$('#category_category').attr('name', 'parent');
			$('#category_category').prop('required', true);

			$('#category_subcategory')
				.parent()
				.hide();

			break;

		case '4':
			$('#category_type')
				.parent()
				.show();
			$('#category_category')
				.parent()
				.show();

			$('#category_subcategory')
				.parent()
				.show();
			$('#category_subcategory').attr('name', 'parent');
			$('#category_subcategory').prop('required', true);

			break;
	}
});

$('#category_type').change(function() {
	$('#category_category').empty();
	$('#category_category').append('<option></option>');
	listItems(
		$('#category_category'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});

$('#category_category').change(function() {
	$('#category_subcategory').empty();
	$('#category_subcategory').append('<option></option>');
	listItems(
		$('#category_subcategory'),
		url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}&parent__ID={1}'.format($('#menu_operators').val(), $(this).val())),
		'name',
		'ID'
	);
});
