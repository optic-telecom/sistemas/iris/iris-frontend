/**
 * @author {Cristian Cantero}
 */

/**
 * @var {number} cont variable usada como contador
 */
var cont = 0;

/**
 *@function remove_item el cual removera un componente
 * @param {element} id
 */
function remove_item(id) {
	$('#div_item{0}'.format(id)).remove();
}

/**
 * @jselectorElement {event} el cual agregara un nuevo componente diseñado en
 * @var {string} hmlt
 */

/**
 * @JselectorElement {event} Envio de la informacion del formularo con los cambios de categoria.
 */
$('#form_change_category').submit(function(e) {
	e.preventDefault();
	form = $(this).serializeArray();
	/** @namespace {object} result - Almacenara los datos. */
	result = {};
	id = form.shift();

	/** Se realiza un forEach para cargar los datos en el @namespace */
	$.each(form, function() {
		result[this.name] = this.value;
	});

	/** se verifica que la variable del @namespace este NULL en [string] para pasarlo a [null] */
	if (result['parent_id'].value == 'null') {
		result['parent_id'].value = null;
	}

	result['operator'] = $('#menu_operators').val()

	if (!result['sla']){

		result['sla'] = 0

	}

	/**
	 * @API [ajax]: Se envian los datos de @namespace  result
	 */
	$.ajax({
		type: 'PUT',
		url: url_base.format('api/v1/tickets/category/{0}/'.format(id['value'])),
		contentType: 'application/json',
		data: JSON.stringify(result),
		success: function() {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Cambio exitosos',
				sticky: false,
				time: ''
			});
			/**@JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
			$('#table-category')
				.DataTable()
				.ajax.reload();
		},
		error: function(error) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Resultado:',
				text: 'Ocurrio un error',
				sticky: false,
				time: ''
			});
		}
	});
});

/**
 * @API [ajax]: Se consultan segun los valores que esta defindo en la url
 */
$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}'.format( $('#menu_operators').val() )),
	success: function(data) {
		/**
		 * @var {string} html
		 * @JselectorElement {id_element}
		 */
		let html = '<option value={0}>{1}</option>';
		let select = $('#change_category_parent');
		/** Se realiza un forEach para renderizar el componente select */
		data.forEach(function(item) {
			html_render = html.format(item['ID'], item['name']);
			select.append(html_render);
		});

		select.select2({
			placeholder: 'Seleccione una tipificación'
		});
		select
			.nextAll()
			.attr('style', 'width: 400px !important');

	},
	error: function(error) {
		//TODO: Vacia
	}
});

$('#change_category_classification').select2({
	placeholder: 'Seleccione una categoria'
});
$('#change_category_classification')
	.nextAll()
	.attr('style', 'width: 400px !important');