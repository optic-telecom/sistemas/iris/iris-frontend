/**
 * @author {Cristian Cantero}
 *  @file {view_template}
 */

/**
 * @function template_change
 * @param {*} id
 */
function template_change(id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/template_change');
	var url_item = url_base.format('api/v1/communications/template/{0}/'.format(id));

	/**
	 * @function post_ajax funcion interna
	 * @namespace {object} data
	 */
	function post_ajax(data) {
		/**
		 * Configuracion de los parametros de a mostrar en la vista
		 * Se cargan los datos en los @JselectorElement {id_element}  paramatrizado con el valor obtenido de data
		 */
		$('#change_template_id').val(data['ID']);
		$('#change_template_name').val(data['name']);
		$('#change_template_html').val(data['template_html']);
		$('#change_template_text').val(data['template_text']);
		$('#change_template_subject').val(data['subject']);
		$('#change_template_operator').val($('#menu_operators').val());
	}
	/**
	 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-template').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Empresa',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Empresa de _START_ a _END_ de un total de _TOTAL_ Empresa',
		sInfoEmpty: 'Mostrando Empresa del 0 al 0 de un total de 0 Empresa',
		sInfoFiltered: '(filtrado de un total de _MAX_ Empresas)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/communications/template/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
		}
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<button onclick='template_change({0})' class='btn btn-indigo' data-toggle='modal'>Modificar</button>";
				return html.format(data);
			},
			targets: 2
		},
		{
			render: function(data, type, row) {
				html = "<button onclick='removeItem(`{0}`, `{1}`)' class='btn btn-info' data-toggle='modal'>Borrar</button>";
				url = url_base.format('api/v1/communications/template/{0}/'.format(data));
				dataTable = '#table-template';
				return html.format(url, dataTable);
			},
			targets: 3
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 1]
		},
		{
			orderable: false,
			searchable: false,
			targets: [2, 3]
		}
	],
	columns: [{ name: 'ID' }, { name: 'name' }, { name: 'Modificar' }, { name: 'Borrar' }]
});

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
	$('#table-template')
		.DataTable()
		.ajax.reload();
});
