/**
 * @author {Cristian Cantero}
 *  @file {}
 */

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form_change_classification').submit(function(e) {
	e.preventDefault();
	/** @var {array} form */
	form = $(this).serializeArray();
	id = form.shift();
	/** @var {string} url  ruta a la cual seran enviado los datos. */
	url = url_base.format('api/v1/tickets/classification/{0}/'.format(id['value']));
	/**
	 * Llama a la @function {@param, @namespace, @param, @function} changeItem del @file {index.js}
	 */
	changeItem(url, form, '#table-classification');
});

/**
 * Llama a la @function {@param, @param, @param, @param, @function} listItems del @file {index.js}
 */
listItems($('#classification_operator'), url_base.format('api/v1/tickets/operator/?fields=ID,name'), 'name', 'ID');
