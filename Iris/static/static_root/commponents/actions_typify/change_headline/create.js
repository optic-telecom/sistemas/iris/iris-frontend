/**
 * @author {Cristian Cantero}
 *  @file {change_headline_list}
 * /

/**
 * @JselectorElement {id_element} @plugin {config} select2
 * @API [ajax]: donde se obtienes los datos de factibilidad que esta asociada al registro
 */
$('#new_service_headline').select2({
	placeholder: 'Seleccione plan',
	ajax: {
		url: url_base.format('api/v1/matrix/customers/?fields=name,rut'),
		dataType: 'json',
		data: function(params) {
			var query = {
				rut__icontains: params.term,
				type: 'public'
			};
			return query;
		},
		processResults: function(data) {
			return {
				results: data.map(function(item) {
					return { id: item.rut, text: '{0} - {1}'.format(item.rut, item.name) };
				})
			};
		}
	}
});

/**
 * @JselectorElement {id_element} @module {attr} estilo para controlar el tamaño
 */
$('#new_service_headline')
	.nextAll()
	.attr('style', 'width: 100% !important');
