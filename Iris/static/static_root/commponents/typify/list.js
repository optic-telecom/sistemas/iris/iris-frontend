/**
 * @author {Cristian Cantero}
 *  @file { typify_create, typify_create_queue, typify_opened, typify_create_me, typify_last_hours}
 */

var init_time = '';
var end_time = '';
var QUERIES = [];

/**
 * @function {void} current_values Contiene los valores acuales del los filtros.
 * @returns {@namespace } los valores que estan actualmente todos los filtros.
 */
function current_values() {

	return {
		draw: '1',

		'columns[0][data]': '0',
		'columns[0][name]': 'ID',
		'columns[0][searchable]': 'true',
		'columns[0][orderable]': 'true',
		'columns[0][search][value]': '',
		'columns[0][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'category',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'columns[2][data]': '2',
		'columns[2][name]': 'customer_type',
		'columns[2][searchable]': 'true',
		'columns[2][orderable]': 'true',
		'columns[2][search][value]': '',
		'columns[2][search][regex]': 'false',

		'columns[3][data]': '3',
		'columns[3][name]': 'services',
		'columns[3][searchable]': 'true',
		'columns[3][orderable]': 'true',
		'columns[3][search][value]': '',
		'columns[3][search][regex]': 'false',

		'columns[4][data]': '4',
		'columns[4][name]': 'liveagent',
		'columns[4][searchable]': 'true',
		'columns[4][orderable]': 'true',
		'columns[4][search][value]': '',
		'columns[4][search][regex]': 'false',

		'columns[5][data]': '5',
		'columns[5][name]': 'channel',
		'columns[5][searchable]': 'true',
		'columns[5][orderable]': 'true',
		'columns[5][search][value]': '',
		'columns[5][search][regex]': 'false',

		'columns[6][data]': '6',
		'columns[6][name]': 'type',
		'columns[6][searchable]': 'true',
		'columns[6][orderable]': 'true',
		'columns[6][search][value]': '',
		'columns[6][search][regex]': 'false',

		'columns[7][data]': '7',
		'columns[7][name]': 'status',
		'columns[7][searchable]': 'true',
		'columns[7][orderable]': 'true',
		'columns[7][search][value]': '',
		'columns[7][search][regex]': 'false',

		'columns[8][data]': '8',
		'columns[8][name]': 'urgency',
		'columns[8][searchable]': 'true',
		'columns[8][orderable]': 'true',
		'columns[8][search][value]': '',
		'columns[8][search][regex]': 'false',

		'columns[9][data]': '9',
		'columns[9][name]': 'city',
		'columns[9][searchable]': 'true',
		'columns[9][orderable]': 'true',
		'columns[9][search][value]': '',
		'columns[9][search][regex]': 'false',

		'columns[10][data]': '10',
		'columns[10][name]': 'management',
		'columns[10][searchable]': 'true',
		'columns[10][orderable]': 'true',
		'columns[10][search][value]': '',
		'columns[10][search][regex]': 'false',

		'columns[10][data]': '11',
		'columns[10][name]': 'discount_reason',
		'columns[10][searchable]': 'true',
		'columns[10][orderable]': 'true',
		'columns[10][search][value]': '',
		'columns[10][search][regex]': 'false',

		'columns[10][data]': '12',
		'columns[10][name]': 'discount_responsable_area',
		'columns[10][searchable]': 'true',
		'columns[10][orderable]': 'true',
		'columns[10][search][value]': '',
		'columns[10][search][regex]': 'false',

		'order[0][column]': '0',
		'order[0][dir]': 'desc',
		start: '0',
		length: '-1',
		'search[value]': $('#table-typify_filter > label > input').val(),
		'search[regex]': 'false',
		urgency: $('#typify_urgency_query').val(),
		category: $('#typify_category_query').val(),
		city: $('#typify_city_query').val(),
		management: $('#typify_management_query').val(),
		operator: $('#menu_operators').val(),
		customer_type: $('#typify_customer_query').val(),
		agent: $('#typify_agent_query').val(),
		status: $('#typify_status_query').val(),
		endtime: $('#typify_datetimeend_query').val(),
		inittime: $('#typify_datetimeinit_query').val(),
		discount_reason: $('#typify_discount_reason_query').val(),
		discount_responsable_area: $('#typify_discount_responsable_area_query').val(),
		custom: $('#typify_custom').val(),

		_: '1568749802687'
	};
}

/**
 * @function {@param }count_record cuenta los valores sengun sea el caso del @param
 * @param {*} values este valor esta indefinido por defecto.
 * cuando values es modificado el count_record se ajusta a las seleciones de los filtros
 */
function count_record(values = undefined) {
	/**
	 * @API [ajax]: El cual se consulan segun los valores que esta defindo en @this @function @param
	 */
	$.ajax({
		type: 'GET',
		dataType: 'json',
		data: values ? values : current_values(),
		url: url_base.format('api/v1/tickets/typify/count_occurrences/')
	}).done(function(data) {
		/**
		 * @namespace {object} _id  contiene los Id de los @JselectorElement
		 *
		 * */
		var _id = {
			category: 'typify_category_query',
			city: 'typify_city_query',
			management: 'typify_management_query',
			urgency: 'typify_urgency_query',
			customer_type: 'typify_customer_query',
			agent: 'typify_agent_query',
			status: 'typify_status_query',
			discount_reason: 'typify_discount_reason_query',
			discount_responsable_area: 'typify_discount_responsable_area_query',
			
		};

		/**
		 * @since [For] que cargara los datos segun @param {array} data
		 * Se hace como primer recorrido del arreglo
		 */
		for (var key_data in data) {
			var data_count = data[key_data];

			/**
			 * @since [For] que cargara los datos segun @param {array} data_count
			 * Se hace como segundo recorrido de los elementos.
			 */
			for (var key_item in data_count) {
				var elements = $('#{0}'.format(_id[key_data])).find('option');

				/**
				 * @since [For] que cargara los datos segun @param {array} elements que esten disponibles.
				 * Se hace como ultimo recorrido de los elementos.
				 */
				for (i = 0; i < elements.length; i++) {
					item = elements[i];

					urgency_value = item.value;

					urgency_count = urgency_value == 'ignore' || !urgency_value ? '' : data_count[item.value] ? data_count[item.value] : 0;

					item.innerText = item.innerText.split(':')[0] + ': ' + urgency_count;
				}

				/**
				 *  @JselectorElement {id_selector} @plugin { actions } select2 limpia el el select del elemento
				 *  @JselectorElement {id_selector} @plugin {void } select2 prepara el elemento para los datos.
				 */
				$('#{0}'.format(_id[key_data])).select2('destroy');
				$('#{0}'.format(_id[key_data])).select2();
			}
		}
	});
}

/**
 * @callback accion del hacer click al botton descargar data.
 * @JselectorElement {id_selector} @module @instance
 *  Se descarga la data, usando el evento {trigger[submit]}
 */
$('#button_download_data').click(function() {
	$('#download_data').trigger('submit');
});

/**
 * @JselectorElement {id_selector} @module @instance
 *  Se descargan la data que esta diponible en la data table.
 *  La misma sera enviada al correo.
 */
$('#download_data').submit(function(event) {
	/** Se captura el evento completo */
	event.preventDefault();

	/** Se leen todos los valores que estan por defectos en los filtros */
	var data = current_values();

	/** Se lee el correo al cual sera enviado los datos  */
	data['email'] = $('#email_download_data').val();

	/** @var {string} urlToSend se construye la ur con todos los parametros */
	var urlToSend = url_base.format('api/v1/tickets/typify/file_typify/') + '?{0}'.format(jQuery.param(data));

	/** @plugin {notfication} Se le notifiaca el resultado al operador */
	$.gritter.add({
		title: 'Resultado:',
		text: 'Informacioón será enviada al correo en breve',
		sticky: false,
		time: ''
	});

	/**
	 * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @var
	 */
	$.ajax({
		type: 'GET',
		url: urlToSend
	});
});

/**
 * @var {table} dt_table[@todo] @JselectorElement @plugin
 *  Se configura todo el DataTable completo el cual se mostara para el [tag] de @file {htlm} tipificaciones.
 */
var dt_table = $('#table-typify').DataTable({
	dom: 'lfrtip',
	lengthMenu: [[10, 25, 50, -1], ['10', '25', '50', 'Todo']],
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Tipificaciones',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Tipificaciones de _START_ a _END_ de un total de _TOTAL_ Tipificaciones',
		sInfoEmpty: 'Mostrando Tipificaciones del 0 al 0 de un total de 0 Tipificaciones',
		sInfoFiltered: '(filtrado de un total de _MAX_ Tipificaciones)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	initComplete: function(settings, json) {
		/**
		 *@function {[values]} current_values (default)
		 * Se cargaran los datos segun como esten los filtros para el momento.
		 */
		count_record(current_values());
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/typify/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.urgency = $('#typify_urgency_query').val();
			d.category = $('#typify_category_query').val();
			d.city = $('#typify_city_query').val();
			d.management = $('#typify_management_query').val();
			d.operator = $('#menu_operators').val();
			d.customer_type = $('#typify_customer_query').val();
			d.agent = $('#typify_agent_query').val();
			d.status = $('#typify_status_query').val();

			/** Se carga para la razon el area del problema */
			d.discount_reason = $('#typify_discount_reason_query').val();
			d.discount_responsable_area = $('#typify_discount_responsable_area_query').val();

			/** Se define para el area del tiempo. */
			d.endtime = $('#typify_datetimeend_query').val();
			d.inittime = $('#typify_datetimeinit_query').val();
			d.custom = $('#typify_custom').val();
		}
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<a href='javascript:;' style='cursor: pointer;' onclick='show_typify(`{0}`)'>{0}</a>";
				return html.format(data);
			},
			targets: 2
		},
		{
			render: function(data, type, row) {
				/** Se muestra la fecha en que se guardo en el servidor. */
				return moment(data)
					.tz(moment.tz.guess())
					.calendar();
			},
			targets: 1
		},
		{
			render: function(data, type, row) {
				/** Se muestra la horas que han transcurrido desde que se creo el registro */
				return moment(data)
					.tz(moment.tz.guess())
					.fromNow();
			},
			targets: 0
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
		}
	],
	columns: [
		{ name: 'created' },
		{ name: 'created' },
		{ name: 'ID' },
		{ name: 'category' },
		{ name: 'customer_type' },
		{ name: 'services' },
		{ name: 'liveagent' },
		{ name: 'channel' },
		{ name: 'type' },
		{ name: 'status' },
		{ name: 'urgency' },
		{ name: 'city' },
		{ name: 'management' }
	]
});

/**
 * Se le adiciona un evento on[search.dt'] donde se le adiciona @function {[values]} count_record
 * Se cargaran todos los datos segun como esten los filtros en su momento.
 */
dt_table.on('search.dt', function(event) {
	count_record(current_values());
});

/**
 * @function {void} handleDataTableRowReorder
 * funcion declarada para hacer responsiba la tabla y reordenar la misma.
 */
var handleDataTableRowReorder = function() {
	'use strict';

	if ($('#table-typify').length !== 0) {
		$('#table-typify').DataTable({
			responsive: true,
			rowReorder: true
		});
	}
};

/**
 * //TODO: Preguntar que hace esta funcion dentro del html. {Tener en cuenta para los plugins.}
 * @function {void} TableManageRowReorder
 * @returns @function {void } handleDataTableRowReorder
 */
var TableManageRowReorder = (function() {
	'use strict';
	return {
		//main function
		init: function() {
			handleDataTableRowReorder();
		}
	};
})();

//
/**
 * @JselectorElement {id_element } @module {event}
 * Seccion de cambios masivos.
 */
$('#masiveChange').click(() => {
	/** Se limpia la modal antes de montar lo que se va a mostrar */
	$('#modal-html').empty();

	/** Se le agrega un titulo de preparacio del modal */
	$('#modal-title').text('Cambios Masivos');

	/** @function {@param @JselectorElement } callView para mostrar un modal */
	callView('masive_change', $('#modal-html'));

	/** Se muestra el modal. */
	$('#modals').show();

	/** Se le agrega la funcionalidad de Select2 */
	$('#masive_typify_new_agent').select2();

	/** Se le agrega la funcionalidad de Select2 */
	$('#masive_typify_new_queue').select2();

	/** Se le agrega la funcionalidad de Select2 */
	$('#masive_typify_new_status').select2();

	/** Cargar los agentes para el cambio masivo. */
	listItems($('#masive_typify_new_agent'), url_base.format('api/v1/user/data/information/'), 'username', 'pk');

	/** Se agregan las variables que van a ser ajustadas. */
	let data = { new_status: '', new_agent: '', new_queue: '' };

	/** Agregan las variables que estan por defecto en los filtros. */
	data = current_values();

	/**
	 * @JselectorElement {id_element} @module {event}
	 * Se carga los datos segun las selecciones en los select, basado en los eventos que pasan en elemento seleccionado.
	 * Este apartado esta desarrollado como TypeScript Event el cual captura los datos de evento.
	 */
	$('#formMasiveChanges').change((e) => {
		e.preventDefault();
		data[e.target.name] = e.target.value;
	});

	/** Evento el cual se mandaran todos los cambios a realizar. */
	$('#modal_button').click(() => {
		/** Se le notifica al operados si esta seguro de hacer los cambios. */
		swal(
			{
				title: 'Alerta!',
				text: 'Seguro que desea realizar los Cambios Masivos',
				type: 'warning',
				showCancelButton: true,
				confirmButtonClass: 'btn-danger',
				confirmButtonText: 'Si, Cambiar!',
				cancelButtonText: 'No, Cancelar',
				closeOnConfirm: true,
				closeOnCancel: false
			},
			function(isConfirm) {
				if (isConfirm) {
					/** Se confirma si al menos unos da las variables se va modificar. */
					if (data.new_status !== undefined || data.new_agent !== undefined || data.new_queue !== undefined) {
						/**
						 * Se envia a la información a la @API [ajax]
						 */
						$.post(url_base.format('api/v1/tickets/typify/massive_change/'), data, (data, status, xhr) => {
							if (status === 'success' && xhr.status === 200) {
								/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
								$('#table-typify')
									.DataTable()
									.ajax.reload();
								setTimeout(function() {
									/** Se le notifica al operados. */
									swal('Muy bien!', 'Los cambios masivos fueron aplicados.', 'success');
								}, 2000);
							} else {
								setTimeout(function() {
									/** Se le notifica al operados. */
									swal('Alerta!', 'Ningún cambion fue aplicado', 'error');
								}, 2000);
							}
						});
					} else {
						setTimeout(function() {
							/** Se le notifica al operados. */
							swal('Alerta!', 'Ningún cambio fue aplicado - consulta vacia', 'warning');
						}, 2000);
					}
				} else {
					/** Se le notifica al operados. */
					swal('Cancelado!', 'Ningún cambion fue aplicado', 'success');
				}
			}
		);
	});
});

/** Filtro de ciudad. */
$('#typify_city_query').select2({ placeholder: 'Seleccione un comuna' });

/** Filtro de gestion. */
$('#typify_management_query').select2({ placeholder: 'Gestión' });

/** Filtro de Urgencia. */
$('#typify_urgency_query').select2({ placeholder: 'Seleccione una urgencia' });

/** Filtro de clientes. */
$('#typify_customer_query').select2({ placeholder: 'Seleccione un cliente' });

/** Filtro de agente. */
$('#typify_agent_query').select2({ placeholder: 'Seleccione un agente' });

/** Filtro de estado. */
$('#typify_status_query').select2({ placeholder: 'Seleccione un agente' });

/** Filtro de motivo de descuento. */
$('#typify_discount_reason_query').select2({
	placeholder: 'Seleccione un motivo'
});

/** Filtro de descuento area responsable de responsable. */
$('#typify_discount_responsable_area_query').select2({
	placeholder: 'Seleccione un área responsable'
});

/**
 * @JselectorElement {class_element}  @module {event}
 * Se cargaran todos los datos cuando ocurran cambios en los filtros.
 * @callback @function {[values]} count_record Con los valores que esten al momento en los filtros.
 *  @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */

$('.query').change(function() {
	count_record(current_values());

	$('#table-typify')
		.DataTable()
		.ajax.reload();
});

/**
 * @function add_two_point La cual coloca los dos puntos en datos de los selectores.
 * @param {*} id
 * @param {*} message
 */
function add_two_point(id, message) {
	var items = $('#{0}'.format(id)).find('option');

	for (i = 0; i < items.length; i++) {
		items[i].innerText = items[i].innerText + ':';
	}

	$('#{0}'.format(id)).select2({
		placeholder: message
	});
}

/** Se cargaran segun este espacio. */
$("select[name='table-typify_length']").select2();

/**
 * @file {indes.js}
 *  @callback {@function} listItems
 */
listItems(
	$('#typify_category_query'),
	url_base.format('api/v1/tickets/category/?fields=ID,name&operator__ID={0}'.format($('#menu_operators').val())),
	'name',
	'ID'
);

/**
 * @file {@this}
 *  @callback {@function} listItems
 */
add_two_point('typify_category_query');

/**
 * @file {indes.js}
 *  @callback {@function} listItems
 */
listItems($('#typify_agent_query'), url_base.format('api/v1/user/data/information/'), 'username', 'pk');

/**
 * @file {@this}
 *  @callback {@function} listItems
 */
add_two_point('typify_agent_query');

/**
 * @function show_typify Muestra las tipificacion
 * @param {number} ID
 */
function show_typify(ID) {
	if (!$('#typify_view_{0}'.format(ID)).length) {
		/** @var {array} data */
		var data = [ID];

		/** @var {string} html_typify elemento de para mostrar la tipificacion */
		var html_typify = `<div id="typify_view_{0}" class="tab-pane fade view_detail">
                                <div>`;

		/** @var {string} html_button_typify transforma los elementos [li] en botones */
		var html_button_typify = `<li style="cursor: pointer;" class="nav-items change_view" id="button_{0}">
                                            <a data-toggle="tab" class="nav-link " href="typify_view_{0}">
                                                <span>{0}</span>
                                            </a>
                                        </li>`;

		/** @var ID se le hace parseInt a base de 10 para que el valor quede entero.  */
		ID = parseInt(ID, 10);

		/**
		 * @JselectorElement {id_element} @module {event}
		 * @JselectorElement {id_element} @module {event}
		 */
		$('#create_view').after(html_typify.format(ID));
		$('#list_show').append(html_button_typify.format(ID));

		/**
		 * @JselectorElement {class_element} @module{event}
		 * */
		$('.change_view').click(function() {
			/**
			 * @var {string} ID se obtiene el nombe de id
			 * @var {string} id_detail el valor es por defecto.
			 */
			var ID = $(this).attr('id');
			var id_detail = '';

			/**
			 * @JselectorElement {class_element} @module{event} remover clases activo
			 * @JselectorElement {class_element} @module{event} remover clase show
			 */
			$('.view_detail').removeClass('active');
			$('.view_detail').removeClass('show');

			/** segun el id se inicializa el @var id_detail */
			switch (ID) {
				case 'show_create':
					id_detail = 'create_view';

					break;

				case 'show_list':
					id_detail = 'list_view';

					break;

				default:
					id_detail = 'typify_view_{0}'.format(ID.replace('button_', ''));
			}

			/**
			 * @JselectorElement {class_element} @module{event} agrega clases activo
			 * @JselectorElement {class_element} @module{event} agrega clase show
			 */
			$('#{0}'.format(id_detail)).addClass('active');
			$('#{0}'.format(id_detail)).addClass('show');
		});

		/**
		 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
		 */
		callView('typify_change', $('#typify_view_{0}'.format(ID)), data);

		/** @callback @function {@param } */
		list_typify(ID); //TODO: funcion que no va a ningun lado
	}
}

/** Se agrega formato al @JselectorElemento {id_element} @plugin {datetimepicker}. */
$('#typify_datetimeinit_query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/** Se agrega formato al @JselectorElemento {id_element} @plugin {datetimepicker}. */
$('#typify_datetimeend_query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/**
 * @JselectorElement {id_element}  @module {event}
 * Se cargaran todos los datos cuando cuando se selecciones el inicio de la fecha para el filtro
 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */

$('#typify_datetimeinit_query').on('dp.change', function(e) {
	$('#table-typify')
		.DataTable()
		.ajax.reload();
});

/**
 * @JselectorElement {id_element}  @module {event}
 * Se cargaran todos los datos cuando cuando se selecciones el final de la fecha para el filtro
 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */
$('#typify_datetimeend_query').on('dp.change', function(e) {
	$('#table-typify')
		.DataTable()
		.ajax.reload();
});

/**
 * @async setTimeout
 * Se implementa un minimo y maximo para la seleccion del evento en la
 * iniciacion del seleccionar el rango de fechas.
 */
setTimeout(function() {

	if ( $('#typify_datetimeend_query').length ){

		$('#typify_datetimeend_query')
		.data('DateTimePicker')
		.maxDate(moment(new Date()));
		$('#typify_datetimeinit_query')
		.data('DateTimePicker')
		.minDate(moment(new Date(2010, 01, 01)));

	}

}, 1000);
