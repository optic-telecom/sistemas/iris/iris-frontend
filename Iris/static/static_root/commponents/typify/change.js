/**
 * @author {Cristian Cantero}
 *  @file { typify_create -> show.html }
 */

/**
 * @function tr_equal - funcion estandar para las columnas de las tablas
 * @param {string} name 
 * @param {string} old 
 * @param {string} current
 * @return @var {html} tr_table formateado.  
 */

var ROAD = []

function tr_equal(name, old, current){

    /** @var {string} tr_table se colocara cada ves que sea llamada */
    var tr_table = 
                `<tr class="{0}">
                    <td class="bg-silver-lighter">{1}</td>
                    <td class="{0}">
                        <a class="editable editable-click">{2}</a>
                    </td>
                    <td class="{0}">
                        <a class="editable editable-click">{3}</a>
                    </td>
                </tr>`

    return tr_table.format( (old == current)? '': 'danger', name, old, current)

}

/** @namespace {object} FIELD_NAMES - @global */
FIELD_NAMES = {
    'category':'Primera tipificación',
    'second_category': 'Segunda tipificación',
    'parent': 'Tipificación padre',
    'agent': 'Agente asignado',
    'customer': 'Cliente',
    'services': 'Servicios',
    'commentaries': 'Comentarios', 
    'liveagent': 'Liveagent',
    'channel':'Canal',
    'type':'Tipo',
    'status': 'Estado',
    'urgency':'Urgencia',
    'city':'Ciudad',
    'management':'Gestón',
    'technician':'Técnico',
}

/**
 * @function readable_field 
 * @param {string} field 
 * @param {string} value 
 */
function readable_field(field, value){
    /** se verifica que el @param no este null */
    if (value == null){return 'No indica'}

    result = value

    /** @namespace {object} FIELD_NAMES - @local */
    FIELD_NAMES = {
        'category':{
            'url':url_base.format('api/v1/tickets/category/{0}/?fields=name'.format(value)),
            'field':'name',
        },
        'second_category': {
            'url':url_base.format('api/v1/tickets/category/{0}/?fields=name'.format(value)),
            'field':'name',
            },
        'parent': null,
        'agent': {
            'url':url_base.format('api/v1/user/data/{0}/?fields=username'.format(value)),
            'field':'username',
        },
        'updater': {
            'url':url_base.format('api/v1/user/data/{0}/?fields=username'.format(value)),
            'field':'username',
        },
        'customer': null,
        'services': null,
        'commentaries': null, 
        'liveagent': null,
        'channel':null,
        'type':null,
        'status': null,
        'urgency':null,
        'city':null,
        'management':null,
        'technician':{
            'url':url_base.format('api/v1/matrix/technicians/{0}/?fields=name'.format(value)),
            'fields':'name',
        },
    }

    /** se verifica que @namespace tenga en su indice el @param field */
    if (FIELD_NAMES[field]){

        /**
         * @function get_data
         * @param {string} field 
         * @param {string} url 
         */
        function get_data(field, url){

            var result = null
            /**
             * @API [ajax]: El cual se consulan segun los valores que esta defindo en  @var
             */
            $.ajax({
                type: "GET",
                dataType: "json",
                async: false,
                url: url,
                complete: function(data) {

                    fields = {
                        'category':'name',
                        'agent':'username',
                        'updater':'username',
                        'technician':'name',
                        'second_category':'name',
                    }

                    result = data.responseJSON[fields[field]]
                    
                },
            });

            return result

        }

        result = get_data(field, FIELD_NAMES[field]['url'])

    }

    return result

}

function removeItemPanel(){

    $('.far.fa-window-close').click(function(){

        
        removeItem( url_base.format( $(this).attr('url') ), null, (ele = $(this)) => ele.parent().parent().parent().parent().remove() )
        

    })


}

function chat(id_typify, chat_id){

    $("#internal-message-chat-id-{0}".format(id_typify)).val(chat_id)

    function form_change(id){
        /**
         * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
         */
        $('#change-chat-input-{0}'.format(id)).submit(function(event){

            event.preventDefault()

            $.ajax({
                type: "PATCH",
                dataType: "json",
                url: url_base.format('api/v1/communications/internal_chat/{0}/'.format( id )),
                data: $(this).serializeArray(),
                extra:{
                    'ID':id,
                },
                success: function(data) {

                    ID = this.extra['ID']

                    $('#message-ID-{0}'.format(ID)).text(data['message'])
                    $('#change-chat-input-{0}'.format(ID)).hide()

                },
            })

        })
    }

    function icon_change(id){

        $('#change-chat-icon-{0}'.format(id)).click(function(){

            
            $('#change-chat-input-{0}'.format(id)).show()

        })

    }

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
    $('#internal-message-form-{0}'.format(id_typify)).submit(function(event){

        event.preventDefault()
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url_base.format('api/v1/communications/internal_chat/'),
            data: $(this).serializeArray(),
            success: function(data) {

                var HTML =  `<li class="message right appeared">
                                <div class="text_wrapper">
                                    <div style="font-size: 1rem; display: inline-flex;" >
                                        <div>
                                            <i class="fas fa-edit fa-fw" style="cursor: pointer;" id="change-chat-icon-{3}"></i>
                                        </div>
                                        <div>
                                            <i class="far fa-window-close" style="cursor: pointer;" url="api/v1/communications/internal_chat/{3}/"></i>
                                        </div>
                                    </div>
                                    <div class="float-right">
                                        <p class="text-dark" >{0}</p>
                                    </div>
                                    <div class="text" id="message-ID-{3}">{2}</div>
                                    <div>
                                        <form name="send_message_form" data-id="message-form" id="change-chat-input-{3}" style="display:none;" >
                                            <div class="input-group">
                                                <input  class="form-control" required type="text"  name="message" value="{2}">
                                                <input required type="hidden" id="change-chat-input-ID-{3}" value="{3}">
                                                <span class="input-group-append">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i></button>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="float-right">
                                        <p class="text-dark" >{1}</p>
                                    </div>
                                </div>
                            </li>`

                var list_message = $("#internal-message-list-{0}".format(id_typify))
                var message_info = data

                list_message.prepend(HTML.format(
                    $('#user_name').text(),
                    moment(message_info['created']).tz(moment.tz.guess()).startOf('minute').fromNow(),
                    message_info['message'],
                    message_info['ID'],
                ))

                form_change(message_info['ID'])
                icon_change(message_info['ID'])
                removeItemPanel()
                
            },
        });
    
    })

    $.ajax({
        type: "GET",
        dataType: "json",
        url: url_base.format('api/v1/communications/internal_chat/?ordering=-created&offset=0&limit=1000&fields=creator,created,message,ID&chat={0}'.format(chat_id)),
        params:{'id_typify':id_typify},
        complete: function(data) {

            $.ajax({
                type: "GET",
                dataType: "json",
                url: url_base.format('api/v1/user/data/information/'),
                params:{
                    'id_typify':this.params['id_typify'],
                    'data_chat':data.responseJSON,
                },
                complete: function(data) {

                    data_users = {}

                    data.responseJSON.forEach(function(item, index){

                        data_users[item['pk']] = item['username']

                    })

                    var id_typify = this.params['id_typify']
                    var data = this.params['data_chat']

                    var HTML =  `<li class="message right appeared">
                                    <div class="text_wrapper">
                                        <div style="font-size: 1rem; display: inline-flex;">
                                            <div>
                                                <i class="fas fa-edit fa-fw" style="cursor: pointer;" id="change-chat-icon-{3}"></i>
                                            </div>
                                            <div>
                                                <i class="far fa-window-close" style="cursor: pointer;" url="api/v1/communications/internal_chat/{3}/"></i>
                                            </div>
                                        </div>
                                        <div class="float-right">
                                            <p class="text-dark" >{0}</p>
                                        </div>
                                        <div class="text" id="message-ID-{3}">{2}</div>
                                        <div>
                                            <form name="send_message_form" data-id="message-form" id="change-chat-input-{3}" style="display:none;" >
                                                <div class="input-group">
                                                    <input  class="form-control" required type="text"  name="message" value="{2}">
                                                    <input required type="hidden" id="change-chat-input-ID-{3}" value="{3}">
                                                    <span class="input-group-append">
                                                        <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i></button>
                                                    </span>
                                                </div>
                                            </form>
                                        </div>
                                        <div class="float-right">
                                            <p class="text-dark" >{1}</p>
                                        </div>
                                    </div>
                                </li>`
                    

                    var list_message = $("#internal-message-list-{0}".format(id_typify))
                    var message_info = null

                    for (i = 0; i < data.length; i++){

                        message_info = data[i]
                        
                        list_message.append(HTML.format(
                            data_users[message_info['creator']],
                            moment(message_info['created']).tz(moment.tz.guess()).startOf('minute').fromNow(),
                            message_info['message'],
                            message_info['ID'],
                        ))

                        form_change(message_info['ID'])
                        icon_change(message_info['ID'])

                    }

                    removeItemPanel()

                },
            });

        },
    });

}

function new_contact(id_typify){
    

    $('#typify_channel_new_contact-{0}'.format(id_typify)).select2({placeholder: "Seleccione canal"})
    $('#typify_channel_new_contact-{0}'.format(id_typify)).nextAll().attr('style', 'width: 100% !important');

    function form_change(id){

        $('#re-contact-choice-ID-{0}'.format(id)).select2()
        $('#re-contact-choice-ID-{0}'.format(id)).nextAll().attr('style', 'width: 100% !important');
        /**
         * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
         */
        $('#change-re-contact-input-{0}'.format(id)).submit(function(event){

            event.preventDefault()

            var values = $(this).serializeArray()
            values.push(
                {
                    'name':'operator',
                    'value':$('#menu_operators').val()
                }
            )

            $.ajax({
                type: "PUT",
                dataType: "json",
                url: url_base.format('api/v1/tickets/new_contact/{0}/'.format( id )),
                data: values,
                extra:{
                    'ID':id,
                },
                success: function(data) {

                    ID = this.extra['ID']

                    $('#re-contact-text-{0}'.format(ID)).text(data['commentary'])
                    $('#re-contact-channel-{0}'.format(ID)).text(data['channel'])
                    $('#change-re-contact-input-{0}'.format(ID)).hide()

                },
            })

        })
    }

    function icon_change(id){

        $('#re-contact-chat-icon-{0}'.format(id)).click(function(){

            
            $('#change-re-contact-input-{0}'.format(id)).show()

        })

    }

    /**
     * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
     */
    $('#new-contact-form-{0}'.format(id_typify)).submit(function(event){

        event.preventDefault()
        var data = $(this).serializeArray()
        data.push({
            'name': 'operator',
            'value': $('#menu_operators').val(),
        })
        
        $.ajax({
            type: "POST",
            dataType: "json",
            url: url_base.format('api/v1/tickets/new_contact/'),
            data: data,
            success: function(data) {
                
                var id_typify = data['typify'].split('/api/v1/tickets/typify/')[1].replace('/','')
            
                var HTML =   `<li class="message right appeared">
                                <div class="text_wrapper">
                                    <div style="font-size: 1rem; display: inline-flex;">
                                        <div>
                                            <i class="fas fa-edit fa-fw" style="cursor: pointer;" id="re-contact-chat-icon-{4}"></i>
                                        </div>
                                        <div>
                                            <i class="far fa-window-close" style="cursor: pointer;" url="api/v1/tickets/new_contact/{4}/"></i>
                                        </div>
                                    </div>
                                    <H5 id="re-contact-channel-{4}" >{2}</H5>
                                    <div class="float-right">
                                        <p class="text-dark" >{0}</p>
                                    </div>
                                    <div class="text" id="re-contact-text-{4}">{3}</div>
                                    <div>
                                        <form name="send_message_form" data-id="message-form" id="change-re-contact-input-{4}" style="display:none;">
                                            <div class="input-group">
                                                <input required type="text" class="form-control" name="commentary" value="{3}">
                                                <input required type="hidden"  value="{5}" name="typify_id">
                                                <span class="input-group-append">
                                                    <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i></button>
                                                </span>
                                            </div>
                                            <div>
                
                                                <select required type="text" name="channel" id="re-contact-choice-ID-{4}" required class="form-control advance-select" data-parsley-group="step-1" data-parsley-required="false" data-parsley-type="alphanum" >
                                                    <option></option>
                                                    <option value="LLAMADA">LLAMADA</option>
                                                    <option value="CORREO">CORREO</option>
                                                    <option value="WHATSAPP">WHATSAPP</option>
                                                    <option value="REDES">REDES</option>
                                                    <option value="CHAT">CHAT</option>
                                                </select>
                
                                            </div>
                                        </form>
                                    </div>
                                    <div class="float-right">
                                        <p class="text-dark" >{1}</p>
                                    </div>
                                </div>
                            </li>`

                var list_message = $("#new-contact-list-{0}".format(id_typify))
                var message_info = data

                list_message.prepend(HTML.format(
                    data_users[message_info['creator']],
                    moment(message_info['created']).tz(moment.tz.guess()).startOf('minute').fromNow(),
                    message_info['channel'],
                    message_info['commentary'],
                    message_info['ID'],
                    id_typify
                ))

                form_change(message_info['ID'])
                icon_change(message_info['ID'])

                $('#re-contact-choice-ID-{0}'.format(message_info['ID'])).val(message_info['channel']).change()
                removeItemPanel()
                
            },
        });
    
    })

    $.ajax({
        type: "GET",
        dataType: "json",
        url: url_base.format('api/v1/tickets/new_contact/?ordering=-created&typify={0}&offset=0&limit=1000&fields=creator,created,channel,commentary,ID'.format(id_typify)),
        params:{'id_typify':id_typify},
        complete: function(data) {

            $.ajax({
                type: "GET",
                dataType: "json",
                url: url_base.format('api/v1/user/data/information/'),
                params:{
                    'id_typify':this.params['id_typify'],
                    'data':data.responseJSON,
                },
                complete: function(data) {

                    data_users = {}

                    data.responseJSON.forEach(function(item, index){

                        data_users[item['pk']] = item['username']

                    })

                    var id_typify = this.params['id_typify']
                    var data = this.params['data']

                    var HTML =  `<li class="message right appeared">
                                    <div class="text_wrapper">
                                        <div style="font-size: 1rem; display: inline-flex;">
                                            <div>
                                                <i class="fas fa-edit fa-fw" style="cursor: pointer;" id="re-contact-chat-icon-{4}"></i>
                                            </div>
                                            <div>
                                                <i class="far fa-window-close" style="cursor: pointer;" url="api/v1/tickets/new_contact/{4}/"></i>
                                            </div>
                                        </div>
                                        <H5 id="re-contact-channel-{4}" >{2}</H5>
                                        <div class="float-right">
                                            <p class="text-dark" >{0}</p>
                                        </div>
                                        <div class="text" id="re-contact-text-{4}">{3}</div>
                                        <div>
                                            <form name="send_message_form" data-id="message-form" id="change-re-contact-input-{4}" style="display:none;">
                                                <div class="input-group">
                                                    <input required type="text" class="form-control" name="commentary" value="{3}">
                                                    <input required type="hidden"  value="{5}" name="typify_id">
                                                    <span class="input-group-append">
                                                        <button class="btn btn-primary" type="submit"><i class="fa fa-paper-plane"></i></button>
                                                    </span>
                                                </div>
                                                <div>
                    
                                                    <select required type="text" name="channel" id="re-contact-choice-ID-{4}" required class="form-control advance-select" data-parsley-group="step-1" data-parsley-required="false" data-parsley-type="alphanum" >
                                                        <option></option>
                                                        <option value="LLAMADA">LLAMADA</option>
                                                        <option value="CORREO">CORREO</option>
                                                        <option value="WHATSAPP">WHATSAPP</option>
                                                        <option value="REDES">REDES</option>
                                                        <option value="CHAT">CHAT</option>
                                                    </select>
                    
                                                </div>
                                            </form>
                                        </div>
                                        <div class="float-right">
                                            <p class="text-dark" >{1}</p>
                                        </div>
                                    </div>
                                </li>`

                    var list_message = $("#new-contact-list-{0}".format(id_typify))
                    var message_info = null

                    for (i = 0; i < data.length; i++){

                        message_info = data[i]

                        list_message.append(HTML.format(
                            data_users[message_info['creator']],
                            moment(message_info['created']).tz(moment.tz.guess()).startOf('minute').fromNow(),
                            message_info['channel'],
                            message_info['commentary'],
                            message_info['ID'],
                            id_typify
                        ))

                        form_change(message_info['ID'])
                        icon_change(message_info['ID'])

                        $('#re-contact-choice-ID-{0}'.format(message_info['ID'])).val(message_info['channel']).change()

                    }

                    removeItemPanel()

                }
            
            })
            
        },
    });

}

function list_typify(id){

    $('.theme-panel.theme-panel-lg .theme-collapse-btn.buttons-{0}'.format(id)).click(function(){
        
        var number_typify = $(this).attr('typify')
        var current_type = $(this).attr('type')

        var type = ['history-panel', 'recontact-panel', 'comments-panel']

        for (i = 0; i < type.length; i++){

            if (type[i] == current_type){

                $( '#{0}-{1}'.format( type[i], number_typify ) ).toggleClass('active')

            }else{

                $( '#{0}-{1}'.format( type[i], number_typify ) ).removeClass('active')

            }


        }
    
    })

    // Buttons typify being
    //
    
    // Remove
    $('#typify-close-{0}'.format(id)).on('click', function(){
        
        var id_typify = $(this).attr('id_typify')

        $('#button_{0}'.format(id_typify)).remove()
        $('#typify_view_{0}'.format(id_typify)).remove()

        $('#show_create').trigger('click')

        $('#show_create a').addClass('active')
        $('#show_create a').addClass('show')

    })

    // Save //
    $('#button-submit-typify-change-{0}'.format(id)).on('click',function(event){
        
        var id_typify = $(this).attr('id_typify')
    
        event.preventDefault()
        $('#typify_operator-{0}'.format(id_typify)).val($("#menu_operators").val())
        
        var result = {}
        var disabled = $('#typify-change-form-{0}'.format(id_typify)).find('select:disabled')
        disabled.removeAttr('disabled')
        var data = $('#typify-change-form-{0}'.format(id_typify)).serializeArray()
        disabled.attr('disabled','disabled')
        var services = ''

        data.forEach(function(item, index){

            switch( item['name'] ){

                case 'services[]':

                    services += (services == '') ? item['value'] :',' + item['value']

                    break;

                case 'category':

                    result['category_id'] = item['value']

                    break;

                case 'tecnical_visit':

                    result['tecnical_visit'] = (item['value'] == 'true')? true : false

                    break;

                default:

                    result[item['name']] = item['value']

            }

        })

        result['services'] = services
        result['parent_id'] = (result['parent_id'])? parseInt(result['parent_id'], 10) : 0

        var url = url_base.format('api/v1/tickets/typify/{0}/'.format(id_typify))

        changeItem(url, result,null,null)

        setTimeout(1000, function(){

            $('#history_typify_item-{0}'.format(id_typify)).empty()
            loadHistory('history_typify_item-{0}'.format(id_typify),  url_base.format('api/v1/tickets/typify/{0}/history/'.format(id_typify)), FIELD_NAMES, readable_field)

        })

    })

    // Save and remove
    $('#button-submit-typify-change-close-{0}'.format(id)).on('click', function(event){

        $('#button-submit-typify-change-{0}'.format(id)).trigger('click')
        $('#typify-close-{0}'.format(id)).trigger('click')

    })

    // Save and close
    $('#button-submit-typify-change-and-closeTypify-{0}'.format(id)).on('click', function(event){

        $('#typify_status-change-{0}'.format(id)).val('CERRADO').trigger('change')
        $('#button-submit-typify-change-{0}'.format(id)).trigger('click')

    })
    

    //
    // Buttons typify end

    // technicians being
    //
    listItems($('#typify_technicians-change-{0}'.format(id)), url_base.format('api/v1/matrix/technicians/?fields=id,name'), 'name', 'id')
    //
    // technicians end

    // agent being
    //
    listItems($('#typify_agent-change-{0}'.format(id)), url_base.format('api/v1/user/data/?fields=username,pk'), 'username', 'username')
    //
    // agent end

    var html_service = `<li class="nav-items">
                            <a data-toggle="tab" class="nav-link">
                                <span class="d-sm-block d-none">{0}</span>
                            </a>
                    </li>`

    $.ajax({
        url: url_base.format('api/v1/tickets/typify/?ID={0}'.format(id)),
        params:{
            'id': id,
        },
        success: function (data){

            // *********************************************************************************************************
            // Typify **************************************************************************************************
            
            data = data[0]

            chat(data['ID'], data['commentaries'])
            new_contact(data['ID'])
            
            DATA_CUSTOMER_OLD =   data['first_data_customer']
            $('#typify_ID').val(id)

            // New contact being
            //
            $('#typify_contact_id-add-{0}'.format(id)).val(id)
            $('#new_contact_operator-add-{0}'.format(id)).val($("#menu_operators").val())
            $('#new_contact_channel-add-{0}'.format(id)).select2({
                placeholder: 'Seleccione canal',
            });

            function get_typify_id(id_typify=id){
                return id_typify
            }

            function get_chat_id(id_chat=data['commentaries']){
                return id_chat
            }
            //
            // New contact end

            // Commentary being
            //

            $('#chat_id-commentary-{0}'.format(id)).val(data['commentaries'])

            //
            // Commentary end

            // Discount being
            //
            $('#typify_discount_reason-change-{0}'.format(id)).select2({
                placeholder: 'Seleccione motivo',
            });
            $('#typify_discount_reason-change-{1} option[value="{0}"]'.format( data['discount_reason'], id )).attr('selected', true);
            $('#typify_discount_reason-change-{0}'.format(id)).trigger('change')

            $('#typify_discount_responsable_area-change-{0}'.format(id)).select2({
                placeholder: 'Seleccione area responsable',
            });
            $('#typify_discount_responsable_area-change-{1} option[value="{0}"]'.format( data['discount_responsable_area'], id )).attr('selected', true);
            $('#typify_discount_responsable_area-change-{0}'.format(id)).trigger('change')

            $('#typify_discount_porcentage_change-{0}'.format(id)).val( data['discount_porcentage'] )
            //
            // Discount end

            // Init services ********************
            // **********************************
            
            if ( ['No cliente', 'Prospecto', 'Nuevo cliente'].includes(data['customer_type']) ){

                $('#customer-info-{0}'.format(id)).hide()

            }else{

                var url = url_base.format('api/v1/matrix/services/?fields=id,number&customer__rut={0}'.format( DATA_CUSTOMER_OLD['customer']['rut'] ))
                var services = data['services'].split(",")
                $('#typify_services-change-{0}'.format(id)).prop('disabled', 'disabled');

                $('#typify_services-change-{0}'.format(id)).empty()
                listItems($('#typify_services-change-{0}'.format(id)), url, 'number', 'number')

                services.forEach(function(item, index){

                    $('#typify_services-change-{1} option[value="{0}"]'.format(item, id)).attr('selected', true);

                })

            }

            $('#typify_services-change-{0}'.format(id)).select2({
            placeholder: 'Seleccione servicios',
            });
            // **********************************
            // End services *********************

            // Init category ********************
            // **********************************

            var background = ''
            var color = ''
            
            switch (data['status_SLA']){

                case 'good':

                    background = "blue"
                    color = "white"

                    break;

                case 'alert':

                    background = "yellow"
                    color = "black"

                    break;

                case 'danger':

                    background = "red"
                    color = "white"

                    break;

            }

            $('#typify_SLA-change-{0}'.format(id)).css("background",background);
            $('#typify_SLA-change-{0}'.format(id)).css("color",color);

            $('.typification').select2({
            placeholder: 'Seleccione categoria',
            });

            $('#typify_SLA-change-{0}'.format(id)).val( moment.duration(data['SLA'], 'seconds').format('d [dias] h [horas] m [minutos]') )
            //$('#typify_SLA-change-{0}'.format(id)).val( moment( data['created'] ).tz(moment.tz.guess()).fromNow("D") )
            $('#typify_rut_change-{0}'.format(id)).prop('disabled', 'disabled');

            $('#typify_parent-change-{0}'.format(id)).select2({
                minimumInputLength: 1,
                ajax: {
                    url: url_base.format('api/v1/tickets/typify/get_ID/'),
                    dataType: 'json',
                    data: function (params) {
                        var query = {
                            search: params.term,
                            operator: $('#menu_operators').val(),
                        }
            
                        return query;
                    },
                    processResults: function (data) {
                        
                        return {
                        results: data
                        };
                    }
                },
                language: {
                    inputTooShort: function () {
                    return 'Ingrese números de tipificaciones';
                    }
                }
            });
            
            if (data['parent']){
                
                var id_parent = data['parent'].split('typify')[1].match(/\d+/)[0]
                var Option_default = new Option(id_parent, id_parent, true, true) 
                $('#typify_parent-change-{0}'.format(id)).append(Option_default).trigger('change')

            }

            // **********************************
            // End category *********************

            // Init channel *********************
            // **********************************


            listItems($('#typify_category_type-change-{0}'.format(id)), url_base.format('api/v1/tickets/category/?fields=ID,name&classification=1&operator={0}'.format( $('#menu_operators').val() )), 'name', 'ID')
            
            $('#typify_category_type-change-{0}'.format(id)).select2({placeholder: "Seleccione tipo"});
            $('#typify_category_category-change-{0}'.format(id)).select2({placeholder: "Seleccione cateogía"});
            $('#typify_category_subcategory-change-{0}'.format(id)).select2({placeholder: "Seleccione SubCategoría"});
            $('#typify_category_secondsubcategory-change-{0}'.format(id)).select2({placeholder: "Seleccione segunda SubCategoría"});

            $('#typify_category_type-change-{0}'.format(id)).change(function(event){
        
                var value = $('#typify_category_type-change-{0}'.format(id)).val()
                
                $('#typify_category_category-change-{0}'.format(id)).empty()
                $('#typify_category_subcategory-change-{0}'.format(id)).empty()
                $('#typify_category_secondsubcategory-change-{0}'.format(id)).empty()
            
                $.ajax({
                    url: url_base.format('api/v1/tickets/category/?fields=ID,name&parent={0}'.format(value)),
                    args: {'id':id},
                    success: function (data){
                        
                        $('#typify_category_category-change-{0}'.format(this.args['id'])).select2({
                            data: data.map(function(val, index){ 
                                return {
                                    'id':val['ID'],
                                    'text':val['name']
                                }; 
                            }),
                        })

                        $('#typify_category_category-change-{0}'.format(this.args['id'])).trigger('change')
            
                    },
                })
            
            })
            $('#typify_category_category-change-{0}'.format(id)).change(function(event){

                var value = $('#typify_category_category-change-{0}'.format(id)).val()
            
                $('#typify_category_subcategory-change-{0}'.format(id)).empty()
                $('#typify_category_secondsubcategory-change-{0}'.format(id)).empty()

                $('#typify_category_secondsubcategory-change-{0}'.format(id)).removeAttr('name');
                $('#typify_category_subcategory-change-{0}'.format(id)).attr('name', 'category');
            
                $.ajax({
                    url: url_base.format('api/v1/tickets/category/?fields=ID,name&parent={0}&operator={1}'.format(value, $('#menu_operators').val() )),
                    args:{'id':id},
                    success: function (data){
        
                        $('#typify_category_subcategory-change-{0}'.format(this.args['id'])).select2({
                            data: data.map(function(val, index){ 
                                return {
                                    'id':val['ID'],
                                    'text':val['name']
                                }; 
                            }),
                        })

                        $('#typify_category_subcategory-change-{0}'.format(this.args['id'])).trigger('change')
                    },
                })
            })
            $('#typify_category_subcategory-change-{0}'.format(id)).change(function(event){

                var value = $('#typify_category_subcategory-change-{0}'.format(id)).val()
                
            
                $('#typify_category_secondsubcategory-change-{0}'.format(id)).empty()
            
                $.ajax({
                    url: url_base.format('api/v1/tickets/category/?fields=ID,name&parent={0}&operator={1}'.format(value, $('#menu_operators').val() )),
                    args:{'id':id},
                    success: function (data){

                        let id = this.args['id']

                        $('#typify_category_secondsubcategory-change-{0}'.format(this.args['id'])).select2({
                            data: data.map(function(val, index){ 
                                return {
                                    'id':val['ID'],
                                    'text':val['name']
                                }; 
                            }),
                        })

                        if ( data.length ){

                            $('#typify_category_subcategory-change-{0}'.format(id)).removeAttr('name');
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).attr('name', 'category');
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).parent().show()
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).prop('required', true);
                            $('#ypify_category_subcategory-change-{0}'.format(id)).prop('required', false);
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).trigger('change')


                        }else{

                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).removeAttr('name');
                            $('#typify_category_subcategory-change-{0}'.format(id)).attr('name', 'category');
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).parent().hide()
                            $('#typify_category_secondsubcategory-change-{0}'.format(id)).prop('required', false);
                            $('#ypify_category_subcategory-change-{0}'.format(id)).prop('required', true);
                            
                        }
        
                    },
                })
            })

            function category_show(url){

                if (url){

                    function search_typify(url){

                        $.ajax({
                            url: url,
                            args:{'id':id},
                            async: false,
                            success: function (data){
    
                                id_typify = this.args['id']
    
                                switch(data['classification']){
    
                                    case 1:

                                        ROAD.push( data['ID'] )
                                        
                                        break;
    
                                    case 2:
    
                                        var category_url = ( data['parent'] )? url_base.format( 'api/{0}'.format(data['parent'].split('api/')[1]) ) : data['parent']
                                        search_typify( category_url)
                                        ROAD.push( data['ID'] )
    
                                        break;
    
                                    case 3:
    
                                        var category_url = ( data['parent'] )? url_base.format( 'api/{0}'.format(data['parent'].split('api/')[1]) ) : data['parent']
                                        search_typify( category_url)
                                        ROAD.push( data['ID'] )
    
                                        break;
    
                                    case 4:
    
                                        var category_url = ( data['parent'] )? url_base.format( 'api/{0}'.format(data['parent'].split('api/')[1]) ) : data['parent']
                                        search_typify( category_url)
                                        ROAD.push( data['ID'] )
    
                                    default:
                                        break;
    
                                }
    
                            },
                        })


                    }

                    var ROAD = []
                    search_typify(url)
                    
                    
                    ROAD.forEach( function(item, index){
                        
                        switch (index){

                            case 0:

                                $('#typify_category_type-change-{0}'.format(id)).val( item ).trigger('change')

                                break;

                            case 1:

                                setTimeout(function(){ 
                                    $('#typify_category_category-change-{0}'.format( id )).val( item ).trigger('change') 
                                }, 1000);

                                break;

                            case 2:

                                setTimeout(function(){ 
                                    $('#typify_category_subcategory-change-{0}'.format( id )).val( item ).trigger('change') 
                                }, 1500);

                                break;

                            case 3:

                                setTimeout(function(){ 
                                    $('#typify_category_secondsubcategory-change-{0}'.format( id )).val( item ).trigger('change')
                                }, 2000);

                                break;

                        }


                    } )


                }

            }

            var category_url = ( data['category'] )? url_base.format( 'api/{0}'.format(data['category'].split('api/')[1]) ) : null
            
            category_show( category_url )

            if (data['agent']){
                $.ajax({
                    url: url_base.format('api/v1/user/data/{0}/'.format(data['agent'].split('data')[1].match(/\d+/)[0])),
                    args:{'id':id},
                    success: function (data){

                        var id_typify = this.args['id']
                        
                        $('#typify_agent-change-{1} option[value="{0}"]'.format( data['username'], id_typify )).attr('selected', true);
                        
                        $('#typify_agent-change-{0}'.format(id_typify)).select2({});
                        $('#typify_agent-change-{0}'.format(id_typify)).trigger('change');
                        

                    },
                })
            }

            $('#typify_channel-change-{1} option[value="{0}"]'.format( data['channel'], id )).attr('selected', true);
            $('#typify_channel-change-{0}'.format(id)).select2({});

            $('#typify_technicians-change-{1} option[value="{0}"]'.format( data['technician'], id )).attr('selected', true);
            $('#typify_technicians-change-{0}'.format(id)).select2({});
            // **********************************
            // End channel **********************

            // Init In u Out ********************
            // **********************************

            $('#typify_in_out-change-{1} option[value="{0}"]'.format( data['type'], id )).attr('selected', true);
            $('#typify_in_out-change-{0}'.format(id)).select2({});
            // **********************************
            // End In u Out *********************

            // Init status **********************
            // **********************************

            $('#typify_status-change-{1} option[value="{0}"]'.format( data['status'], id )).attr('selected', true);
            $('#typify_status-change-{0}'.format(id)).select2({});
            // **********************************
            // End status ***********************

            // Init urgency ********************
            // **********************************

            $('#typify_urgency-change-{1} option[value="{0}"]'.format( data['urgency'], id )).attr('selected', true);
            $('#typify_urgency-change-{0}'.format(id)).select2({});
            // **********************************
            // End urgency **********************

            // Init city ************************
            // **********************************

            $('#typify_city-change-{1} option[value="{0}"]'.format( data['city'], id )).attr('selected', true);
            $('#typify_city-change-{0}'.format(id)).select2({});
            // **********************************
            // End city *************************

            // Init visit ***********************
            // **********************************

            $('#typify_management-change-{1} option[value="{0}"]'.format( data['management'], id )).attr('selected', true);
            $('#typify_management-change-{0}'.format(id)).change(function(event){
                
                if ($(this).val() == 'NINGÚNA'){
            
                    $('#typify_technicians-change-{0}'.format(id)).next(".select2-container").hide()
            
                }else{
            
                    $('#typify_technicians-change-{0}'.format(id)).next(".select2-container").show()
            
                }
            
            })
            $('#typify_management-change-{0}'.format(id)).select2({});
            $('#typify_management-change-{0}'.format(id)).trigger('change')

            // **********************************
            // End visit ************************

            // Init liveagent *******************
            // **********************************

            $('#typify_liveagent-change-{0}'.format(id)).val( data['liveagent'] );
            // **********************************
            // End liveagent ********************


            // Init customer ********************
            // **********************************

            $('#typify_customer-{0}'.format(id)).val( data['customer_type'] );

            

            // **********************************
            // End customer *********************


            // *********************************************************************************************************
            // Customer ************************************************************************************************

            var tr_customer = `<tr>
                                <td class="bg-silver-lighter">{0}</td>
                                <td>
                                    <a class="editable editable-click">{1}</a>
                                </td>
                                <td>
                                    <span class="text-black-lighter">{2}</span>
                                </td>
                            </tr>`
            
            if ( !['No cliente', 'Prospecto', 'Nuevo cliente'].includes(data['customer_type']) ){

                var tr_table = `<tr>
                                <td class="bg-silver-lighter">{0}</td>
                                <td class="{1}">
                                    <a class="editable editable-click">{2}</a>
                                </td>
                                <td class="{1}">
                                    <a class="editable editable-click">{3}</a>
                                </td>
                            </tr>`
                
                $.ajax({
                    url: url_customer = url_base.format('api/v1/matrix/customers/?rut={0}&fields=rut,name,email,phone,kind,default_due_day,composite_address,commune,notes'.format(DATA_CUSTOMER_OLD['customer']['rut'])),
                    args:{'tr_customer': tr_table, 'id':id, 'OLD_CUSTOMER':DATA_CUSTOMER_OLD['customer']},
                    success: function (data){

                        function tr_difference(tr, name, old, current){

                            return tr.format(name, (old != current)? 'danger' : '', old, current)

                        }

                        var OLD_CUSTOMER = this.args['OLD_CUSTOMER']
                        var tr_customer = this.args['tr_customer']
                        var id_typify = this.args['id']
                        var table_customer = $('#table-customer-personal-{0}'.format(id_typify)).find('tbody')
                        data = data[0]
                        
                        $('#typify_rut_change-{0}'.format(id_typify)).val(data['rut']).prop('disabled', 'disabled')
                        $('#typify_name_change-{0}'.format(id_typify)).val(data['name']).prop('disabled', 'disabled')
                        $('#typify_email_change-{0}'.format(id_typify)).val(data['email']).prop('disabled', 'disabled')
                        $('#typify_phone_change-{0}'.format(id_typify)).val(data['phone']).prop('disabled', 'disabled')

                        tr = tr_difference(tr_customer, 'Rut', OLD_CUSTOMER['rut'], data['rut']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Nombre', OLD_CUSTOMER['name'], data['name']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Tipo', OLD_CUSTOMER['kind'], data['kind']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Correo', OLD_CUSTOMER['email'], data['email']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Telefono', OLD_CUSTOMER['phone'], data['phone']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Día de pago', OLD_CUSTOMER['default_due_day'] , data['default_due_day']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Dirección', OLD_CUSTOMER['composite_address'], data['composite_address']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Comuna', OLD_CUSTOMER['commune'], data['commune']);
                        table_customer.append(tr);
                        tr = tr_difference(tr_customer, 'Nota', OLD_CUSTOMER['notes'], data['notes']);
                        table_customer.append(tr);

                        $.ajax({
                            url: url_base.format('api/v1/matrix/commercial-activities/?customer__rut={0}'.format(data['rut'])),
                            args:{
                                'tr_customer': tr_customer,
                                'OLD_CUSTOMER_COMMERCIAL_ACTIVITY':OLD_CUSTOMER['commercial_activity'],
                                'id':id,
                            },
                            success: function (data){

                                    var OLD_CUSTOMER_COMMERCIAL_ACTIVITY = this.args['OLD_CUSTOMER_COMMERCIAL_ACTIVITY']
                                    var activity_new = (data.length)? data[0]['name'] : ''
                                    var activity_old = ( OLD_CUSTOMER_COMMERCIAL_ACTIVITY )? OLD_CUSTOMER_COMMERCIAL_ACTIVITY : ''
                                    var id_typify = this.args['id']
                                    var tr = tr_difference(this.args['tr_customer'], 'Actividad comercial', activity_old, activity_new);

                                    $('#table-customer-personal-{0}'.format(id_typify)).find('tbody').append( tr );

                            },
                        })

                        $.ajax({
                            url: url_base.format('api/v1/matrix/balances/?customer__rut={0}&fields=balance'.format(data['rut'])),
                            args:{
                                'tr_customer': tr_customer,
                                'OLD_CUSTOMER_BALANCE':OLD_CUSTOMER['balance'],
                                'id':id,
                            },
                            success: function (data){
                                    
                                    var id_typify = this.args['id']
                                    var tr = tr_difference(this.args['tr_customer'], 'Balance', this.args['OLD_CUSTOMER_BALANCE'], data[0]['balance'] );

                                    $('#table-customer-personal-{0}'.format(id_typify)).find('tbody').append( tr );

                            },
                        })

                    },
                });

                $.ajax({
                    url: url_customer = url_base.format('api/v1/matrix/payorruts/?customer__rut={0}&fields=rut,name'.format(DATA_CUSTOMER_OLD['customer']['rut'])),
                    args:{
                        'tr_customer': tr_table,
                        'id':id,
                        'OLD_CUSTOMER_PAYORRUTS':DATA_CUSTOMER_OLD['customer']['payorruts']},
                    success: function (data){

                        var old_payorruts = this.args['OLD_CUSTOMER_PAYORRUTS']
                        var id_typify = this.args['id']
                        
                        data.forEach(function(item){

                            var tr_table = `<tr>
                                            <td class="{0}">
                                                <a class="editable editable-click">{1}</a>
                                            </td>
                                            <td class="{0}">
                                                <a class="editable editable-click">{2}</a>
                                            </td>
                                        </tr>`
                            var tr = ''

                            if( old_payorruts.some(e => e.rut == item['rut']) ) {

                                tr = tr_table.format('', item['rut'], item['name'])

                            }else{

                                tr = tr_table.format('danger', item['rut'], item['name'])

                            }

                            $('#table-customer-payorruts-{0} > tbody:last-child'.format(id_typify)).append(tr);

                        })

                        $('#table-customer-payorruts-{0}'.format(id_typify)).DataTable({
                            autoWidth: true,
                            keys: true,
                            "language": {
                                "sLengthMenu":     "Mostrar _MENU_ Pagadores",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                "sInfo":           "Mostrando Pagadores de _START_ a _END_ de un total de _TOTAL_ Pagadores",
                                "sInfoEmpty":      "Mostrando Pagadores del 0 al 0 de un total de 0 Pagadores",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ Pagadores)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "sProcessing": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Último",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                        });

                    },
                });

                $.ajax({
                    url: url_customer = url_base.format('api/v1/matrix/payments/?customer__rut={0}&fields=paid_on,deposited_on,amount,manual,kind,cleared,comment'.format(DATA_CUSTOMER_OLD['customer']['rut'])),
                    args:{
                        'tr_customer': tr_table,
                        'created': Date.parse(data['created']),
                        'id':id,
                        'OLD_CUSTOMER_PAYMENTS':DATA_CUSTOMER_OLD['customer']['payments']},
                    success: function (data){

                        var old_payments = this.args['OLD_CUSTOMER_PAYMENTS']
                        var created = this.args['created'] 
                        var id_typify = this.args['id']
                        
                        data.forEach(function(item){

                            var tr_table =  `<tr>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{1}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{2}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{3}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{4}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{5}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{6}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{7}</a>
                                                </td>
                                            </tr>`

                            var tr = ''
                            
                            if( Date.parse(item['paid_on'])  > created) {

                                var tr = tr_table.format(
                                    '',
                                    moment(item['paid_on']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    moment(item['deposited_on']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    item['amount'],
                                    (item['manual'])? 'Si': 'No',
                                    item['kind'],
                                    (item['cleared'])? 'Si': 'No',
                                    item['comment'],
                                )
                            }else{

                                var tr = tr_table.format(
                                    'danger',
                                    moment(item['paid_on']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    moment(item['deposited_on']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    item['amount'],
                                    (item['manual'])? 'Si': 'No',
                                    item['kind'],
                                    (item['cleared'])? 'Si': 'No',
                                    item['comment'],
                                )

                            }

                            $('#table-customer-payments-{0} > tbody:last-child'.format(id_typify)).append(tr);

                        })

                        $('#table-customer-payments-{0}'.format(id_typify)).DataTable({
                            autoWidth: true,
                            keys: true,
                            "language": {
                                "sLengthMenu":     "Mostrar _MENU_ Pagos",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                "sInfo":           "Mostrando Pagos de _START_ a _END_ de un total de _TOTAL_ Pagos",
                                "sInfoEmpty":      "Mostrando Pagos del 0 al 0 de un total de 0 Pagos",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ Pagos)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "sProcessing": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Último",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                        });

                    },
                });

                $.ajax({
                    url: url_customer = url_base.format('api/v1/matrix/invoices/?customer__rut={0}&fields=kind,folio,due_date,total,paid,status,comment'.format(DATA_CUSTOMER_OLD['customer']['rut'])),
                    args:{
                        'tr_customer': tr_table,
                        'created': Date.parse(data['created']),
                        'id':id,
                        'OLD_CUSTOMER_INVOICES':DATA_CUSTOMER_OLD['customer']['invoices'],
                    },
                    success: function (data){

                        var old_invoices = this.args['OLD_CUSTOMER_INVOICES']
                        var created = this.args.created 
                        var id_typify = this.args['id']

                        data.forEach(function(item){

                            var tr_table =  `<tr>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{1}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{2}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{3}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{4}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{5}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{6}</a>
                                                </td>
                                                <td class="{0}">
                                                    <a class="editable editable-click">{7}</a>
                                                </td>
                                            </tr>`

                            var tr = ''


                            if( Date.parse(item['due_date'])  > created ) {
                                
                                var tr = tr_table.format(
                                    '',
                                    item['kind'],
                                    item['folio'],
                                    moment(item['due_date']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    item['total'],
                                    (item['paid'])? 'Si' : 'No',
                                    item['status'],
                                    item['comment'],
                                )

                            }else{

                                var tr = tr_table.format(
                                    'danger',
                                    item['kind'],
                                    item['folio'],
                                    moment(item['due_date']).tz(moment.tz.guess()).subtract(10, 'days').calendar(),
                                    item['total'],
                                    (item['paid'])? 'Si' : 'No',
                                    item['status'],
                                    item['comment'],
                                )

                            }

                            $('#table-customer-invoices-{0} > tbody:last-child'.format(id_typify)).append(tr);

                        })

                        $('#table-customer-invoices-{0}'.format(id_typify)).DataTable({
                            autoWidth: true,
                            keys: true,
                            "language": {
                                "sLengthMenu":     "Mostrar _MENU_ Facturas",
                                "sZeroRecords":    "No se encontraron resultados",
                                "sEmptyTable":     "Ningún dato disponible en esta tabla",
                                "sInfo":           "Mostrando Facturas de _START_ a _END_ de un total de _TOTAL_ Facturas",
                                "sInfoEmpty":      "Mostrando Facturas del 0 al 0 de un total de 0 Facturas",
                                "sInfoFiltered":   "(filtrado de un total de _MAX_ Facturas)",
                                "sInfoPostFix":    "",
                                "sSearch":         "Buscar:",
                                "sUrl":            "",
                                "sInfoThousands":  ",",
                                "sLoadingRecords": "Cargando...",
                                "sProcessing": "Cargando...",
                                "oPaginate": {
                                    "sFirst":    "Primero",
                                    "sLast":     "Último",
                                    "sNext":     "Siguiente",
                                    "sPrevious": "Anterior"
                                },
                                "oAria": {
                                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                                }
                            }
                        });

                    },
                });

                if (data['services']){

                    data['services'].split(',').forEach(function(item,index,[],id_typify=id){
                        
                        
                        var html_button = `<li class="nav-items">
                                            <a href="#nav-pills-tab-{0}-{3}" data-toggle="tab" class="nav-link">
                                            <span class="d-sm-none">{1}</span>
                                            <span class="d-sm-block d-none">{2}</span>
                                            </a>
                                        </li>`

                        var html_table = `<div class="tab-content tab-pane fade" id="nav-pills-tab-{0}-{1}">
                                            <ul class="nav nav-pills">
                                                <li class="nav-items">
                                                    <a href="#service-{0}-{1}" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Servicio</span>
                                                    <span class="d-sm-block d-none">Servicio</span>
                                                    </a>
                                                </li>
                                                <li class="nav-items">
                                                    <a href="#node-{0}-{1}" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Nodo</span>
                                                    <span class="d-sm-block d-none">Nodo</span>
                                                    </a>
                                                </li>
                                                <li class="nav-items">
                                                    <a href="#plan-{0}-{1}" data-toggle="tab" class="nav-link">
                                                    <span class="d-sm-none">Plan</span>
                                                    <span class="d-sm-block d-none">Plan</span>
                                                    </a>
                                                </li>
                                            </ul>
                                            <div class="tab-content table-responsive tab-pane fade" id="service-{0}-{1}"  style="margin-right: 5px; margin-left: 5px;">
                                                <table  class="table table-condensed table-bordered" id="table-service-{0}-{1}">
                                                    <thead>
                                                        <tr>
                                                            <th>Campo</th>
                                                            <th>Antes</th>
                                                            <th>Ahora</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-responsive tab-pane fade" id="node-{0}-{1}" style="margin-right: 5px; margin-left: 5px; ">
                                                <table  class="table table-condensed table-bordered" id="table-node-{0}-{1}">
                                                    <thead>
                                                        <tr>
                                                            <th>Campo</th>
                                                            <th>Antes</th>
                                                            <th>Ahora</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div class="table-responsive tab-pane fade" id="plan-{0}-{1}" style="margin-right: 5px; margin-left: 5px;">
                                                <table  class="table table-condensed table-bordered" id="table-plan-{0}-{1}">
                                                    <thead>
                                                        <tr>
                                                            <th>Campo</th>
                                                            <th>Antes</th>
                                                            <th>Ahora</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>`

                        var tr_table = `<tr>
                                        <td class="bg-silver-lighter">{0}</td>
                                        <td>
                                            <a class="editable editable-click">{1}</a>
                                        </td>
                                    </tr>`

                        $.ajax({
                            url: url_customer = url_base.format('api/v1/matrix/services/?number={0}'.format(item)),
                            args: {
                                'service':DATA_CUSTOMER_OLD['services'][item],
                                'button': html_button,
                                'tr_table': tr_table,
                                'tables': html_table,
                                'id':id_typify,
                            },
                            success: function (data){

                                var data = data[0]
                                var service = this.args['service']
                                var number_service = data['number']
                                var tr = ''
                                var id_typify = this.args['id']

                                $('#nav-pills-tab-services-{0}'.format(id_typify)).append(this.args['tables'].format(number_service, id_typify))
                                $('#ul-services-{0}'.format(id_typify)).append(this.args['button'].format(data['number'], data['number'], number_service, id_typify))
                                var table_service = $('#table-service-{0}-{1}'.format(number_service, id_typify)).find('tbody')

                                /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Tipo de documento', service['document_type'], data['document_type'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Dia de pago', service['due_day'], data['due_day'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Estado', service['get_status_display'], data['get_status_display'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('SSID', service['ssid'], data['ssid'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Tecnologia', service['get_technology_kind_display'], data['get_technology_kind_display'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Direccion', service['composite_address'], data['composite_address'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Comuna', service['commune'], data['commune'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('¿Auto corte?', (service["allow_auto_cut"]) ? 'Si' : 'No', (data["allow_auto_cut"]) ? 'Si' : 'No')
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('¿Visto conectado?', (service["seen_connected"]) ? 'Si' : 'No', (data["seen_connected"]) ? 'Si' : 'No')
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('MAC de ONU', service['mac_onu'], data['mac_onu'])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Estado de red', service['network_status'][1], data['network_status'][1])
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Fecha de activacion', time_to_human(service['activated_on']), time_to_human(data['activated_on']))
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Fecha de instalacion', time_to_human(service['installed_on']), time_to_human(data['installed_on']))
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Fecha de desintalacion', time_to_human(service['uninstalled_on']), time_to_human(data['uninstalled_on']))
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Fecha de expiracion', time_to_human(service["expired_on"]), time_to_human(service["expired_on"]))
                                table_service.append( tr );
                                 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Nota facturacion', service['billing_notes'], data['billing_notes'])
                                table_service.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                tr = tr_equal('Nota interna', service['internal_notes'], data['internal_notes'])
                                table_service.append( tr );

                                $.ajax({
                                    url: url_base.format('api/v1/matrix/network-equipments/?contract__number={0}&primary=True'.format(data["number"])),
                                    args: {
                                        'number': data['number'],
                                        'service':service,
                                        'id':id_typify,
                                    },
                                    success: function (data){

                                        var ip = ''

                                        if (data.length){

                                            ip = data[0]["ip"]

                                        }else{

                                            ip = "No indica"

                                        }
                                         /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('IP', service['ip'], ip)
                                        var table_service = $('#table-service-{0}-{1}'.format(this.args['number'], this.args['id'])).find('tbody')
                                        table_service.before( tr );

                                    },
                                })

                                $.ajax({
                                    url: url_base.format('api/v1/matrix/nodes/{0}/'.format(service["node"]['id'])),
                                    args: {
                                        'node':service["node"],
                                        'number': data['number'],
                                        'id':id_typify,
                                    },
                                    success: function (data){

                                        var node = this.args['node']
                                        var table_node = $('#table-node-{0}-{1}'.format(this.args['number'], this.args['id'])).find('tbody')
                                         /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Codigo', node['code'], data['code'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Estado', node['status'], data['status'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Direccion', node['address'], data['address'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Comuna', node['commune'], data['commune'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Ubicacion', node['location'], data['location'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('¿Es edificio?', ( node["is_building"] ) ? 'Si' : 'No', ( data["is_building"] ) ? 'Si' : 'No')
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('¿Es fibra?', ( node["is_optical_fiber"] ) ? 'Si' : 'No', ( data["is_optical_fiber"] ) ? 'Si' : 'No')
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('GPON', ( node["gpon"] ) ? 'Si' : 'No', ( data["gpon"] ) ? 'Si' : 'No')
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('GEPON', ( node["gepon"] ) ? 'Si' : 'No', ( data["gepon"] ) ? 'Si' : 'No')
                                        table_node.append( tr );

                                    },
                                })
                                
                                $.ajax({
                                    url: url_base.format('api/v1/matrix/plans/{0}/'.format(service["plan"]['id'])),
                                    args: {
                                        'plan':service["plan"],
                                        'number': data['number'],
                                        'id':id_typify,
                                    },
                                    success: function (data){

                                        var plan = this.args['plan']
                                        var table_node = $('#table-plan-{0}-{1}'.format(this.args['number'], this.args['id'])).find('tbody')
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Nombre', plan['name'], data['name'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Precio', plan['price'], data['price'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('Categoria', plan['get_category_display'], data['get_category_display'])
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('UF', (plan["uf"]) ? 'Si' : 'No', (data["uf"]) ? 'Si' : 'No')
                                        table_node.append( tr );
 /** @function {@param, @param, @param} tr_equal declarado @file {@this} */
                                        tr = tr_equal('¿Activo?', (plan["active"]) ? 'Si' : 'No', (data["active"]) ? 'Si' : 'No')
                                        table_node.append( tr );

                                    },
                                })

                            },
                        });


                    })

                }

            }else{

                $('#customer-services-{0}'.format(id)).hide()

            }
            
            loadHistory('history_typify_item-{0}'.format(this.params['id']),  url_base.format('api/v1/tickets/typify/{0}/history/'.format(id)), FIELD_NAMES, readable_field)

        },
    });

}
