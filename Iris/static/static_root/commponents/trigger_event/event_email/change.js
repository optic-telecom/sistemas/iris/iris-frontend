/**
 * @author {Cristian Cantero}
 *  @file {}
 */

$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/communications/template/?fields=ID,name&operator={0}'.format($('#menu_operators').val())),
	async: false,
	complete: function(data) {
		data = data.responseJSON;

		if (!data.length) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Error',
				text: 'No hay plantillas para esta empresa',
				sticky: false,
				time: ''
			});
		} else {
			var element = $('#event_email_template');

			element.append(new Option());
			data.forEach(function(value, index) {
				element.append(new Option(value['name'], value['ID']));
			});

			$('#event_email_template').select2({
				placeholder: 'Seleccione una plantilla'
			});
			$('#event_email_template')
				.nextAll()
				.attr('style', 'width: 100% !important');
		}
	}
});

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form-trigger-email-event-change').submit(function(event) {
	event.preventDefault();
	/** @var {array} data */
	var data = $(this).serializeArray();
	id = data.shift();
	/** @var {string} url  ruta a la cual seran enviado los datos. */
	url = url_base.format('api/v1/communications/event_email/{0}/'.format(id['value']));
	/**
	 * Llama a la @function {@param, @namespace, @param, @function} changeItem del @file {index.js}
	 */
	changeItem(url, data, '#table-trigger-event');
});
