$.ajax({
	type: 'GET',
	dataType: 'json',
	url: url_base.format('api/v1/communications/template/?fields=ID,name&operator={0}'.format($('#menu_operators').val())),
	complete: function(data) {
		data = data.responseJSON;

		if (!data.length) {
			/** @plugin {notfication} Se le notifiaca el resultado al operador */
			$.gritter.add({
				title: 'Error',
				text: 'No hay plantillas para esta empresa',
				sticky: false,
				time: ''
			});
		} else {
			var element = $('#event_email_template');

			element.append(new Option());
			data.forEach(function(value, index) {
				element.append(new Option(value['name'], value['ID']));
			});

			$('#event_email_template').select2({
				placeholder: 'Seleccione una plantilla'
			});
			$('#event_email_template')
				.nextAll()
				.attr('style', 'width: 100% !important');
		}
	}
});

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form-trigger-email-event-create').submit(function(event) {
	event.preventDefault();

	var data = $(this).serializeArray();
	var url = url_base.format('api/v1/communications/event_email/');

	data.push({
		name: 'operator',
		value: $('#menu_operators').val()
	});

	createItem(url, data, 'Evento correo creado', 'Error al crear evento');
});
