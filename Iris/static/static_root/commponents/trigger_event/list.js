/**
 * @author {Cristian Cantero}
 *  @file {}
 */

/**
 * @function trigger_event_change
 * @param {*} id
 */
function trigger_event_change(id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/view_trigger_event_change');
	var url_item = url_base.format('api/v1/communications/trigger_event/{0}/'.format(id));
	/**
	 * @function post_ajax funcion interna
	 * @namespace {object} data
	 */
	function post_ajax(data) {
		/**
		 * Configuracion de los parametros de a mostrar en la vista
		 * Se cargan los datos en los @JselectorElement {id_element}  paramatrizado con el valor obtenido de data
		 */
		$('#triger_event_id').val(data['ID']);
		$('#triger_event_name').val(data['name']);
		$('#triger_event_description').val(data['description']);
	}
	/**
	 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}

function trigger_event_email(id) {
	$.ajax({
		type: 'GET',
		dataType: 'json',
		async: false,
		url: url_base.format('api/v1/communications/event_email/?trigger_event__ID={0}'.format(id)),
		args: { id: id },
		complete: function(data) {
			if (data.responseJSON.length == 0) {
				$('#modal-html').empty();
				/**
				 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
				 */
				callView('view_trigger_event_email_create', (content = $('#modal-html')), (data = []));
				$('#modals').modal('show');
				$('#event_email_id_trigger').val(this.args['id']);
			} else {
				/**
				 * @var {string} url_html se guarda la direccion URL  formateada
				 * @var {string} url_item se guarda la direccion URL  formateada
				 */
				var url_html = url_base_templates.format('Iris/view_trigger_event_email_change');
				var url_item = url_base.format('api/v1/communications/event_email/{0}/'.format(data.responseJSON[0]['ID']));
				/**
				 * @function post_ajax funcion interna
				 * @namespace {object} data
				 */
				function post_ajax(data) {
					/**
					 * Configuracion de los parametros de a mostrar en la vista
					 * Se cargan los datos en los @JselectorElement {id_element}  paramatrizado con el valor obtenido de data
					 */
					$('#id_event_email').val(data['ID']);
					$('#event_email_test').val(data['email_test']);
					$('#event_email_response').val(data['email_response']);
					$('#event_email_copy').val(data['email_copy']);
					$('#event_email_template').val(data['template'].split('template/')[1].replace('/', ''));
					$('#event_email_template').trigger('change');
				}
				/**
				 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
				 */
				changeModal(url_html, url_item, post_ajax, data.responseJSON[0]);
			}
		}
	});
}

//Funcion de eliminacion de disparadores de eventos
function trigger_event_delete(id) {
	//Se estructura la direccion, la cual se enviara el id a eliinar
	let url = url_base.format('api/v1/communications/trigger_event/{0}/'.format(id));

	//Se le pregunta al usuario si desea eliminar la seleccionada.
	swal(
		{
			title: '¿Estas seguro?',
			text: 'Se eliminara el disparador de Evento {0}!'.format(id),
			type: 'warning',
			showCancelButton: true,
			confirmButtonClass: 'btn-danger',
			confirmButtonText: 'Si, eliminarlo!',
			cancelButtonText: 'No, Cancelar',
			closeOnConfirm: true,
			closeOnCancel: false
		},
		function(isConfirm) {
			if (isConfirm) {
				/**
				 * Llama a la @function {@param, @plugin , @function } removeItem del @file {index.js}
				 */
				removeItem(url, '#table-trigger-event');
			} else {
				swal('Cancelado', 'El disparador conttinua en la lista', 'success');
			}
		}
	);
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-trigger-event').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Disparador',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Disparador de _START_ a _END_ de un total de _TOTAL_ Disparador',
		sInfoEmpty: 'Mostrando Disparador del 0 al 0 de un total de 0 Disparador',
		sInfoFiltered: '(filtrado de un total de _MAX_ Disparadores)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/communications/trigger_event/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
		}
	},
	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 2]
		},
		{
			render: function(data, type, row) {
				html = "<button onclick='trigger_event_change({0})' class='btn btn-sm btn-purple' data-toggle='modal'>Modificar</button>";
				return html.format(row[0]);
			},
			targets: 3
		},
		{
			render: function(data, type, row) {
				html = "<button onclick='trigger_event_email({0})' class='btn btn-sm btn-info' data-toggle='modal'>Correo</button>";
				return html.format(row[0]);
			},
			targets: 4
		},
		{
			render: function(data, type, row) {
				html = "<button onclick='trigger_event_delete({0})' class='btn btn-sm btn-danger' >Eliminar</button>";
				return html.format(row[0]);
			},
			targets: 5
		}
	],
	columns: [{ name: 'ID' }, { name: 'name' }, { name: 'description' }, { name: 'ID' }, { name: 'ID' }, { name: 'ID' }]
});

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
	$('#table-trigger-event')
		.DataTable()
		.ajax.reload();
});
