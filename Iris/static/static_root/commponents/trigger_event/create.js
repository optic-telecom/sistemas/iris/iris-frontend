/**
 * @author {Cristian Cantero}
 *  @file {}
 */
/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form-trigger-event').submit(function(event) {
	event.preventDefault();

	var data = $(this).serializeArray();
	var url = url_base.format('api/v1/communications/trigger_event/');

	data.push({
		name: 'operator',
		value: $('#menu_operators').val()
	});
	/**
	 * Llama a la @function {@param, @namespace, @param, @param, @function } createItem del @file {index.js}
	 */
	createItem(url, data, 'Disparador creado', 'Error al crear disparador', () =>
		$('#table-trigger-event')
			.DataTable()
			.ajax.reload()
	);

	//Se recarga la tabla de los disparadores de eventos.

	//Area de creacion
	$('#list_show > li > a[id="cv"]').removeClass('active show');
	$('#create_view').removeClass('active show');
	//Area de lista de disparadores de eventos.
	$('#list_show > li > a[id="lv"]').addClass('active show');
	$('#list_view').addClass('active show');
	//Se limpia el formulario para las próximas creaciones de disparadores de evento.
	document.getElementById('form-trigger-event').reset();
});
