/**
 * @author {Cristian Cantero}
 *  @file {}
 */
/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form-trigger-event-change').submit(function(e) {
	e.preventDefault();
	/** @var {array} form */
	form = $(this).serializeArray();
	id = form.shift();
	/** @var {string} url  ruta a la cual seran enviado los datos. */
	url = url_base.format('api/v1/communications/trigger_event/{0}/'.format(id['value']));
	/**
	 * Llama a la @function {@param, @namespace, @param, @function} changeItem del @file {index.js}
	 */
	changeItem(url, form, '#table-trigger-event');
});
