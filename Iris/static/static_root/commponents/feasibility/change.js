/**
 * @author {Cristian Cantero}
 * @file { feasibility_list: list.js  }
 */

/**
 * @function load_data_feasibility que cargara la data de la factibilidades.
 * @param {number} ID
 */
function load_data_feasibility(ID) {
	/**
	 * @function load_data funcion interna que carga la data.
	 * @param {*} ID
	 */
	function load_data(ID) {
		/**
		 * @var {string} url se guarda la direccion URL  formateada
		 */
		var url = url_base.format('api/v1/customers/feasibility/{0}/'.format(ID));

		/**
		 * @JselectorElement {id_element} @plugin {conf} select2  se le indica al operado que debe elegir una opcion
		 */
		$('#feasibility_change_plan').select2({
			placeholder: 'Seleccione un plan'
		});

		/** @JselectorElement {id_element} @module {attr} estilo el cual mantiene al tamaño de la pantalla  segun su resolucion */
		$('#feasibility_change_plan')
			.nextAll()
			.attr('style', 'width: 100% !important');

		/**
		 * @API [ajax]: donde se obtienes los datos de factibilidad que esta asociada al registro
		 */
		$.ajax({
			type: 'GET',
			url: url,
			success: function(data) {
				/**
				 * @JselectorElement {id_element} : @module {push[val]} data
				 */
				$('#feasibility_change_region').val(data['region']);
				$('#feasibility_change_commune').val(data['commune']);
				$('#feasibility_change_street').val(data['street']);
				$('#feasibility_change_departament').val(data['department']);
				$('#feasibility_change_phone').val(data['phone']);
				$('#feasibility_change_name').val(data['name']);
				$('#feasibility_change_email').val(data['email']);
				$('#feasibility_change_ID').val(data['ID']);
				$('#feasibility_change_number_typify').val(data['number_typify']);
				/** Se le adiciona el disparador de evento de cambio. */
				$('#feasibility_change_plan')
					.val(data['number_plan'])
					.trigger('change');
			},
			error: function() {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Resultado:',
					text: 'Error al cargar factibilidad',
					sticky: false,
					time: ''
				});
			}
		});
	}

	/** @var {@JselectorElement} SELECT */
	SELECT = $('#feasibility_change_plan');
	/** @var {@JselectorElement} URL */
	URL = url_base.format('api/v1/matrix/plans/');

	/**
	 * Llama a la @function {@param, @param, @param, @param, @param} listItems del @file {index.js}
	 */
	listItems(SELECT, URL, 'name', 'id', () => load_data(ID));
}

/**
 * @JselectorElement {id_element} @module {event} botton para aplicar los cambios
 */
$('#feasibility_change_button').click(function() {
	/** @var {array} data donde se almacenaran los datos serializado. */
	var data = $('#form-feasibility-change').serializeArray();
	/** @var {number} ID */
	var ID = $('#feasibility_change_ID').val();

	/**
	 * @var {string} url se guarda la direccion URL  formateada
	 */
	var url = url_base.format('api/v1/customers/feasibility/{0}/'.format(ID));

	/**
	 * @function callBack Funcion interna para
	 * @JselectorElement {id_element} @plugin {event} DataTable
	 * @JselectorElement {id_element} @module {event} ocultara el modal
	 */
	function callBack() {
		/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion  */
		$('#table-feasibility')
			.DataTable()
			.ajax.reload();
		$('#modals').hide();
	}

	/** Se agregan al objeto el name y el value del operador */
	data.push({ name: 'operator', value: $('#menu_operators').val() });

	/** Se agregan al objeto el name del numero de tipficacion y el value del operador */
	data.push({ name: 'number_typify', value: $('#feasibility_change_number_typify').val() });

	/**
	 * Llama a la @function {@param, @param, @param, @param} changeItem del @file {index.js}
	 */
	changeItem(url, data, null, callBack);
});
