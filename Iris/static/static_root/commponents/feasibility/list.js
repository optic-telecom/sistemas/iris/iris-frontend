/**
 * @author {Cristian Cantero}
 * @file { feasibility_lis }
 */

/**
 * @function change_feasibility
 * @param {number} ID
 * Llama a la @function {@param, @JselectorElement } callView del @file {index.js}
 * Llama a la @function {@param, @JselectorElement } load_data_feasibility del @file {index.js}
 */
function change_feasibility(ID) {
	/** @JselectorElement {id_element} @module {event} Se limpia la modal antes de montar lo que se va a mostrar */
	$('#modal-html').empty();
	callView('feasibility_change', $('#modal-html'));
	load_data_feasibility(ID);
	$('#modals').show();
}

/**
 * @function delete_feasibility esta funcio elimina (desactiva) la factibilidad para vista
 * @param {number} ID
 */
function delete_feasibility(ID) {
	/**
	 * Llama a la @function {@param, @JselectorElement } removeItem del @file {index.js}
	 */
	removeItem(url_base.format('api/v1/customers/feasibility/{0}/'.format(ID)), $('#table-feasibility'));
}

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-feasibility').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Factibilidad',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Factibilidad de _START_ a _END_ de un total de _TOTAL_ Factibilidad',
		sInfoEmpty: 'Mostrando Factibilidad del 0 al 0 de un total de 0 Factibilidad',
		sInfoFiltered: '(filtrado de un total de _MAX_ Factibilidads)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/customers/feasibility/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) },
		data: function(d) {
			d.operator = $('#menu_operators').val();
			d.endtime = $('#feasibility_datetimeend_query').val();
			d.inittime = $('#feasibility_datetimeinit_query').val();
		}
	},
	columnDefs: [
		{
			orderable: true,
			searchable: true,
			targets: [0, 3, 4, 5, 6, 7, 8, 9, 10]
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='change_feasibility({0})' class='btn btn-sm btn-success' data-toggle='modal' style='color:white;'>Cambiar</a>";
				debugger;
				return html.format(row[row.length - 1]);
			},
			targets: 11
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='delete_feasibility({0})' class='btn btn-sm btn-primary'' data-toggle='modal' style='color:white;'>Borrar</a>";
				return html.format(row[row.length - 1]);
			},
			targets: 12
		},
		{
			render: function(data, type, row) {
				/** Se coloco la diferencia en horas que ha ha sido creado el registro.  */
				return 'Hace ' + moment().diff(data, 'hours') + ' horas';
				//moment( data ).tz(moment.tz.guess()).subtract(10, 'days').calendar()
			},
			targets: 0
		}
	],
	columns: [
		{ name: 'created' },
		{ name: 'creator' },
		{ name: 'region' },
		{ name: 'commune' },
		{ name: 'street' },
		{ name: 'department' },
		{ name: 'phone' },
		{ name: 'name' },
		{ name: 'email' },
		{ name: 'number_plan' },
		{ name: 'number_typify' }
	]
});

/**
 * function current_values
 * @returns {object} con los valores definidos en el vistas
 */
function current_values() {
	return {
		draw: '1',

		'columns[0][data]': '0',
		'columns[0][name]': 'endtime',
		'columns[0][searchable]': 'true',
		'columns[0][orderable]': 'true',
		'columns[0][search][value]': '',
		'columns[0][search][regex]': 'false',

		'columns[1][data]': '1',
		'columns[1][name]': 'inittime',
		'columns[1][searchable]': 'true',
		'columns[1][orderable]': 'true',
		'columns[1][search][value]': '',
		'columns[1][search][regex]': 'false',

		'order[0][column]': '0',
		'order[0][dir]': 'desc',
		start: '0',
		length: '-1',
		'search[value]': $('#download_data_feasibility > label > input').val(),
		'search[regex]': 'false',
		endtime: $('#feasibility_datetimeinit_query').val(),
		inittime: $('#feasibility_datetimeend_query').val(),
		_: '1568749802687'
	};
}

/**
 * @JselectorElement {id_element} @module {event} botton para descarga la informacion.
 */
$('#download_data_feasibility').click(function() {
	/**
	 * Se declara una @var {XHLHR}  req
	 */
	var req = new XMLHttpRequest();
	/** se declara una @var urlToSend con formato de la ruta a la cual se solicitara la información */
	var urlToSend = url_base.format('api/v1/customers/feasibility/download_data/') + '?{0}'.format(jQuery.param(current_values()));

	/** Se configura la solicitud del request.  */
	req.open('GET', urlToSend, true);
	req.setRequestHeader('Authorization', 'JWT {0}'.format(Cookies.get('token')));
	req.responseType = 'blob';
	req.onload = function(event) {
		var blob = req.response;
		var link = document.createElement('a');
		link.href = window.URL.createObjectURL(blob);
		link.download = 'Factibilidades.xlsx';
		link.click();
		window.URL.revokeObjectURL(link.href);
	};

	/** se tealiza el envio del request con toda la configuracion cargada.  */
	req.send();
});

/**
 * @JselectorElement {id_element} @plugin {config} datetimepicker se formatea el fecha y tiempo [inicio]
 */
$('#feasibility_datetimeinit_query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/**
 * @JselectorElement {id_element} @plugin {config} datetimepicker se formatea el fecha y tiempo [fin]
 */
$('#feasibility_datetimeend_query').datetimepicker({
	format: 'DD/MM/YYYY HH:mm:ss'
});

/**
 * @JselectorElement {id_element} @on {event} dp.change se realizan los cambios en
 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */
$('#feasibility_datetimeinit_query').on('dp.change', function(e) {
	$('#table-feasibility')
		.DataTable()
		.ajax.reload();
});

/**
 * @JselectorElement {id_element} @on {event} dp.change se realizan los cambios en
 * @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion
 */
$('#feasibility_datetimeend_query').on('dp.change', function(e) {
	$('#table-feasibility')
		.DataTable()
		.ajax.reload();
});
