/**
 * @author {Cristian Cantero}
 *  @file { }
 */

/**
 * @var {element} @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-contact').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Contactos',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Empresa de _START_ a _END_ de un total de _TOTAL_ Contactos',
		sInfoEmpty: 'Mostrando Empresa del 0 al 0 de un total de 0 Contactos',
		sInfoFiltered: '(filtrado de un total de _MAX_ Contactos)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/contact/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<i  onclick=show_contact(`{0}`) class='btn btn-sm btn-success fa fa-eye aria-hidden=`true`'></i>";
				return html.format(data);
			},
			targets: 0
		},
		{
			render: function(data, type, row) {
				html = "<i onclick=add_phone_contact(`{0}`) class='btn btn-sm btn-success fa fa-phone aria-hidden=`true`'></i>";
				return html.format(data);
			},
			targets: 2
		},
		{
			render: function(data, type, row) {
				html = "<i onclick=add_email_contact(`{0}`) class='btn btn-sm btn-success fa fa-envelope aria-hidden=`true`'></i>";
				return html.format(data);
			},
			targets: 3
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 1]
		},
		{
			orderable: false,
			searchable: false,
			targets: [2, 3]
		}
	],
	columns: [{ name: 'ID' }, { name: 'rut' }, { name: 'telephones' }, { name: 'emails address' }]
});

//TODO funciones declaradas sin ningun tipo de accion vacias
function add_phone_contact(ID) {}
//TODO funciones declaradas sin ningun tipo de accion vacias
function add_email_contact(ID) {}
//TODO funciones declaradas sin ningun tipo de accion vacias
function show_contact(ID) {}
