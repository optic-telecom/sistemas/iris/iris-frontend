/**
 * @author {Cristian Cantero}
 * @file { view_triggers_time }
 */

var COUNT_FILTER = 0;

var TYPES = [];
var FILTERS = [];
var FILTERS_TRANSITION = {};
var FILTERS_TYPES = {};
var FILTER_TYPE_TRANSITION = {};

/**
 * @function FormWizardView - vista donde se desplegaran el formulario en el wizard
 * @param {String} ID
 */
function FormWizardView(ID) {
	/**
	 * @function filters Se cargaran todos los filtros
	 * @param {*} NUMBER
	 */
	function filters(NUMBER) {
		/**@JselectorElement {id_element} */
		$('#filter_type_{0}'.format(NUMBER));

		for (const [key, value] of Object.entries(FILTERS_TRANSITION)) {
			$('#filter_type_{0}'.format(NUMBER)).append(new Option(key, value));
		}

		/**@JselectorElement {id_element} @plugin {conf} select2 @module {style} se mantiene el tamaño de forma constante  */
		$('#filter_type_{0}'.format(NUMBER))
			.select2({
				placeholder: 'Seleccione filtro'
			})
			.nextAll()
			.attr('style', 'width: 100% !important');

		/**@JselectorElement {class_element} @module {event} change  */
		$('.filter_type').change(function() {
			/**
			 * @var {element} parent
			 * @var {number} number
			 * @var {string} list_comparation data estraida del  @dict FILTERS_TYPES
			 */
			var parent = $(this).parent();
			var number = parent.parent().attr('number_id');
			var list_comparation = FILTERS_TYPES[TYPES[$('#filter_type_{0} option:selected'.format(number)).text()]];

			/** @var {string} node_comparation    */
			var node_comparation = `<div class="filter_trigger">
                                        <select type="text" data-parsley-group="step-2" class="form-control category parsley-error filter_comparation" data-parsley-required="true" id="filter_comparation_{0}">
                                        </select>
									</div>`;
			/** @var {string} node_value_text_time   */
			var node_value_text_time = `<div class="filter_trigger">
                                                <input type="text" data-parsley-group="step-2" class="form-control category parsley-error" data-parsley-required="true"  id="filter_value_{0}">
											</div>`;
			/** @var {string} node_value_int    */
			var node_value_int = `<div class="filter_trigger">
                                        <input type="number" data-parsley-group="step-2" class="form-control category parsley-error" data-parsley-required="true"  id="filter_value_{0}">
                                    </div>`;
			/** @var {string} node_value_bool   */
			var node_value_bool = `<div class="filter_trigger">
                                        <select type="text" data-parsley-group="step-2" class="form-control category parsley-error" data-parsley-required="true"  id="filter_value_{0}">
                                            <option value="True">Cierto</option>
                                            <option value="False">Falso</option>
                                        </select>
									</div>`;
			/** @var {string} node_value_list  */
			var node_value_list = `<div class="filter_trigger">
                                        <select type="text" data-parsley-group="step-2" class="form-control category parsley-error" data-parsley-required="true"  id="filter_value_{0}">
                                        </select>
									</div>`;
			/** @namespace {object} dict_list_values lista de diccionarios */
			var dict_list_values = {
				status: {
					data: {
						1: 'ACTIVE',
						2: 'PENDING',
						3: 'INACTIVE',
						4: 'NOT_INSTALLED',
						5: 'FREE',
						6: 'INSTALL_REJECTED',
						7: 'OFF_PLAN_SALE',
						8: 'DELINQUENT',
						9: 'TO_RESCHEDULE',
						10: 'HOLDER_CHANGE',
						11: 'DISCARDED',
						12: 'ON_HOLD',
						13: 'LOST'
					},
					message: 'Seleccione estado de cliente'
				},
				technology_kind: {
					data: {
						1: 'NETWORK_CABLE',
						2: 'GPON',
						3: 'GEPON',
						4: 'ANTENNA'
					},
					message: 'Seleccione tipo de tecnología'
				},
				document_type: {
					data: {
						1: 'BOLETA',
						2: 'FACTURA',
						3: 'NOTA_DE_VENTA'
					},
					message: 'Seleccione tipo de documento'
				},
				customer__kind: {
					data: {
						1: 'HOME',
						2: 'COMPANY'
					},
					message: 'Seleccione tipo de cliente'
				},
				service__category: {
					data: {
						1: 'INTERNET_HOGAR',
						2: 'INTERNET_EMPRESAS',
						3: 'TELEFONIA_EMPRESAS',
						4: 'RED_EMPRESAS',
						5: 'TELEVISION_HOGAR'
					},
					message: 'Seleccione categoria del servicio'
				}
			};
			/**
			 * @API [ajax]:de consulta
			 */
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: url_base.format('api/v1/matrix/plans/?fields=id,name'),
				async: false,
				complete: function(data) {
					/** @constant {data} data_plans */
					const data_plans = data.responseJSON;

					/**
					 * @namespace {object} dict_result
					 * @namespace {object} dict_data
					 * @namespace {object} instance
					 */
					var dict_result = {};
					var dict_data = {};
					var instance = {};

					data.responseJSON;

					for (i = 0; i < data_plans.length; i++) {
						instance = data_plans[i];
						dict_data[instance['id']] = instance['name'];
					}

					dict_result['data'] = dict_data;
					dict_result['message'] = 'Seleccione plan del servicio';
					dict_list_values['plan__id'] = dict_result;
				}
			});

			/** Se remueve el parent */
			parent.nextAll().remove();

			/** se agrega depues el nodo de comparacion  */
			parent.after(node_comparation.format(number));

			/**@JselectorElement {id_element} @plugin {conf} select2 @module {style} se mantiene el tamaño de forma constante  */
			$('#filter_comparation_{0}'.format(number))
				.select2({
					placeholder: 'Seleccione criterio'
				})
				.nextAll()
				.attr('style', 'width: 100% !important');

			for (var i = 0; i < list_comparation.length; i++) {
				$('#filter_comparation_{0}'.format(number)).append(new Option(list_comparation[i], FILTER_TYPE_TRANSITION[list_comparation[i]]));
			}

			/** Segun el el caso se define el filtro de comparacio  */
			switch (TYPES[$('#filter_type_{0} option:selected'.format(number)).text()]) {
				case 'int':
					$('#filter_comparation_{0}'.format(number))
						.parent()
						.after(node_value_int.format(number));

					break;

				case 'str':
					$('#filter_comparation_{0}'.format(number))
						.parent()
						.after(node_value_text_time.format(number));

					break;

				case 'bool':
					$('#filter_comparation_{0}'.format(number))
						.parent()
						.after(node_value_bool.format(number));
					/**@JselectorElement {id_element} @plugin {conf} select2 @module {style} se mantiene el tamaño de forma constante  */
					$('#filter_value_{0}'.format(number))
						.select2({
							placeholder: 'Seleccione valor'
						})
						.nextAll()
						.attr('style', 'width: 100% !important');

					break;

				case 'date':
					$('#filter_comparation_{0}'.format(number))
						.parent()
						.after(node_value_text_time.format(number));
					/** Se agrega formato al @JselectorElemento {id_element} @plugin {datetimepicker}. */
					$('#filter_value_{0}'.format(number)).datetimepicker({
						format: 'DD/MM/YYYY'
					});

					break;

				case 'list':
					var type_filter_value = dict_list_values[$('#filter_type_{0} option:selected'.format(number)).val()];

					if (type_filter_value) {
						var data_list = type_filter_value['data'];
						var message_list = type_filter_value['message'];

						$('#filter_comparation_{0}'.format(number))
							.parent()
							.after(node_value_list.format(number));

						for (const [key, value] of Object.entries(data_list)) {
							$('#filter_value_{0}'.format(number)).append(new Option(value, key));
						}
						/**@JselectorElement {id_element} @plugin {conf} select2 @module {style} se mantiene el tamaño de forma constante  */
						$('#filter_value_{0}'.format(number))
							.select2({
								placeholder: message_list
							})
							.nextAll()
							.attr('style', 'width: 100% !important');
					} else {
					}

					break;
			}

			$('#filter_comparation_{0}'.format(number)).change(function() {
				if (TYPES[$('#filter_type_{0} option:selected'.format(number)).text()] == 'date') {
					$(this)
						.parent()
						.nextAll()
						.remove();

					if (['year', 'month', 'day', 'week_day', 'week', 'quarter'].includes($(this).val())) {
						$('#filter_comparation_{0}'.format(number))
							.parent()
							.after(node_value_int.format(number));
					} else {
						$('#filter_comparation_{0}'.format(number))
							.parent()
							.after(node_value_text_time.format(number));
						/** Se agrega formato al @JselectorElemento {id_element} @plugin {datetimepicker}. */
						$('#filter_value_{0}'.format(number)).datetimepicker({
							format: 'DD/MM/YYYY'
						});
					}
				}
			});
		});

		$('#filter_type_{0}'.format(NUMBER)).trigger('change');
		$('#filter_comparation_{0}'.format(NUMBER)).trigger('change');
	}

	function handleBootstrapWizardsValidation(ID) {
		$('#{0}'.format(ID))
			.find('.wizard_triggers_time')
			.smartWizard({
				selected: 0,
				keyNavigation: false,
				theme: 'default',
				transitionEffect: 'slide',
				transitionSpeed: 600,
				useURLhash: !1,
				showStepURLhash: !1,
				toolbarSettings: {
					toolbarPosition: 'bottom'
				},
				lang: {
					next: 'Siguiente',
					previous: 'Anterior'
				}
			}),
			$('#{0}'.format(ID))
				.find('.wizard_triggers_time')
				.on('leaveStep', function(e, anchorObject, stepNumber, stepDirection) {
					var result = false;

					if (stepDirection == 'backward') {
						result = true;
					} else {
						result = $(this)
							.parent()
							.parsley()
							.validate('step-' + (stepNumber + 1));
						exist_filters = $(this).find('.load_filters').children().length? true: false;
						include_file = $(this).find('.include')[0].files[0] != undefined ? true : false;

						result = stepNumber == 1 ? (exist_filters || include_file ? result : false) : result;

						if (!(exist_filters || include_file) && stepNumber == 1) {
							/** @plugin {notfication} Se le notifiaca el resultado al operador */
							$.gritter.add({
								title: 'Muy Bien!',
								text: 'Se debe agregar al menos un filtro o agregar un archivo de incluir servicios',
								sticky: false,
								time: ''
							});
						}
					}

					if (result && stepNumber == 1 && stepDirection != 'backward') {
						$(this)
							.parent()
							.trigger('submit');
					}

					return result;
				}),
			$('#{0}'.format(ID))
				.find('.wizard_triggers_time')
				.keypress(function(t) {
					13 == t.which && $(this).smartWizard('next');
				});
		$('#{0}'.format(ID))
			.find('.wizard_triggers_time')
			.on('showStep', function(t, a, i, r) {
				return true;
			});
	}

	handleBootstrapWizardsValidation(ID);

	/**@JselectorElement {id_element} @plugin {conf} select2 @se le indica al operador que debe colocar una opcion */
	$('#{0}'.format(ID))
		.find('.type_trigger_time')
		.select2({
			placeholder: 'Tipo'
		});
	/**@JselectorElement {id_element} @plugin {conf} select2 @module {style} se mantiene el tamaño de forma constante  */
	$('#{0}'.format(ID))
		.find('.type_trigger_time')
		.nextAll()
		.attr('style', 'width: 100% !important');
	/**@JselectorElement {id_element} @plugin {conf} select2 se le indica al operador que debe colocar una opcion */
	$('#{0}'.format(ID))
		.find('.template_list_trigger_time')
		.select2({
			placeholder: 'Plantilla'
		});
	/**@JselectorElement {id_element} @module {style} se mantiene el tamaño de forma constante  */
	$('#{0}'.format(ID))
		.find('.template_list_trigger_time')
		.nextAll()
		.attr('style', 'width: 100% !important');
	/**@JselectorElement {id_element} @plugin {conf} select2 se le indica al operador que debe colocar una opcion */
	$('#{0}'.format(ID))
		.find('.type_repeat_trigger_time')
		.select2({
			placeholder: 'Repeticiones'
		});
	/**@JselectorElement {id_element} @module {style} se mantiene el tamaño de forma constante  */
	$('#{0}'.format(ID))
		.find('.type_repeat_trigger_time')
		.nextAll()
		.attr('style', 'width: 100% !important');
	/**@JselectorElement {id_element} @plugin {conf} select2 se le indica al operador que debe colocar una opcion */
	$('#{0}'.format(ID))
		.find('.category')
		.select2({
			placeholder: ''
		});
	/**@JselectorElement {id_element} @module {style} se mantiene el tamaño de forma constante  */
	$('#{0}'.format(ID))
		.find('.category')
		.nextAll()
		.attr('style', 'width: 100% !important');
	/** Se agrega formato al @JselectorElemento {id_element} @plugin {datetimepicker}. */
	$('#{0}'.format(ID))
		.find('.first_time_trigger_time')
		.datetimepicker({
			format: 'DD/MM/YYYY HH:mm:ss'
		});

	/**@JselectorElement {id_element} @plugin {conf} select2 se le indica al operador que debe colocar una opcion */
	$('#{0}'.format(ID))
		.find('.type_logic_trigger_time')
		.select2({
			placeholder: 'Tipo'
		});

	$('#{0}'.format(ID))
		.find('.type_trigger_time')
		.change(function() {
			var type_repeat_trigger_time = $(this)
				.parent()
				.next()
				.find('.type_repeat_trigger_time');
			var number_repeat_trigger_time = $(this)
				.parent()
				.next()
				.next()
				.find('.number_repeat_trigger_time');

			if ($(this).val() == 'O') {
				type_repeat_trigger_time.prop('disabled', true);
				number_repeat_trigger_time.prop('disabled', true);

				type_repeat_trigger_time.val('0').trigger('change');
				number_repeat_trigger_time.val('0').trigger('change');
			} else {
				type_repeat_trigger_time.prop('disabled', false);
				number_repeat_trigger_time.prop('disabled', false);
			}
		});

	$('#{0}'.format(ID))
		.find('.type_repeat_trigger_time')
		.change(function() {
			var number_repeat_trigger_time = $(this)
				.parent()
				.next()
				.find('.number_repeat_trigger_time');

			number_repeat_trigger_time.prop('disabled', true);

			switch ($(this).val()) {
				case '0':
					number_repeat_trigger_time.val('0').trigger('change');

					break;

				case '-1':
					number_repeat_trigger_time.val('-1').trigger('change');

					break;

				default:
					number_repeat_trigger_time.prop('disabled', false);
			}
		});
	/**
	 * @API [ajax]: Se consultan los valores segun este el operador iniciado.
	 */
	$.ajax({
		type: 'GET',
		dataType: 'json',
		async: false,
		url: url_base.format('api/v1/communications/template/?fields=ID,name,template,subject&operator={0}'.format($('#menu_operators').val())),
		args: {
			ID: ID
		},
		complete: function(data) {
			data = data.responseJSON;

			if (!data.length) {
				/** @plugin {notfication} Se le notifiaca el resultado al operador */
				$.gritter.add({
					title: 'Error',
					text: 'No hay plantillas para esta empresa',
					sticky: false,
					time: ''
				});
			} else {
				var element = $('#{0}'.format(ID)).find('.template_list_trigger_time');

				element.append(new Option());
				data.forEach(function(value, index) {
					element.append(new Option(value['name'], value['ID']));
				});
				/**
				 * @JselectorElement {id_element} @plugin {conf} select2  se le indica al operado que debe elegir una opcion
				 */
				$('#{0}'.format(ID))
					.find('.template_list_trigger_time')
					.select2({
						placeholder: 'Seleccione una plantilla'
					});
				/**@JselectorElement {id_element} @module {style} se mantiene el tamaño de forma constante  */
				$('#{0}'.format(ID))
					.find('.template_list_trigger_time')
					.nextAll()
					.attr('style', 'width: 100% !important');
			}
		}
	});

	$('#{0}'.format(ID))
		.find('.template_list_trigger_time')
		.change(function() {
			/**
			 * @API [ajax]: Se envian los datos de @constant  result
			 */
			$.ajax({
				type: 'GET',
				dataType: 'json',
				url: url_base.format('api/v1/communications/template/{0}/'.format($(this).val())),
				args: {
					element: $(this)
				},
				complete: function(data) {
					data = data.responseJSON;
					this.args['element']
						.parent()
						.next()
						.find('.subject_trigger_time')
						.val(data['subject']);
					this.args['element']
						.parent()
						.parent()
						.next()
						.find('.template_trigger_time')
						.val(data['template_html']);
				}
			});
		});

	$('#{0}'.format(ID))
		.find('.type_logic_trigger_time')
		.change(function() {
			var messages = $(this)
				.parent()
				.parent()
				.find('.font-weight-bold');

			if ($(this).val() == 'C') {
				messages[0].style.display = 'block';
				messages[1].style.display = 'none';
			} else {
				messages[0].style.display = 'none';
				messages[1].style.display = 'block';
			}
		});

	$('#{0}'.format(ID))
		.find('.add_filter')
		.click(function() {
			var HTML_FILTER = `
            <div class="filter_group" number_id="{0}">
                <div class="filter_group_close">
                    <i class="far fa-times-circle fa-2x text-muted" ></i>
                </div>

                <div class="filter_trigger">
                    <select type="text" name="filter_{0}[]" data-parsley-group="step-2" class="form-control category parsley-error filter_type" data-parsley-required="true" id="filter_type_{0}" >
                    </select>
                </div>

            </div>
            `;

			$(this)
				.parent()
				.parent()
				.find('.load_filters')[0]
				.insertAdjacentHTML('beforeend', HTML_FILTER.format(COUNT_FILTER++));

			$('.filter_group_close .far.fa-times-circle.fa-2x.text-muted').click(function() {
				$(this)
					.parent()
					.parent()
					.remove();
			});

			filters(COUNT_FILTER - 1);
		});

	function previus(ID) {
		var previous = 0;

		$('#{0}'.format(ID))
			.find('.number_repeat_trigger_time')
			.on('focus', function() {
				// Store the current value on focus and on change
				previous = this.value;
			})
			.change(function(event) {
				switch (
					$(this)
						.parent()
						.parent()
						.prev()
						.find('.type_repeat_trigger_time')
						.val()
				) {
					case '-1':
						$(this).val(-1);

						break;

					case '0':
						$(this).val(0);

						break;

					default:
						$(this).val($(this).val() > 0 ? $(this).val() : previous);

						break;
				}

				event.preventDefault();
			});
	}

	previus(ID);
}
/**
 * @API [ajax]: consultan los filtros de comunicaciones masivas.
 */
$.ajax({
	url: url_base.format('api/v1/communications/massive/filters/'),
	type: 'GET',
	success: function(data) {
		TYPES = data['filters'];
		FILTERS = Object.keys(data['filters']);
		FILTERS_TYPES = data['type_filters'];
		FILTERS_TRANSITION = data['filters_translation'];
		FILTER_TYPE_TRANSITION = data['type_filters_tranlation'];
	},
	error: function(a) {
		/** @plugin {notfication} Se le notifiaca el resultado al operador */
		$.gritter.add({
			title: 'Error',
			text: 'No se puedo cargar el recurso',
			sticky: false,
			time: ''
		});
	}
});
