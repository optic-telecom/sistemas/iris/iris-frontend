/**
 * @author {Cristian Cantero}
 * @file { operator_view }
 */

/**
 * @function handleBootstrapWizardsValidation
 */
var handleBootstrapWizardsValidation = function() {
		'use strict';
		/**
		 * @JselectorElement {id_element} @plugin {conf} smartWizard
		 */
		$('#wizard').smartWizard({
			selected: 0,
			theme: 'default',
			transitionEffect: 'slide',
			transitionSpeed: 600,
			useURLhash: !1,
			showStepURLhash: !1,
			toolbarSettings: {
				toolbarPosition: 'bottom'
			},
			lang: {
				next: 'Siguiente',
				previous: 'Anterior'
			}
		}),
			/**
			 * @JselectorElement {id_element} @module {leaveStep} smartWizard cambios de los pasos
			 */
			$('#wizard').on('leaveStep', function(t, a, i, r) {
				if (r == 'backward') {
					return i == 2 ? false : true;
				}
				/** @var {boolean} result - usardo como vandera */
				var result = false;
				/**
				 * segun el paso que este se realiza diferentes aciones.
				 * @return result
				 */
				switch (i) {
					case 0:
						if (inputValue($('#operator_name'), 'Ingrese nombre de un operador')) {
							var url = url_base.format('api/v1/tickets/operator/?name={0}'.format(name));
							/**
							 * Llama a la @function {@param, @param, @param } existItem del @file {index.js}
							 */
							var existOperator = existItem(url, 'El operador ya existe', 'Ocurrio un error');
							if (!existOperator) {
								result = true;
								var name = $('#operator_name').val();
								$('#item-value-1').html(name);
							}
						}
						break;

					case 1:
						if (inputValue($('#operator_template'), 'Ingrese una plantilla de correos')) {
							result = true;
							var data = $(this)
								.parent()
								.serializeArray();
							var url = url_base.format('api/v1/tickets/operator/');
							/**
							 * Llama a la @function {@param, @namespace, @param, @param, @function } createItem del @file {index.js}
							 */
							createItem(url, data, 'Operador creado', 'Error al crear operador');
							/** se recargara la pagina despues de aplicar todos los cambios */
							location.reload();
						}
						break;
					default:
					//TODO vacio!
				}
				return result;
			}),
			/**
			 * @JselectorElement {id_element} @module {keypress} smartWizard cuando se da enter o se presiona el botton next
			 */
			$('#wizard').keypress(function(t) {
				13 == t.which && $('#wizard').smartWizard('next');
			});

		/**
		 * @JselectorElement {id_element} @module {showStep} smartWizard muestra los pasos
		 */
		$('#wizard').on('showStep', function(t, a, i, r) {
			if (r == 'backward') {
				var step_dict = {};
				step_dict[1] = 'Ingresar nombre de operador';
				step_dict[2] = 'Ingresar plantilla';
				/**
				 * Llama a la @function {@param, @namespace } removeDone del @file {index.js}
				 */
				removeDone(i, step_dict);
			}
		});
	},
	/**
	 * @function FormWizardValidation
	 *  Se usa para deplegar la funcion interna.
	 */
	FormWizardValidation = (function() {
		'use strict';
		return {
			init: function() {
				handleBootstrapWizardsValidation();
			}
		};
	})();

/**  se despliega la funcion que contien el wizard  */
FormWizardValidation.init();
