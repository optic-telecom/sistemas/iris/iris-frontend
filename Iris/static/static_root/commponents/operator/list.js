/**
 * @author {Cristian Cantero}
 * @file { operator_view }
 */

function operator_change(id) {
	/**
	 * @var {string} url_html se guarda la direccion URL  formateada
	 * @var {string} url_item se guarda la direccion URL  formateada
	 */
	var url_html = url_base_templates.format('Iris/operator_change');
	var url_item = url_base.format('api/v1/tickets/operator/{0}/'.format(id));

	/**
	 * @function post_ajax funcion interna
	 * @namespace {object} data
	 */
	function post_ajax(data) {
		/**
		 * Configuracion de los parametros de a mostrar en la vista en los @JselectorElement {id_element}
		 */
		$('#change_operator_id').val(data['ID']);
		$('#change_operator_name').val(data['name']);
		$('#change_operator_email').val(data['email']);
		$('#change_operator_template_html').val(data['template_html']);
		$('#change_operator_template_text').val(data['template_text']);
		$('[data-id="color-palette-1"]').val(data['color_body']);
		$('[data-id="color-palette-2"]').val(data['color_lateral']);
		$('[data-id="color-palette-3"]').val(data['color_higher']);
		/**
		 * @JselectorElement {id_element} @plugin {conf} select2 formateado con el id_contact
		 */
		$('#operator_email_list').select2({
			/** @API [ajax=get] Se consulta si tiene tipificaciones abiertas. */
			ajax: {
				url: url_base.format('api/v1/tickets/operator_email/'),
				data: { operator: data['ID'] },
				processResults: function(data) {
					data.forEach(function(item, index, array) {
						array[index] = {
							id: item['ID'],
							text: item['email']
						};
					});
					return {
						results: data
					};
				}
			}
		});

		/** @JselectorElement {id_element} @module {attr}estilo para mantener el tamaño de manera uniforme */
		$('#operator_email_list')
			.nextAll()
			.attr('style', 'width: 400px !important');
	}

	/**
	 * Llama a la @function {@param, @param, @function, @namespace } changeModal del @file {index.js}
	 */
	changeModal(url_html, url_item, post_ajax);
}

/**
 * @var {table} dt_table[@todo] @JselectorElement @plugin {conf} dataTable
 */
var dt_table = $('#table-operator').dataTable({
	aaSorting: [[0, 'desc']],
	language: {
		sLengthMenu: 'Mostrar _MENU_ Operador',
		sZeroRecords: 'No se encontraron resultados',
		sEmptyTable: 'Ningún dato disponible en esta tabla',
		sInfo: 'Mostrando Operador de _START_ a _END_ de un total de _TOTAL_ Operador',
		sInfoEmpty: 'Mostrando Operador del 0 al 0 de un total de 0 Operador',
		sInfoFiltered: '(filtrado de un total de _MAX_ Operador)',
		sInfoPostFix: '',
		sSearch: 'Buscar:',
		sUrl: '',
		sInfoThousands: ',',
		sLoadingRecords: 'Cargando...',
		sProcessing: 'Cargando...',
		oPaginate: {
			sFirst: 'Primero',
			sLast: 'Último',
			sNext: 'Siguiente',
			sPrevious: 'Anterior'
		},
		oAria: {
			sSortAscending: ': Activar para ordenar la columna de manera ascendente',
			sSortDescending: ': Activar para ordenar la columna de manera descendente'
		}
	},
	processing: true,
	serverSide: true,
	ajax: {
		url: url_base.format('api/v1/tickets/operator/datatables/'),
		type: 'GET',
		headers: { Authorization: 'JWT {0}'.format(Cookies.get('token')) }
	},
	columnDefs: [
		{
			render: function(data, type, row) {
				html = "<a onclick='operator_change({0})' class='btn btn-sm btn-success' data-toggle='modal'>Modificar</a>";
				return html.format(data);
			},
			targets: 2
		},
		{
			render: function(data, type, row) {
				html = "<a onclick='removeItem(`{0}`, `{1}`)' class='btn btn-sm btn-success' data-toggle='modal'>Borrar</a>";
				url = url_base.format('api/v1/tickets/operator/{0}/'.format(data));
				dataTable = '#table-operator';
				return html.format(url, dataTable);
			},
			targets: 3
		},
		{
			orderable: true,
			searchable: true,
			targets: [0, 1]
		},
		{
			orderable: false,
			searchable: false,
			targets: [2, 3]
		}
	],
	columns: [{ name: 'ID' }, { name: 'name' }, { name: 'Modificar' }, { name: 'Borrar' }]
});

$(document).on('hide.bs.modal', '#modals', function() {
	/** @JselectorElement {id_element} @plugin {event} DataTable para recargar la informacion */
	$('#table-operator')
		.DataTable()
		.ajax.reload();
});
