/**
 * @author {Cristian Cantero}
 * @include { operator_view }
 */

/**
 * @JselectorElement {id_element} para enviar los datos ya establecido en el vista.
 */
$('#form_change_operator').submit(function(e) {
	e.preventDefault();
	/** @var {array} form */
	var form = $(this).serializeArray();
	var id = form.shift();
	/** @var {string} url  ruta a la cual seran enviado los datos. */
	var url = url_base.format('api/v1/tickets/operator/{0}/'.format(id['value']));
	/**
	 * Llama a la @function {@param, @namespace, @param, @function} changeItem del @file {index.js}
	 */
	changeItem(url, form, $('#table-operator'));

	/** se verifica el cambio del operado sea diferente del menu del operador */
	if ($('#change_operator_id').val() != $('#menu_operators').val()) {
		$('#body').css('background', COLOR_BODY);
		$('#sidebar').css('background', COLOR_SIDEBAR);
		$('#header').css('background', COLOR_HEADER);
	}
});

/** @JselectorElement {event} @plugin {event} cambio del background */
$('#color-palette-1')
	.colorPalette()
	.on('selectColor', function(e) {
		var element = $('[data-id="color-palette-1"]');
		element.val(e.color);
		$('#body').css('background', element.val());
	});
/** @JselectorElement {event} @plugin {event} cambio del background */
$('#color-palette-2')
	.colorPalette()
	.on('selectColor', function(e) {
		var element = $('[data-id="color-palette-2"]');
		element.val(e.color);
		$('#sidebar').css('background', element.val());
	});
/** @JselectorElement {event} @plugin {event} cambio del background */
$('#color-palette-3')
	.colorPalette()
	.on('selectColor', function(e) {
		var element = $('[data-id="color-palette-3"]');
		element.val(e.color);
		$('#header').css('background', element.val());
	});
