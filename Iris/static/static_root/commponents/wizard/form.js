//Cargar el Plugin de Wizard.
class Wizard {

    constructor(container, form){
        this.formId = form;
        this.container = $(`#${container}`);
    }

    // Setea la configuracion del Wizard
    config(json) { this.json = json }

    //Se carga la informacion 
    async getComponentUrl(targetId, url, put = null){

        console.log(this.positionStep, this.nextItems[this.smartWizard.current_index]);
        //this.nextItems[this.smartWizard.current_index].actions.advance !== this.positionStep
        if(put === 'put'){
            await $.get(url, (response) =>{ 
                targetId.find('div#components').html(response);
                targetId.find('#components').children('div.row').addClass('servicio'); 
            });
        }
        
        //this.step[this.smartWizard.current_index] === this.positionStep
        if(put === 'add'){
            await $.get(url, (response) =>{ 
                targetId.find('div#components').append(response); 
                targetId.find('#components > div.row').eq(1).addClass('muestra'); 
            });
        }
    }

    //Ocultar los pasos que no son requeridos para el wizard. 
     setHiddenStep() {
         for (let index = 0; index < this.hiddenSteps.length; index++) {
            if (this.smartWizard.steps.eq(this.hiddenSteps[index]).parent('li').hasClass('done')) {
                this.container.smartWizard("stepState", [parseInt(this.hiddenSteps[index])], "hide"); 
                this.smartWizard.steps.eq(this.hiddenSteps[index]).parent('li').removeClass('done');
                break;           
            }
         }
    }

    // Inicializa el Wizard
    init() {
        this.positionStep = Object.keys(this.json)[0];
        this.children = Object.keys(this.json[`${this.positionStep}`].childrens)[0];

        //Se  llama la funcion que carga los pasos y contenedores 
        this.loadStep();
       
        //Variables la cual sera usada con fines de alcance a las funciones del smartWizard
        this.smartWizard = this.container.data('smartWizard');
        
        //Carga de los primeros elementos.
        this.step.forEach((element, index) => {
            if(index === 0 || index === (this.step.length-1)){
                this.builder(element);
            }
        })
    }

    //Cargar los pasos
    loadStep () {

        let htmlStep = ''; //Esta variable es para la carga del paso.
        let htmlStepContent = ''; //Esta variable es la que carga el contenido del step
        let data = this.json; //Solo para obtener datos informativos.

        //Se identifican los numeros que van en los pasos.
        let numbreSteps =  Object.keys(this.json).map((key, index) =>{ return (index+1)});
        numbreSteps.push((Object.keys(this.json).length + 1)); //Solo para el ambito del paso final. 
        
        //Se prepara el arreglo el cual ocultara el los steps-contents.
        this.hiddenSteps = Object.keys(numbreSteps).filter((k, i) => { return i !== 0 && i !== numbreSteps.length-1 });

        this.step = Object.keys(this.json);//Se cargan los Key para los steps.
        this.step.push('step-end'); //Solo para el ambito del paso final.

        //Se colocan los pasos que posiblemente recorera el wizard.
        for (let index = 0; index < this.step.length; index++) {
            let  stepInfo  = (this.step[index] === this.step[this.step.length-1])? 'Fin' : data[this.step[index]].settings.name;
            let  description  = (this.step[index] === this.step[this.step.length-1])? 'Completado' : data[this.step[index]].settings.description;
            htmlStep += '<li>'+
                '<a href="#'+this.step[index]+'">'+
                    '<span class="number"></span>'+ 
                    '<span class="info">'+stepInfo+
                    ' <small>'+description+'</small>'+
                    '</span>'+
                '</a>'+
            '</li>';     
        }  

        // Se cargan los pasos que estan 
        $('.steps').html(htmlStep);

        //Se agregan los contenedores que se reflejaran los steps. 
        for (let index = 0; index < this.step.length; index++) {
            //let  contentInfo  = (step[index] === step[step.length-1])? '' : data[step[index]].settings.information;
            htmlStepContent += '<div id="'+this.step[index]+'">'+
                                                '<fieldset>'+
                                                    '<div class="row">'+
                                                        '<div class="col-xl-8 offset-xl-2">'+
                                                            '<legend class="no-border f-w-700 p-b-0 m-t-0 m-b-20 f-s-16 text-inverse"></legend>'+
                                                            '<div id="components"></div>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</fieldset>'+
                                            '</div>';    
        }
        
        //Se cargan los espacios de los contenedores. 
        $('.steps-content').html(htmlStepContent);
        
        //Por ultimo se le agregan las configuraciones del smartWizard al maquetado creado. 
        this.container.smartWizard({
            selected: 0,
            keyNavigation: false,
            autoAdjustHeight: true,
            useURLhash: !1,
            showStepURLhash: !1,
            hiddenSteps: this.hiddenSteps,
            toolbarSettings: {
                showNextButton: false, // show/hide a Next button
                showPreviousButton: false, // show/hide a Previous button
            }
        });
    }

    //Cargar los componetes en los pasos implemtados.
    builder(positionStep = null){ 

        //Se le indica en que parte va a colocar el contenido segun su paso. 
        this.targetId = $(`#${positionStep}`);
        
        if (positionStep !== 'step-end') {
            this.event(positionStep);
            let component = this.dataChildrens[this.children].name_component;
            console.log(this.dataChildrens[this.children], component);
            let url = url_base_templates.format(`static/commponents/wizard/components/${component}.html`); 
            if (this.dataChildrens[this.children].dependence !== null && this.positionStep === this.step[this.smartWizard.current_index]) {
                this.getComponentUrl(this.targetId, url, 'add');    
            }else{
                this.getComponentUrl(this.targetId, url, 'put');
            }
        }else{
            //paso final que se va a construir.
            let url = url_base_templates.format(`static/commponents/wizard/components/end.html`);
            this.getComponentUrl(this.targetId, url, 'add');
        }
    }

    change(value = null){
        //se evalua si se quiere remover todos los items que estan en el contenedor.
        let advance_step = ($.isEmptyObject(this.nextItems)  || value === 0)? 'remover' : this.nextItems[value].actions.advance_step;

        if (advance_step === 'remover') {
            this.setHiddenStep();
            this.dataChildrens = this.json[this.positionStep].childrens;
            this.children = Object.keys(this.dataChildrens)[0];
            this.nextItems =this.dataChildrens[this.children].next_items; 
            $('#components > div.row').remove('.muestra');           
        }
        
        if(advance_step !==  this.positionStep && advance_step !== 'remover'){
            
            this.setHiddenStep();

            let step = parseInt(this.hiddenSteps.filter((key, index) =>{
                return this.step[key] === advance_step
            }))
            
            this.container.smartWizard("stepState", [step], "show");
            this.smartWizard ._showStep(step);

            this.children = this.nextItems[value].id;
            this.positionStep = this.nextItems[value].actions.advance_step;
            
            this.builder(this.nextItems[value].actions.advance_step);
        }

        console.log(advance_step, this.positionStep);
        if(advance_step ===  this.positionStep){ 
            this.children = (this.nextItems[value].id !== undefined)? this.nextItems[value].id : "";
            this.builder(this.nextItems[value].actions.advance_step);

        }

        
    }

    event(step = null){
        this.positionStep = (step !== null)? step : this.step[this.smartWizard.current_index];
        //Presentacion del primer hijo del paso.
        let data = this.json[this.positionStep];
        //Se cargan los hijos que tiene paso. 
        this.dataChildrens = data.childrens;
        console.log(this.positionStep, this.dataChildrens);
        
        this.nextItems = (this.dataChildrens[this.children].next_items !== undefined)? this.dataChildrens[this.children].next_items : {} ; 
    }

    //Pendiente.
    reset () {
        setTimeout(() => {
            this.step = 1;
            this.init()
            document.getElementById(this.formId).reset();
            $('.reset').css({'display': 'none'});
        },100);
    }

    //Para hacer el envio de los datos a la base de datos.
    submit(){
        this.smartWizard ._showStep(this.step.length - 1);
    }
}


//Se crea una instancia del Wizard
const wizard = new Wizard("wizard", "form-wizard");

//Cargar los datos de básicos de wizard.
wload = () => {
    var urlJSON = url_base_templates.format('static/commponents/wizard/samples/bpmn.io.json');
    $.get(urlJSON,        
        (data) => { 
            wizard.config(data);
            wizard.init();
        }, 
        'json'); 
}

$(".steps ").click(function(){ wizard.event(); });

wchange = (value) => { wizard.change(value) }

wreset = () => { wizard.reset(); }

wsubmit= () => { wizard.submit() }

wload();