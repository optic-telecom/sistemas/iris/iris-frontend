from .base import *

ALLOWED_HOSTS = ['iris7.cl', 'localhost', '127.0.0.1', '0.0.0.0']

DEBUG = False

# Variable para definir el que ambiente estoy
SENTRY_ET = 'Production'
