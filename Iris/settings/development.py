from .base import *

ALLOWED_HOSTS = ['iris.devel7.cl', 'localhost', '127.0.0.1', '0.0.0.0']

DEBUG = True

# Variable para definir el que ambiente estoy
SENTRY_ET = 'Development'
