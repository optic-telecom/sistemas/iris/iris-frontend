from .base import *

ALLOWED_HOSTS = ['localhost', '127.0.0.1', '0.0.0.0']

DEBUG = True

# Variable para definir el que ambiente estoy
SENTRY_ET = 'Local'
