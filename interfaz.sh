#!/bin/bash

NAME="$3" # Name of the application
DJANGODIR=/home/cristian/iris/iris-frontend # Django project directory
LOGFILE=/var/log/interfaz/$2
LOGDIR=$(dirname $LOGFILE)
SOCKFILE=/home/cristian/iris/iris-frontend/interfaz.sock # we will communicate using this unix socket
LOGDIR_PROJECT=/home/cristian/iris/iris-frontend/logs # we will communicate using this unix socket
USER=cristian # the user to run as
GROUP=cristian # the group to run as
NUM_WORKERS=3 # how many worker processes should Gunicorn spawn
DJANGO_WSGI_MODULE=Iris.wsgi
DJANGO_SETTINGS=$4
PORT=$1
echo "Starting $NAME as `whoami`"


# Activate the virtual environment
# Activate the virtual environment

#source /home/devel/sites/environments/sentinel/bin/activate
export DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS_MODULE
export PYTHONPATH=$DJANGODIR:$PYTHONPATH

# Create the run directory if it doesn't exist
RUNDIR=$(dirname $SOCKFILE)
test -d $RUNDIR || mkdir -p $RUNDIR

# Create the run directory if it doesn't exist for logs sentinel
test -d $LOGDIR_PROJECT || mkdir -p $LOGDIR_PROJECT

# Start your Django Unicorn
# Programs meant to be run under supervisor should not daemonize themselves (do not use --daemon)
source /home/cristian/environment/Frontend/bin/activate
source /home/cristian/projects/iris-frontend/Iris/enviroments/$5


exec /home/cristian/environment/Frontend/bin/gunicorn --env DJANGO_SETTINGS_MODULE=$DJANGO_SETTINGS $DJANGO_WSGI_MODULE \
--name $NAME \
--workers $NUM_WORKERS \
--user=$USER --group=$GROUP \
-b 0.0.0.0:$1 \
--log-level=debug \
--log-file=$LOGFILE 2>>$LOGFILE